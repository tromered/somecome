package superobject

import (
	"context"
	contentDTO "somecome/domain/content/dto"
)

type ContentDomain interface {
	// RegisterKey - регистрирует ключ контента
	RegisterKey(ctx context.Context, key contentDTO.ContentKey) error
	// GetKeyLink - возвращает ссылку на контент по его ключу
	GetKeyLink(ctx context.Context, contentKey string) *string
	// GetKeyName - возвращает название контента по его ключу
	GetKeyName(ctx context.Context, contentKey string) *string
	// GetKeyDescription - возвращает описание контента по его ключу
	GetKeyDescription(ctx context.Context, contentKey string) *string
	// AddTagToContent - добавляет тег контенту
	AddTagToContent(ctx context.Context, tagID int64, contentKey string, userID int64) error
	// ContentTags - теги привязанные к контенту
	ContentTags(ctx context.Context, contentKey string) ([]*contentDTO.Tag, error)
	// GetTags - все теги в системе
	GetTags(ctx context.Context) ([]*contentDTO.Tag, error)
	// CreateTag - создает новый тег
	CreateTag(ctx context.Context, tagInfo contentDTO.TagInput) (int64, error)
	// UpdateTag - обновляет тег
	UpdateTag(ctx context.Context, tagInfo contentDTO.TagInput) error
	// GetTag - возвращает тег по его ид
	GetTag(ctx context.Context, tagID int64) (*contentDTO.Tag, error)
	// ContentByTag - выполняет поиск контента по тегу
	ContentByTag(ctx context.Context, tagID int64) ([]*contentDTO.TaggedContent, error)
}
