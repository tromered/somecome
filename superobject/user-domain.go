package superobject

import (
	"context"
	userDTO "somecome/domain/user/dto"
)

// UserDomain - домен с пользовательскими данными
type UserDomain interface {
	// CreateMainInfo - создает основную информацию о пользователе
	CreateMainInfo(ctx context.Context, info userDTO.MainInfo) error
	// GetAllPrettyInfo - возвращает информацию о всех пользователях для рендеров
	GetAllPrettyInfo(ctx context.Context) ([]*userDTO.PrettyInfo, error)
	// GetMainInfo - возвращает информацию о пользователе по ИД
	GetMainInfo(ctx context.Context, id int64) (*userDTO.MainInfo, error)
	// GetPrettyInfo - возвращает информацию о пользователе для рендеров по ИД
	GetPrettyInfo(ctx context.Context, id int64) (*userDTO.PrettyInfo, error)
	// UpdateUserAvatar - обновляет аватар пользователя
	UpdateUserAvatar(ctx context.Context, userID int64, fileToken string) error
	// UploadUserAvatar - загружает новый аватар пользователя
	UploadUserAvatar(ctx context.Context, userID int64, fileInfo userDTO.AvatarInput) error
	// UpdateMainInfo - обновляет информацию о пользователе по ИД
	UpdateMainInfo(ctx context.Context, userID int64, input userDTO.UpdateMainInfoInput) error

	// GetNotifications - возвращает список уведомлений пользователя
	GetNotifications(ctx context.Context, userID int64, page *int64) (*userDTO.Notifications, error)
	// GetUnreadNotificationCount - возвращает количество непрочитанных уведомлений пользователя
	GetUnreadNotificationCount(ctx context.Context, userID int64) (int64, error)
	// MarkNotificationAsRead - отмечает уведомление прочитанным
	MarkNotificationAsRead(ctx context.Context, notificationID, userID int64) error
	// MarkNotificationAsReadByUser - отмечает уведомления пользователя прочитанными
	MarkNotificationAsReadByUser(ctx context.Context, userID int64) error
	// CreateNotification - создает новое уведомление
	CreateNotification(ctx context.Context, notification userDTO.NotificationInput) (int64, error)
}
