package application

import (
	"context"
	"somecome/superobject"
)

// BaseInit - инициализирует добавленные домены
func (app *Application) Start(ctx context.Context) (<-chan struct{}, error) {
	exit := make(chan struct{})
	buf := make([]<-chan struct{}, 0)

	// Для корректной работы необходимо выполнить обработку канала возврата в конце
	defer func() {
		// Запускаем асинхронный обработчик завершения дочерних каналов закрытия
		go func() {
			// канал должен быть закрыт в любом случае
			defer close(exit)

			// Ожидаем закрытие каждого канала
			for _, c := range buf {
				<-c
			}
		}()
	}()

	for _, domain := range app.domains {
		d, hasStart := domain.(superobject.DomainWithStart)
		// У домена нет асинхронной обработки, пропускаем
		if !hasStart {
			continue
		}

		c, err := d.Start(ctx)
		if err != nil {
			return exit, err
		}

		buf = append(buf, c)
	}

	return exit, nil
}
