package application

import (
	"context"
	"somecome/config"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

var _ superobject.SuperObject = new(Application)

type Application struct {
	authDomain          superobject.AuthDomain
	fileDomain          superobject.FileDomain
	userDomain          superobject.UserDomain
	forumDomain         superobject.ForumDomain
	reactionDomain      superobject.ReactionDomain
	commentDomain       superobject.CommentDomain
	purchaseDomain      superobject.PurchaseDomain
	webServerDomain     superobject.WebServerDomain
	textProcessorDomain superobject.TextProcessorDomain
	contentDomain       superobject.ContentDomain
	domains             []superobject.Domain
}

// AddDomains - добавляет домены в приложение
func (app *Application) AddDomains(domains ...superobject.Domain) {
	app.domains = append(app.domains, domains...)
}

// BaseInit - инициализирует добавленные домены
func (app *Application) BaseInit(ctx context.Context) error {
	for _, domain := range app.domains {
		err := domain.InnerInit(ctx, app)
		if err != nil {
			return err
		}
	}

	return nil
}

// ExtendInit - подготавливает добавленные домены
func (app *Application) ExtendInit(ctx context.Context) error {
	for _, domain := range app.domains {
		err := domain.ExternalInit(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

// Configure - конфигурирует домены
func (app *Application) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	for _, domain := range app.domains {
		err := domain.Configure(ctx, db, cfg)
		if err != nil {
			return err
		}
	}

	return nil
}

// ActiveDomains - возвращает список активных доменов
func (app *Application) ActiveDomains(ctx context.Context) []string {
	result := make([]string, 0, len(app.domains))
	for _, domain := range app.domains {
		result = append(result, domain.Name())
	}

	return result
}
