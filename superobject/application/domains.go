package application

import "somecome/superobject"

func (app *Application) AuthDomain() superobject.AuthDomain {
	if app.authDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.AuthDomain)
			if ok {
				app.authDomain = casted

				break
			}
		}
	}

	return app.authDomain
}

func (app *Application) CommentDomain() superobject.CommentDomain {
	if app.commentDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.CommentDomain)
			if ok {
				app.commentDomain = casted

				break
			}
		}
	}

	return app.commentDomain
}

func (app *Application) FileDomain() superobject.FileDomain {
	if app.fileDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.FileDomain)
			if ok {
				app.fileDomain = casted

				break
			}
		}
	}

	return app.fileDomain
}

func (app *Application) ForumDomain() superobject.ForumDomain {
	if app.forumDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.ForumDomain)
			if ok {
				app.forumDomain = casted

				break
			}
		}
	}

	return app.forumDomain
}

func (app *Application) PurchaseDomain() superobject.PurchaseDomain {
	if app.purchaseDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.PurchaseDomain)
			if ok {
				app.purchaseDomain = casted

				break
			}
		}
	}

	return app.purchaseDomain
}

func (app *Application) ReactionDomain() superobject.ReactionDomain {
	if app.reactionDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.ReactionDomain)
			if ok {
				app.reactionDomain = casted

				break
			}
		}
	}

	return app.reactionDomain
}

func (app *Application) UserDomain() superobject.UserDomain {
	if app.userDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.UserDomain)
			if ok {
				app.userDomain = casted

				break
			}
		}
	}

	return app.userDomain
}

func (app *Application) WebServerDomain() superobject.WebServerDomain {
	if app.webServerDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.WebServerDomain)
			if ok {
				app.webServerDomain = casted

				break
			}
		}
	}

	return app.webServerDomain
}

// TextProcessorDomain - домен обработки текстов
func (app *Application) TextProcessorDomain() superobject.TextProcessorDomain {
	if app.textProcessorDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.TextProcessorDomain)
			if ok {
				app.textProcessorDomain = casted

				break
			}
		}
	}

	return app.textProcessorDomain
}

// ContentDomain - домен работы с контентом
func (app *Application) ContentDomain() superobject.ContentDomain {
	if app.textProcessorDomain == nil {
		for _, domain := range app.domains {
			casted, ok := domain.(superobject.ContentDomain)
			if ok {
				app.contentDomain = casted

				break
			}
		}
	}

	return app.contentDomain
}
