package superobject

import (
	"context"
	purchaseDTO "somecome/domain/purchase/dto"
)

type PurchaseDomain interface {
	GetPurchase(ctx context.Context, id int64, full bool) (*purchaseDTO.Purchase, error)
	ListCard(ctx context.Context, creatorID int64) ([]*purchaseDTO.Card, error)
	ListCategories(ctx context.Context, creatorID int64) ([]*purchaseDTO.Category, error)
	ListOrganizations(ctx context.Context, creatorID int64) ([]*purchaseDTO.Organization, error)
	ListPurchases(ctx context.Context, creatorID int64, page int64, full bool) ([]*purchaseDTO.Purchase, error)
	NewCard(ctx context.Context, creatorID int64, name string) (int64, error)
	NewCategory(ctx context.Context, creatorID int64, name string) (int64, error)
	NewOrganization(ctx context.Context, creatorID int64, name string) (int64, error)
	NewPurchase(ctx context.Context, purchase *purchaseDTO.Purchase, creatorID int64) (int64, error)
	PurchaseCount(ctx context.Context, creatorID int64) (int64, int64, error)
	UpdatePurchase(ctx context.Context, purchase *purchaseDTO.Purchase, creatorID int64) error
}
