package superobject

import (
	"context"
	"io"
	fileDTO "somecome/domain/file/dto"
)

type FileDomain interface {
	// CreateFileToken - подготавливает файл для загрузки
	CreateFileToken(ctx context.Context, userID int64) (string, error)
	// GetFile - возвращает данные о файле в хранилище
	GetFile(ctx context.Context, token string, includeBody bool) (*fileDTO.FileInfo, io.ReadCloser, error)
	// GetUserImages - возвращает список изображений для пользователя из хранилища
	GetUserImages(ctx context.Context, userID int64) ([]*fileDTO.FileInfo, error)
	// UploadFile - подготавливает файл для загрузки
	UploadFile(ctx context.Context, info fileDTO.FileInfo, r io.Reader) error
	// UploadFileFull - загружает файл с созданием токена при необходимости
	UploadFileFull(ctx context.Context, userID int64, file fileDTO.FileInput) (string, error)
	// DeleteFile - удаляет файл
	DeleteFile(ctx context.Context, token string) error
	// GetUserFiles - возвращает список файлов загруженным пользователем в хранилище,
	// или данных токенов для загрузки, если файл еще не был загружен на диск (тело файла).
	GetUserFiles(ctx context.Context, userID int64, page *int64) ([]*fileDTO.FileInfo, error)
	// GetUserFilesTotal - возвращает сумму занимаемого места и количество файлов пользователя.
	GetUserFilesTotal(ctx context.Context, userID int64) (*fileDTO.TotalInfo, error)
	// DeleteUserFile - удаляет файл с проверкой соответствия пользователя
	DeleteUserFile(ctx context.Context, token string, userID int64) error
}
