package superobject

import (
	"context"
	reactionDTO "somecome/domain/reaction/dto"
)

type ReactionDomain interface {
	ContentReactionCompressed(ctx context.Context, contentKey string, userID *int64) ([]reactionDTO.CRCompressedInfo, error)
	ContentReactionSwitch(ctx context.Context, input reactionDTO.CRInput) error
	CreateContentReaction(ctx context.Context, input reactionDTO.CRInput) error
	CreateReaction(ctx context.Context, data reactionDTO.CreateReactionInput) (int64, error)
	GetReaction(ctx context.Context, id int64) (*reactionDTO.Reaction, error)
	GetReactions(ctx context.Context) ([]*reactionDTO.Reaction, error)
}
