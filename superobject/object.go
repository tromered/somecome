package superobject

import (
	"context"
	"somecome/config"

	"github.com/jmoiron/sqlx"
)

type SuperObject interface {
	// AuthDomain - домен авторизации
	AuthDomain() AuthDomain
	// CommentDomain - домен комментариев
	CommentDomain() CommentDomain
	// FileDomain - домен файлов
	FileDomain() FileDomain
	// ForumDomain - домен форума
	ForumDomain() ForumDomain
	// PurchaseDomain - домен покупок
	PurchaseDomain() PurchaseDomain
	// ReactionDomain - домен реакций
	ReactionDomain() ReactionDomain
	// UserDomain - домен данных пользователя
	UserDomain() UserDomain
	// WebServerDomain - домен веб сервера
	WebServerDomain() WebServerDomain
	// TextProcessorDomain - домен обработки текстов
	TextProcessorDomain() TextProcessorDomain
	// ContentDomain - домен работы с контентом
	ContentDomain() ContentDomain

	// ActiveDomains - возвращает список активных доменов
	ActiveDomains(ctx context.Context) []string
}

// Domain - общий интерфейс для домена
type Domain interface {
	// Name - название домена
	Name() string
	// Configure - конфигурирует домен
	Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error
	// InnerInit - инициализирует домен
	InnerInit(ctx context.Context, obj SuperObject) error
	// ExternalInit - подготавливает домен
	ExternalInit(ctx context.Context) error
}

// DomainWithStart - домен имеющий функцию обработки логики в отдельном потоке
type DomainWithStart interface {
	// Start - запускает логику в отдельном потоке
	Start(ctx context.Context) (<-chan struct{}, error)
}
