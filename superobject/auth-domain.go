package superobject

import (
	"context"
	authDTO "somecome/domain/auth/dto"
	"time"
)

type AuthDomain interface {
	AllUsers(ctx context.Context) ([]*authDTO.User, error)
	CreateGroup(ctx context.Context, group authDTO.Group) (int64, error)
	CreateJWT(ctx context.Context, login string, password string) (string, error)
	CreateSession(ctx context.Context, login string, password string) (string, error)
	CreateUser(ctx context.Context, login string, password string) (int64, error)
	CtxWithUser(ctx context.Context, user *authDTO.User) context.Context
	DeleteSession(ctx context.Context, token string) error
	GetGroup(ctx context.Context, id int64) (*authDTO.Group, error)
	GetUser(ctx context.Context, id int64) (*authDTO.User, error)
	GetUserByJWT(ctx context.Context, token string) (*authDTO.User, error)
	GetUserBySession(ctx context.Context, token string) (*authDTO.User, error)
	GetUserFromContext(ctx context.Context) (*authDTO.User, error)
	HasAccessFromCtx(ctx context.Context, code string, lv int16) bool
	ListGroupPermissions(ctx context.Context, groupID int64) ([]*authDTO.DomainWithPermissions, error)
	ListGroups(ctx context.Context) ([]*authDTO.Group, error)
	ListLevels(ctx context.Context) ([]*authDTO.LevelInfo, error)
	ListPoU(ctx context.Context) ([]*authDTO.DomainWithPermissions, error)
	SetGroupPermissions(ctx context.Context, groupID int64, newPermissions []authDTO.PermissionInput) error
	SetGroupUsers(ctx context.Context, groupID int64, userIDs []int64) error
	SetPoU(ctx context.Context, newPermissions []authDTO.PermissionInput) error
	UnauthorizedHasAccess(ctx context.Context, code string, lv int16) bool
	UpdateGroup(ctx context.Context, group authDTO.Group) error
	UserHasAccess(ctx context.Context, user *authDTO.User, code string, lv int16) bool
	UsersInGroup(ctx context.Context, groupID int64) (map[int64]time.Time, error)
	// RegisterPermissions - регистрирует права
	RegisterPermissions(ctx context.Context, domains ...authDTO.RegisterDomainInput) error
}
