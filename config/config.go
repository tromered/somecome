package config

import (
	"context"
	"encoding/json"
	"os"
	"somecome/pkg/errs"
)

// Config - общая конфигурация приложения
type Config struct {
	// Конфигурация БД
	DB DBConfig `json:"db"`
	// Конфигурация JWT
	JWT JWTConfig `json:"jwt"`
	// Общая конфигурация приложения
	System *SystemConfig `json:"system,omitempty"`
}

// SystemConfig - общая конфигурация приложения
type SystemConfig struct {
	// Режим отладки
	DebugMode bool `json:"debug"`
	// Порт веб сервера
	WebServerPort int64 `json:"ws_port"`
	// Путь до папки с файлами хранилища
	FileDir string `json:"file_dir"`
}

// JWTConfig - конфигурация JWT
type JWTConfig struct {
	// Приватный ключ для подписания
	PrivateKey string `json:"private_key"`
	// Публичный ключ для проверки
	PublicKey string `json:"public_key"`
}

// DBConfig - конфигурация базы данных
type DBConfig struct {
	// Адрес
	Host string `json:"host"`
	// Порт
	Port int `json:"port"`
	// Пользователь
	User string `json:"user"`
	// Пароль
	Password string `json:"password"`
	// Название базы
	DBName string `json:"db_name"`
}

// Load - загружает конфигурацию приложения
func Load(ctx context.Context, filename string) (*Config, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, errs.WrapError(ctx, nil, err)
	}

	data := new(Config)

	err = json.NewDecoder(file).Decode(&data)
	if err != nil {
		return nil, errs.WrapError(ctx, nil, err)
	}

	return data, nil
}
