-- Схема с реакциями пользователей
CREATE SCHEMA reaction;

-- Данные реакций
CREATE TABLE reaction.reactions(
    id          SERIAL8     PRIMARY KEY,
    name        TEXT        NOT NULL,
    creator_id  INT8        NOT NULL REFERENCES auth.users (id),
    file_token  TEXT        REFERENCES file.files (token),
    created     TIMESTAMPTZ NOT NULL,
    updated     TIMESTAMPTZ
);

-- Реакции на контент
CREATE TABLE reaction.content_reactions(
    content_key     TEXT        NOT NULL,
    user_id         INT8        NOT NULL REFERENCES auth.users (id),
    reaction_id     INT8        NOT NULL REFERENCES reaction.reactions (id),
    comment         TEXT,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ,
    UNIQUE(content_key, user_id, reaction_id)
);