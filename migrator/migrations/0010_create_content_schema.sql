-- Схема с данными для контента
CREATE SCHEMA content;

-- Данные тегов
CREATE TABLE content.tags(
    id          SERIAL8     PRIMARY KEY,
    tag         TEXT        NOT NULL UNIQUE,
    color       TEXT        NOT NULL,
    text_color  TEXT        NOT NULL,
    creator_id  INT8        NOT NULL REFERENCES auth.users (id),
    created     TIMESTAMPTZ NOT NULL,
    updated     TIMESTAMPTZ
);

-- Тегированный контент
CREATE TABLE content.tagged_content(
    tag_id          INT8        NOT NULL REFERENCES content.tags (id),
    content_key     TEXT        NOT NULL,
    creator_id      INT8        NOT NULL REFERENCES auth.users (id),
    created         TIMESTAMPTZ NOT NULL,
    UNIQUE(tag_id, content_key)
);
