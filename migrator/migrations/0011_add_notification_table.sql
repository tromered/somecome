-- Переименовывание схемы
ALTER SCHEMA user_info RENAME TO "user";

-- Уведомления пользователя
CREATE TABLE "user"."notification"(
    id              BIGSERIAL   PRIMARY KEY,
    user_id         INT8        NOT NULL REFERENCES auth.users (id),
    title           TEXT        NOT NULL,
    message         TEXT        NOT NULL,
    content_key     TEXT,
    link            TEXT,
    link_text       TEXT,
    is_read         BOOLEAN     NOT NULL DEFAULT FALSE,
    created         TIMESTAMPTZ NOT NULL
);

