package pageHandlers

import (
	"context"
	"net/http"
	fileDTO "somecome/domain/file/dto"
	userDTO "somecome/domain/user/dto"
	"somecome/domain/webserver/link"
	"somecome/pkg/logger"
	"somecome/superobject"
	"time"
)

// FileInfoUnit - данные файла
type FileInfoUnit struct {
	// Токен
	Token string
	// Название файла
	Name string
	// Адрес хранения файла
	URL string
	// Дата загрузки файла
	Uploaded time.Time
}

func (o *Object) listReactionPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ReactionListPageName)

		reactions, err := o.superObject.ReactionDomain().GetReactions(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		data := ReactionListTD{}

		for _, reaction := range reactions {
			user, err := userUnit(o.superObject, ctx, reaction.CreatorID)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

				return
			}

			file, err := fileInfoUnit(o.superObject, ctx, reaction.FileToken)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

				return
			}

			unit := ReactionListUnit{
				ID:      reaction.ID,
				Name:    reaction.Name,
				Creator: *user,
				File:    *file,
				Created: reaction.Created,
			}

			data.Reactions = append(data.Reactions, unit)
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

// fileInfoUnit - заполняет данные для рендеринга файла по его токену
func fileInfoUnit(
	wsd superobject.SuperObject,
	ctx context.Context, fileToken string,
) (*FileInfoUnit, error) {
	raw, _, err := wsd.FileDomain().GetFile(ctx, fileToken, false)
	if err != nil {
		logger.DebugError(ctx, err)

		return nil, err
	}

	info := fileInfoUnitFromModel(raw)

	return &info, nil
}

func fileInfoUnitFromModel(raw *fileDTO.FileInfo) FileInfoUnit {
	info := FileInfoUnit{
		Token:    raw.Token,
		URL:      link.GetFileLink(raw.Token, false),
		Uploaded: raw.Created,
	}

	if raw.Name != nil {
		info.Name = *raw.Name
	}

	if raw.Updated != nil {
		info.Uploaded = *raw.Updated
	}

	return info
}

// userUnit - заполняет данные для рендеринга пользователя по его ИД
func userUnit(
	wsd superobject.SuperObject,
	ctx context.Context, userID int64,
) (*UserUnit, error) {
	mainInfo, err := wsd.UserDomain().GetPrettyInfo(ctx, userID)
	if err != nil {

		return nil, err
	}

	info := userUnitFromModel(mainInfo)

	return info, nil
}

func userUnitFromModel(raw *userDTO.PrettyInfo) *UserUnit {
	info := &UserUnit{
		ID:          raw.UserID,
		DisplayName: raw.DisplayName,
	}

	if raw.HasAvatar {
		aurl := link.GetAvatarLink(raw.UserID)
		info.AvatarURL = &aurl
	}

	return info
}
