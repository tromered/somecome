package pageHandlers

import (
	"net/http"
	reactionDTO "somecome/domain/reaction/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"strconv"
)

func (o *Object) createFMRPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ReactionCreateFMRPageName)

		contentKey := r.URL.Query().Get("content-key")
		redirectTo := r.URL.Query().Get("redirect")

		data := ReactionCreateFMRTD{
			ContentKey:   contentKey,
			RedirectLink: redirectTo,
			SaveLink:     PathToCreateReactionOnContent,
		}

		reactions, err := o.superObject.ReactionDomain().GetReactions(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		for _, reaction := range reactions {
			data.Reactions = append(data.Reactions, ReactionSimpleListUnit{
				ID:          reaction.ID,
				Name:        reaction.Name,
				PreviewLink: link.GetReactionLink(reaction.ID),
			})
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) createFMRPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		contentKey := r.FormValue("content-key")

		reactionID, err := strconv.ParseInt(r.FormValue("reaction-id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		redirectTo := r.FormValue("redirect")
		if redirectTo == "" {
			redirectTo = "/"
		}

		comment := r.FormValue("comment")

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		err = o.superObject.ReactionDomain().CreateContentReaction(
			ctx, reactionDTO.CRInput{
				ContentKey: contentKey,
				ReactionID: reactionID,
				UserID:     userData.ID,
				Comment:    comment,
			},
		)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		http.Redirect(w, r, redirectTo, http.StatusSeeOther)
	})
}
