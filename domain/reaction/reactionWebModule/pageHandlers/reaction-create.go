package pageHandlers

import (
	"net/http"
	reactionDTO "somecome/domain/reaction/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/logger"
	"somecome/superobject"
)

// ReactionCreateTD - данные для рендеринга создания новой реакции
type ReactionCreateTD struct {
	// Ссылка для сохранения
	SaveLink string
}

func (o *Object) createReactionPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ReactionCreatePageName)
		tdata.SetData(ReactionCreateTD{
			SaveLink: PathToCreateReaction,
		})
		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) createReactionPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ReactionCreatePageName)
		tdata.SetData(ReactionCreateTD{
			SaveLink: PathToCreateReaction,
		})

		err := r.ParseMultipartForm(core.MaxFileSize)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		fileBody, fileHeader, err := r.FormFile("file")
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}
		defer logger.IfErrFunc(ctx, fileBody.Close)

		reactionName := r.FormValue("name")
		fileName := fileHeader.Filename
		fileMime := fileHeader.Header.Get("Content-Type")

		messageID, err := o.superObject.ReactionDomain().CreateReaction(
			ctx, reactionDTO.CreateReactionInput{
				Name:      reactionName,
				CreatorID: userData.ID,
				File: reactionDTO.FileToUpload{
					Name: fileName,
					Mime: fileMime,
					Body: fileBody,
				},
			},
		)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		logger.Debug(ctx, "reaction created:", messageID)

		http.Redirect(w, r, PathToCreateReaction, http.StatusSeeOther)
	})
}
