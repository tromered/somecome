package pageHandlers

import "somecome/domain/webserver/link"

const moduleTemplateName = "module:reaction:page"

// Коды страниц для рендеринга шаблонов
var (
	ReactionCreatePageName    = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:reaction:page:create-new"}
	ReactionListPageName      = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:reaction:page:reaction-list"}
	ReactionCreateFMRPageName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:reaction:page:create-fmr"}
)

// Ендпоинты веб сервера
const (
	PathToCreateReaction          = "/pages/reaction/create"
	PathToReactionList            = "/pages/reaction/list"
	PathToCreateReactionOnContent = "/pages/reaction/create-on-content"
)
