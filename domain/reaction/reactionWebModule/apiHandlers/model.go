package apiHandlers

type ReactionOutput struct {
	// ИД реакции
	ID int64 `json:"id"`
	// Название реакции
	Name string `json:"name"`
	// Ссылка на предпросмотр
	PreviewLink string `json:"preview_link"`
}

type ContentReactionInput struct {
	// Ключ контента
	ContentKey string `json:"content_key"`
	// ИД реакции
	ReactionID int64 `json:"reaction_id"`
	// Комментарий
	Comment string `json:"comment,omitempty"`
}

type CRCompressedInfoOutput struct {
	// Ключ контента
	ContentKey string `json:"content_key"`
	// ИД реакции
	ReactionID int64 `json:"reaction_id"`
	// Количество таких реакций
	Count int64 `json:"count"`
	// Активирована текущим пользователем
	Used bool `json:"used"`
	// Ссылка на предпросмотр
	PreviewLink string `json:"preview_link"`
}

type CRCompressedInfoInput struct {
	// Ключ контента
	ContentKey string `json:"content_key"`
}
