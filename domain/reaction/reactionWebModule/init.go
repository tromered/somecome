package reactionWebModule

import (
	"context"
	"net/http"
	"somecome/domain/reaction/reactionWebModule/apiHandlers"
	"somecome/domain/reaction/reactionWebModule/pageHandlers"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля реакций
	ModuleError = errs.NewGroup("reaction module")
)

// InitModule - инициализирует модуль реакций
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		apiHandler:  apiHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль реакций
type Module struct {
	pageHandler *pageHandlers.Object
	apiHandler  *apiHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "reaction" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
	m.apiHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, category)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var category = webServerDTO.MenuCategoryInternal{
	Name:  "Реакции",
	Login: webServerDTO.LoginAuthorizedOnly,
	Items: []webServerDTO.MenuItemInternal{
		{
			Link: pageHandlers.PathToCreateReaction,
			Name: "Создать",
		},
		{
			Link: pageHandlers.PathToReactionList,
			Name: "Список",
		},
	},
}
