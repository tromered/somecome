package reaction

import (
	"context"
	fileDTO "somecome/domain/file/dto"
	reactionDTO "somecome/domain/reaction/dto"
	"somecome/pkg/errs"
)

// CreateReaction - создает реакцию
func (domain *Domain) CreateReaction(ctx context.Context, data reactionDTO.CreateReactionInput) (int64, error) {
	fileToken, err := domain.superObject.FileDomain().UploadFileFull(ctx, data.CreatorID, fileDTO.FileInput{
		Name: data.File.Name,
		Mime: data.File.Mime,
		Body: data.File.Body,
	})
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	id, err := domain.storage.CreateReaction(ctx, data.Name, data.CreatorID, fileToken)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}
