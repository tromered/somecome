package reaction

import (
	"context"
	"database/sql"
	"errors"
	"somecome/domain/reaction/database"
	reactionDTO "somecome/domain/reaction/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

func fmrInputToDB(raw reactionDTO.CRInput) *database.ContentReaction {
	return &database.ContentReaction{
		ContentKey: raw.ContentKey,
		UserID:     raw.UserID,
		ReactionID: raw.ReactionID,
		Comment:    sqlconvert.ToNullStringE(raw.Comment),
	}
}

// ContentReactionSwitch - переключает состояние реакций,
// если реакция була проставлена - то удаляет ее, иначе добавляет
func (domain *Domain) ContentReactionSwitch(ctx context.Context, input reactionDTO.CRInput) error {
	existsReaction, err := domain.storage.SelectContentReaction(ctx, input.ContentKey, input.UserID, input.ReactionID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return errs.WrapError(ctx, DomainError, err)
	}

	// Реакция существует, удаляем ее
	if existsReaction != nil {
		err = domain.storage.DeleteContentReaction(ctx, existsReaction)
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}

		return nil
	}

	err = domain.storage.CreateContentReaction(ctx, fmrInputToDB(input))
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// ContentReactionCompressed - возвращает список реакций на контент
func (domain *Domain) ContentReactionCompressed(ctx context.Context, contentKey string, userID *int64) ([]reactionDTO.CRCompressedInfo, error) {
	reactionsRaw, err := domain.storage.SelectContentReactions(ctx, contentKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	reactionsMap := make(map[int64]reactionDTO.CRCompressedInfo)
	for _, reactionRaw := range reactionsRaw {
		reaction := reactionsMap[reactionRaw.ReactionID]

		reaction.ContentKey = contentKey
		reaction.ReactionID = reactionRaw.ReactionID
		reaction.Count++
		reaction.Used = reaction.Used || (userID != nil && reactionRaw.UserID == *userID)

		reactionsMap[reactionRaw.ReactionID] = reaction
	}

	result := make([]reactionDTO.CRCompressedInfo, 0)
	for _, reaction := range reactionsMap {
		result = append(result, reaction)
	}

	return result, nil
}

// CreateContentReaction - создает реакцию на контент
func (domain *Domain) CreateContentReaction(ctx context.Context, input reactionDTO.CRInput) error {
	err := domain.storage.CreateContentReaction(ctx, fmrInputToDB(input))
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}
