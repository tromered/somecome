package reactionDTO

import (
	"io"
	"time"
)

// Reaction - данные о реакции
type Reaction struct {
	// ИД реакции
	ID int64
	// Название реакции
	Name string
	// Ид пользователя создавшего реакцию
	CreatorID int64
	// Токен файла из хранилища
	FileToken string
	// Время создания реакции
	Created time.Time
	// Время последнего обновления реакции
	Updated *time.Time
}

// FileToUpload - данные для загрузки файла
type FileToUpload struct {
	// Название файла
	Name string
	// MIME тип файла
	Mime string
	// Тело файла
	Body io.Reader
}

// CreateReactionInput - данные для создания реакции
type CreateReactionInput struct {
	// Название реакции
	Name string
	// Ид пользователя создавшего реакцию
	CreatorID int64
	// Данные файла для реакции
	File FileToUpload
}

// CRInput - входные данные для реакции на контент
type CRInput struct {
	// Ключ контента
	ContentKey string
	// ИД реакции
	ReactionID int64
	// ИД пользователя
	UserID int64
	// Комментарий
	Comment string
}

// CRCompressedInfo - данные о реакции на контент, сжатые по одной реакции
type CRCompressedInfo struct {
	// Ключ контента
	ContentKey string
	// ИД реакции
	ReactionID int64
	// Количество таких реакций
	Count int64
	// Активирована текущим пользователем
	Used bool
}
