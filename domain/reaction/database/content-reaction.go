package database

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// CreateContentReaction - создает запись о новой реакции на контент в БД
func (d *Database) CreateContentReaction(ctx context.Context, reaction *ContentReaction) error {
	_, err := d.db.ExecContext(
		ctx,
		`INSERT INTO reaction.content_reactions(content_key, user_id, reaction_id, comment, created) VALUES ($1, $2, $3, $4, $5);`,
		reaction.ContentKey, reaction.UserID, reaction.ReactionID, reaction.Comment, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// SelectContentReaction - получает реакцию на контент
func (d *Database) SelectContentReaction(ctx context.Context, contentKey string, userID, reactionID int64) (*ContentReaction, error) {
	data := new(ContentReaction)

	err := d.db.GetContext(
		ctx, data,
		`SELECT * FROM reaction.content_reactions WHERE content_key = $1 AND user_id = $2 AND reaction_id = $3 LIMIT 1;`,
		contentKey, userID, reactionID,
	)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return data, nil
}

// SelectContentReactions - получает реакции на контент
func (d *Database) SelectContentReactions(ctx context.Context, contentKey string) ([]*ContentReaction, error) {
	list := make([]*ContentReaction, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM reaction.content_reactions WHERE content_key = $1;`, contentKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// DeleteContentReaction - удаляет запись о реакции на контент в БД
func (d *Database) DeleteContentReaction(ctx context.Context, reaction *ContentReaction) error {
	_, err := d.db.ExecContext(
		ctx,
		`DELETE FROM reaction.content_reactions WHERE content_key = $1 AND user_id = $2 AND reaction_id = $3;`,
		reaction.ContentKey, reaction.UserID, reaction.ReactionID,
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
