package commentDTO

import "errors"

var (
	// Сообщение не создано пользователем
	NotAuthorErr = errors.New("not author")
)
