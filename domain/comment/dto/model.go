package commentDTO

import (
	"io"
	"time"
)

// Message - сообщение на контент
type Message struct {
	// ИД сообщения
	ID int64
	// Ключ контента, на который ссылается сообщение
	LinkedToContentKey string
	// Ид пользователя написавшего сообщение
	CreatorID int64
	// Текст сообщения
	RawText string
	// Диалект сообщения
	Dialect *string
	// Время создания сообщения
	Created time.Time
	// Время последнего обновления сообщения
	Updated *time.Time
	// Вложения прикрепленные к сообщению
	Attachments []Attachment

	// Ключ контента самого сообщения
	InnerContentKey string
}

// Attachment - вложения в сообщение
type Attachment struct {
	// ИД сообщения
	MessageID int64
	// Токен файла вложения из хранилища
	FileToken string
	// Ид пользователя прикрепившего вложение
	CreatorID int64
	// Время создания вложения
	Created time.Time
	// Время последнего обновления вложения
	Updated *time.Time
}

// FileInput - спец обвязка для файла
type FileInput struct {
	// Название файла
	Name string
	// MIME тип файла
	Mime string
	// Размер файла
	Size int64
	// Тело файла
	Body io.Reader
}

// MessageInput - входные данные для сообщения на контент
type MessageInput struct {
	// Ключ контента
	ContentKey string
	// Ид пользователя написавшего сообщение
	CreatorID int64
	// Текст сообщения
	RawText string
	// Диалект сообщения
	Dialect *string
	// Вложения прикрепленные к сообщению
	Attachments []FileInput
}
