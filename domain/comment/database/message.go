package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// CreateMessage - создает запись о новом сообщении в БД
func (d *Database) CreateMessage(ctx context.Context, contentKey string, text string, dialect sql.NullString, userID int64) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO comment.messages(content_key, creator_id, raw_text, dialect, created)
	 VALUES ($1, $2, $3, $4, $5) RETURNING id;`, contentKey, userID, text, dialect, time.Now())
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// SelectMessages - получает список сообщений на контент
func (d *Database) SelectMessages(ctx context.Context, contentKey string) ([]*Message, error) {
	list := make([]*Message, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM comment.messages WHERE content_key = $1 ORDER BY id;`, contentKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectMessage - получает сообщение на контент по ID
func (d *Database) SelectMessage(ctx context.Context, id int64) (*Message, error) {
	message := new(Message)

	err := d.db.GetContext(ctx, message, `SELECT * FROM comment.messages WHERE id = $1 LIMIT 1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return message, nil
}

// SelectMessagesWithLimit - получает список сообщений на контент с ограничением и сдвигом
func (d *Database) SelectMessagesWithLimit(ctx context.Context, contentKey string, limit int64, offset int64) ([]*Message, error) {
	list := make([]*Message, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM comment.messages WHERE content_key = $1 ORDER BY id LIMIT $2 OFFSET $3;`, contentKey, limit, offset)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectMessageCount - получает количество сообщений на контент
func (d *Database) SelectMessageCount(ctx context.Context, contentKey string) (int64, error) {
	var count int64

	err := d.db.GetContext(ctx, &count, `SELECT count(*) FROM comment.messages WHERE content_key = $1;`, contentKey)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return count, nil
}

// UpdateMessage - обновляет сообщение (комментарий) в БД
func (d *Database) UpdateMessage(ctx context.Context, message *Message) error {
	_, err := d.db.ExecContext(ctx, `UPDATE comment.messages SET
	content_key = $2, creator_id = $3, raw_text = $4, dialect = $5, updated = $6
	WHERE id = $1`, message.ID, message.ContentKey, message.CreatorID, message.RawText, message.Dialect, time.Now())
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
