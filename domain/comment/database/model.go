package database

import (
	"database/sql"
	"time"
)

// Message - сообщение на контент
type Message struct {
	// ИД сообщения
	ID int64 `db:"id"`
	// Ключ контента
	ContentKey string `db:"content_key"`
	// Ид пользователя написавшего сообщение
	CreatorID int64 `db:"creator_id"`
	// Текст сообщения
	RawText string `db:"raw_text"`
	// Диалект сообщения
	Dialect sql.NullString `db:"dialect"`
	// Время создания сообщения
	Created time.Time `db:"created"`
	// Время последнего обновления сообщения
	Updated sql.NullTime `db:"updated"`
}

// Attachment - вложения в сообщение
type Attachment struct {
	// ИД сообщения
	MessageID int64 `db:"message_id"`
	// Токен файла вложения из хранилища
	FileToken string `db:"file_token"`
	// Ид пользователя прикрепившего вложение
	CreatorID int64 `db:"creator_id"`
	// Время создания вложения
	Created time.Time `db:"created"`
	// Время последнего обновления вложения
	Updated sql.NullTime `db:"updated"`
}
