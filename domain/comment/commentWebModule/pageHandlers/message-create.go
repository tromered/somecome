package pageHandlers

import (
	"net/http"
	commentDTO "somecome/domain/comment/dto"
	txtpDTO "somecome/domain/txtp/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/logger"
	"somecome/superobject"
)

func (o *Object) forumMessageCreatePageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, MessageCreatePageName)

		redirect := r.URL.Query().Get("redirect")
		contentKey := r.URL.Query().Get("content-key")

		tdata.SetData(CreateMessagePageTD{
			ContentKey: contentKey,
			Dialects: []string{
				string(txtpDTO.PlainTextDialect),
				string(txtpDTO.MarkdownBaseDialect),
				string(txtpDTO.MarkdownExtendDialect),
			},
			NewMessageLink: PathToCreateMessage,
			RedirectLink:   redirect,
		})

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) forumMessageCreatePagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, MessageCreatePageName)
		err := r.ParseMultipartForm(core.MaxFileSize)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		redirect := r.FormValue("redirect")
		contentKey := r.FormValue("content-key")
		messageText := r.FormValue("message-text")
		var messageDialect *string
		if v := r.FormValue("message-dialect"); v != "" {
			messageDialect = &v
		}

		attachments := make([]commentDTO.FileInput, 0)

		for _, fileHeader := range r.MultipartForm.File["message-attachments"] {
			logger.DebugData(ctx, fileHeader.Header)

			fileName := fileHeader.Filename
			fileMime := fileHeader.Header.Get("Content-Type")

			fileBody, err := fileHeader.Open()
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}
			defer logger.IfErrFunc(ctx, fileBody.Close)

			attachments = append(attachments, commentDTO.FileInput{
				Name: fileName,
				Mime: fileMime,
				Body: fileBody,
			})
		}

		messageID, err := o.superObject.CommentDomain().NewMessage(
			ctx, &commentDTO.MessageInput{
				ContentKey:  contentKey,
				CreatorID:   userData.ID,
				RawText:     messageText,
				Dialect:     messageDialect,
				Attachments: attachments,
			},
		)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		logger.Debug(ctx, "comment created:", messageID)

		if redirect == "" {
			redirect = "/"
		}

		http.Redirect(w, r, redirect, http.StatusSeeOther)
	})
}
