package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToListMessage, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.messageListPageGet(wsd),
		},
	))
	mux.Handle(PathToCreateMessage, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.forumMessageCreatePageGet(wsd),
			http.MethodPost: o.forumMessageCreatePagePost(wsd),
		},
	))
	mux.Handle(PathToEditMessage, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.forumMessageEditPageGet(wsd),
			http.MethodPost: o.forumMessageEditPagePost(wsd),
		},
	))
}
