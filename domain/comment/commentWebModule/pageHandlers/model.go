package pageHandlers

import (
	webServerDTO "somecome/domain/webserver/dto"
	"time"
)

// MessageListTD - данные для страницы со списком комментов
type MessageListTD struct {
	// Данные о сообщениях
	Messages []MessageUnit
	// Страницы форума для пагинации
	Pages []webServerDTO.Page
	// Доступные диалекты сообщений
	Dialects []string
	// Ссылка на форму для создания нового сообщения
	NewMessageLink string
}

// MessageUnit - данные комментария
type MessageUnit struct {
	// Текст сообщения
	Text string
	// Диалект сообщения
	Dialect *string
	// Данные о пользователе что оставил сообщение
	User *UserUnit
	// Вложения в сообщении
	Attachments []MessageAttachmentUnit
	// Контент ключ для реакций
	FMRKey string
	// Ссылка на редактирование сообщения
	EditLink *string
	// Время создания сообщения
	Created time.Time
	// Время последнего обновления сообщения
	Updated *time.Time
}

// MessageAttachmentUnit - данные вложения в комментарий
type MessageAttachmentUnit struct {
	// Расположение файла
	URL string
	// Название вложения
	Name string
	// Указывает на то что это изображение
	IsImage bool
}

// CreateMessagePageTD - данные для страницы создания комментария
type CreateMessagePageTD struct {
	// Ключ контента
	ContentKey string
	// Диалекты сообщения
	Dialects []string
	// Ссылка на форму для создания нового сообщения
	NewMessageLink string
	// Ссылка для последующего редиректа
	RedirectLink string
}

// UserUnit - данные пользователя для отображения
type UserUnit struct {
	// ИД пользователя
	ID int64
	// Аватарка пользователя
	AvatarURL *string
	// Отображаемое имя
	DisplayName string
}
