package pageHandlers

import (
	"net/url"
	"somecome/domain/webserver/link"
	"strconv"
)

const moduleTemplateName = "module:comment:page"

// Коды страниц для рендеринга шаблонов
var (
	MessageCreatePageName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:comment:page:create-message"}
	MessageEditPageName   = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:comment:page:edit-message"}
	MessageListPageName   = link.PageTemplateName{TemplateName: "module:comment:comment-list", PageName: ""}
)

// Ендпоинты веб сервера
const (
	PathToListMessage   = "/pages/comment/list"
	PathToCreateMessage = "/pages/comment/create"
	PathToEditMessage   = "/pages/comment/edit"
)

// GetMessagePageLink - генерирует ссылку на страницу сообщений
func GetMessagePageLink(contentKey string, pageNumber *int64, toLast bool) string {
	link := url.URL{
		Path: PathToListMessage,
	}

	args := url.Values{
		"content-key": []string{contentKey},
	}

	switch {
	case pageNumber != nil:
		args.Set("page", strconv.FormatInt(*pageNumber, 10))
	case toLast:
		args.Set("page", "last")
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetMessagePageLinkWrapper - враппер для генерации ссылок на страницы сообщений
func GetMessagePageLinkWrapper(contentKey string) link.PageLinkGetter {
	return func(pageNumber int64) string {
		return GetMessagePageLink(contentKey, &pageNumber, false)
	}
}

// GetCreateMessageLink - генерирует ссылку для создания нового сообщения
func GetCreateMessageLink(contentKey string, redirect string) string {
	link := url.URL{
		Path: PathToCreateMessage,
	}

	args := url.Values{
		"content-key": []string{contentKey},
		"redirect":    []string{redirect},
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetEditMessageLink - генерирует ссылку для редактирования сообщения
func GetEditMessageLink(messageID int64, redirect string) string {
	link := url.URL{
		Path: PathToEditMessage,
	}

	args := url.Values{
		"message-id": []string{strconv.FormatInt(messageID, 10)},
		"redirect":   []string{redirect},
	}

	link.RawQuery = args.Encode()

	return link.String()
}
