package commentWebModule

import (
	"context"
	"net/http"
	"somecome/domain/comment/commentWebModule/pageHandlers"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля комментариев
	ModuleError = errs.NewGroup("comment module")
)

// InitModule - инициализирует модуль комментариев
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль комментариев
type Module struct {
	pageHandler *pageHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "comment" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
}
