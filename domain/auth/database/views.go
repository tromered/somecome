package database

import (
	"context"
	"somecome/pkg/errs"
)

// UserPermissionView - права доступа пользователя
type UserPermissionView struct {
	// ИД права доступа
	PermissionID int64 `db:"permission_id"`
	// Код права доступа
	Code string `db:"code"`
	// Количество агрегированных записей
	Amount int64 `db:"amount"`
	// Максимальное значение записи
	Maximum int16 `db:"maximum"`
	// Минимальное значение записи
	Minimum int16 `db:"minimum"`
}

// SelectUserPermissionViews - получает список прав доступа пользователя
func (d *Database) SelectUserPermissionViews(ctx context.Context, userID int64) ([]*UserPermissionView, error) {
	list := make([]*UserPermissionView, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT
	gp.permission_id,
	p.code,
	COUNT(*) AS amount,
	MAX(gp."level") AS maximum,
	MIN(gp."level") AS minimum
FROM
	auth.group_users gu
INNER JOIN auth.group_permissions gp ON
	gu.group_id = gp.group_id
INNER JOIN auth.permissions p ON
	p.id = gp.permission_id
WHERE
	gu.user_id = $1
GROUP BY
	gp.permission_id,
	p.code;`, userID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// GetUserPermissionViewByID - получает данные права доступа пользователя по ИД права
func (d *Database) GetUserPermissionViewByID(ctx context.Context, userID int64, permissionID int64) (*UserPermissionView, error) {
	item := new(UserPermissionView)

	err := d.db.GetContext(ctx, item, `SELECT
	gp.permission_id,
	p.code,
	COUNT(*) AS amount,
	MAX(gp."level") AS maximum,
	MIN(gp."level") AS minimum
FROM
	auth.group_users gu
INNER JOIN auth.group_permissions gp ON
	gu.group_id = gp.group_id
INNER JOIN auth.permissions p ON
	p.id = gp.permission_id
WHERE
	gu.user_id = $1
	AND gp.permission_id = $2
GROUP BY
	gp.permission_id,
	p.code
LIMIT 1;`, userID, permissionID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return item, nil
}

// GetUserPermissionViewByCode - получает данные права доступа пользователя по коду права
func (d *Database) GetUserPermissionViewByCode(ctx context.Context, userID int64, permissionCode string) (*UserPermissionView, error) {
	item := new(UserPermissionView)

	err := d.db.GetContext(ctx, item, `SELECT
	gp.permission_id,
	p.code,
	COUNT(*) AS amount,
	MAX(gp."level") AS maximum,
	MIN(gp."level") AS minimum
FROM
	auth.group_users gu
INNER JOIN auth.group_permissions gp ON
	gu.group_id = gp.group_id
INNER JOIN auth.permissions p ON
	p.id = gp.permission_id
WHERE
	gu.user_id = $1
	AND p.code = $2
GROUP BY
	gp.permission_id,
	p.code
LIMIT 1;`, userID, permissionCode)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return item, nil
}
