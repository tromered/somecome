package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Session - данные сессии пользователя
type Session struct {
	// Токен сессии
	Token string `db:"token"`
	// Ид пользователя в базе
	UserID int64 `db:"user_id"`
	// Сессия закрыта (завершена)
	IsClosed bool `db:"is_closed"`
	// Время создания сессии
	Created time.Time `db:"created"`
	// Время последнего использования (обращения) сессии
	Used sql.NullTime `db:"used"`
	// Время последнего обновления сессии
	Updated sql.NullTime `db:"updated"`
}

// CreateSession - создает в базе запись о сессии пользователя
// Поля IsClosed, Used, Updated игнорируются, поле Created заменяется
func (d *Database) CreateSession(ctx context.Context, session *Session) error {
	session.Created = time.Now()
	_, err := d.db.NamedExecContext(ctx, `INSERT INTO auth.sessions(token, user_id, created) VALUES (:token, :user_id, :created);`, session)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// GetSessionByToken - возвращает сессию пользователя по токену, не проверяет ее на закрытие.
func (d *Database) GetSessionByToken(ctx context.Context, token string) (*Session, error) {
	session := new(Session)

	err := d.db.GetContext(ctx, session, `SELECT * FROM auth.sessions WHERE token = $1 LIMIT 1;`, token)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return session, nil
}

// UpdateSessionUsedTime - обновляет время использования сессии.
func (d *Database) UpdateSessionUsedTime(ctx context.Context, token string) error {
	_, err := d.db.ExecContext(ctx, `UPDATE auth.sessions SET used = $1 WHERE token = $2;`, time.Now(), token)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// CloseSessionByToken - аннулирует сессию по токену.
func (d *Database) CloseSessionByToken(ctx context.Context, token string) error {
	_, err := d.db.ExecContext(ctx, `UPDATE auth.sessions SET is_closed = $1 WHERE token = $2;`, true, token)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
