package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// PermissionsOfUnauthorized - доступы неавторизованных
type PermissionsOfUnauthorized struct {
	// ИД доступа
	PermissionID int64 `db:"permission_id"`
	// Уровень доступа
	Level int16 `db:"level"`
	// Время создания доступа для неавторизованных
	Created time.Time `db:"created"`
	// Время обновления доступа для неавторизованных
	Updated sql.NullTime `db:"updated"`
}

// SelectPermissionOfUnauthorized - получает список уровней доступа для неавторизованных
func (d *Database) SelectPermissionOfUnauthorized(ctx context.Context) ([]*PermissionsOfUnauthorized, error) {
	list := make([]*PermissionsOfUnauthorized, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.permissions_of_unauthorized ORDER BY permission_id;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// GetPermissionOfUnauthorized - получает уровень доступа для неавторизованных
func (d *Database) GetPermissionOfUnauthorized(ctx context.Context, permissionID int64) (*PermissionsOfUnauthorized, error) {
	item := new(PermissionsOfUnauthorized)

	err := d.db.GetContext(ctx, item, `SELECT * FROM auth.permissions_of_unauthorized WHERE permission_id = $1 LIMIT 1;`, permissionID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return item, nil
}

// GetPermissionOfUnauthorizedByCode - получает уровень доступа для неавторизованных по коду
func (d *Database) GetPermissionOfUnauthorizedByCode(ctx context.Context, permissionCode string) (*PermissionsOfUnauthorized, error) {
	item := new(PermissionsOfUnauthorized)

	err := d.db.GetContext(ctx, item, `SELECT pou.* FROM auth.permissions_of_unauthorized pou INNER JOIN auth.permissions p ON pou.permission_id = p.id WHERE p.code = $1 LIMIT 1;`, permissionCode)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return item, nil
}

// InsertPermissionOfUnauthorized - добавляет новую привилегию для неавторизованных
func (d *Database) InsertPermissionOfUnauthorized(ctx context.Context, permissionID int64, level int16) error {
	_, err := d.db.ExecContext(ctx,
		`INSERT INTO auth.permissions_of_unauthorized (permission_id, level, created) VALUES ($1, $2, $3);`,
		permissionID, level, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// UpdatePermissionOfUnauthorized - обновляет привилегию для неавторизованных
func (d *Database) UpdatePermissionOfUnauthorized(ctx context.Context, permissionID int64, level int16) error {
	_, err := d.db.ExecContext(ctx,
		`UPDATE auth.permissions_of_unauthorized SET level = $2, updated = $3 WHERE permission_id = $1;`,
		permissionID, level, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// DeletePermissionOfUnauthorized - удаляет привилегию для неавторизованных
func (d *Database) DeletePermissionOfUnauthorized(ctx context.Context, permissionID int64) error {
	_, err := d.db.ExecContext(ctx, `DELETE FROM auth.permissions_of_unauthorized WHERE permission_id = $1;`, permissionID)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
