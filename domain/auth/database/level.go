package database

import (
	"context"
	"somecome/pkg/errs"
)

// PermissionLevel - уровни доступа
type PermissionLevel struct {
	// Номер уровня
	Level int16 `db:"level"`
	// Название уровня
	Name string `db:"name"`
}

// SelectPermissionLevels - получает список уровней доступа
func (d *Database) SelectPermissionLevels(ctx context.Context) ([]*PermissionLevel, error) {
	list := make([]*PermissionLevel, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.permission_levels ORDER BY level;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}
