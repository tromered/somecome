package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Domain - домен для группировки доступов
type Domain struct {
	// Код домена
	Code string `db:"code"`
	// Название домена
	Name string `db:"name"`
	// Описание домена
	Description sql.NullString `db:"description"`
	// Время создания домена
	Created time.Time `db:"created"`
	// Время обновления домена
	Updated sql.NullTime `db:"updated"`
}

// SelectDomains - получает список доменов для доступа
func (d *Database) SelectDomains(ctx context.Context) ([]*Domain, error) {
	list := make([]*Domain, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.domains ORDER BY code;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectDomain - получает домен для доступа
func (d *Database) SelectDomain(ctx context.Context, code string) (*Domain, error) {
	item := new(Domain)

	err := d.db.GetContext(ctx, item, `SELECT * FROM auth.domains WHERE code = $1 LIMIT 1;`, code)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return item, nil
}

// InsertDomain - добавляет домен для доступа
func (d *Database) InsertDomain(ctx context.Context, code, name string, description sql.NullString) error {
	_, err := d.db.ExecContext(ctx,
		`INSERT INTO auth.domains(code, "name", "description", "created") VALUES ($1, $2, $3, $4);`,
		code, name, description, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// UpdateDomain - обновляет домен для доступа
func (d *Database) UpdateDomain(ctx context.Context, code, name string, description sql.NullString) error {
	_, err := d.db.ExecContext(ctx,
		`UPDATE auth.domains SET "name" = $2, "description" = $3, "updated" = $4 WHERE code = $1;`,
		code, name, description, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
