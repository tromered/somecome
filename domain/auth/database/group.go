package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Group - группа доступов пользователей
type Group struct {
	// ИД группы
	ID int64 `db:"id"`
	// Название группы
	Name string `db:"name"`
	// Описание группы
	Description sql.NullString `db:"description"`
	// Признак того что пользователя после регистрации необходимо автоматически добавить в группу
	AutoAppend bool `db:"auto_append"`
	// Время создания группы
	Created time.Time `db:"created"`
	// Время обновления группы
	Updated sql.NullTime `db:"updated"`
}

// SelectGroups - получает список групп доступа
func (d *Database) SelectGroups(ctx context.Context) ([]*Group, error) {
	list := make([]*Group, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.groups ORDER BY name;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectGroup - получает группу доступа по ИД
func (d *Database) SelectGroup(ctx context.Context, id int64) (*Group, error) {
	group := new(Group)

	err := d.db.GetContext(ctx, group, `SELECT * FROM auth.groups WHERE id = $1 LIMIT 1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return group, nil
}

// InsertGroup - добавляет новую группу доступа
func (d *Database) InsertGroup(ctx context.Context, name string, description sql.NullString, autoAppend bool) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id,
		`INSERT INTO auth.groups (name, description, auto_append, created) VALUES ($1, $2, $3, $4) RETURNING id;`,
		name, description, autoAppend, time.Now(),
	)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// UpdateGroup - обновляет группу доступа
func (d *Database) UpdateGroup(ctx context.Context, id int64, name string, description sql.NullString, autoAppend bool) error {
	_, err := d.db.ExecContext(ctx,
		`UPDATE auth.groups SET name = $2, description = $3, auto_append = $4, updated = $5 WHERE id = $1;`,
		id, name, description, autoAppend, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// DeleteGroup - удаляет группу доступа
func (d *Database) DeleteGroup(ctx context.Context, id int64) error {
	_, err := d.db.ExecContext(ctx, `DELETE FROM auth.groups WHERE id = $1;`, id)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// SelectAutoAppendGroups - получает список групп доступа с автодобавлением пользователя
func (d *Database) SelectAutoAppendGroups(ctx context.Context) ([]*Group, error) {
	list := make([]*Group, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.groups WHERE auto_append = TRUE ORDER BY name;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}
