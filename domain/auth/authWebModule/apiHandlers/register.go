package apiHandlers

import (
	"net/http"
	userDTO "somecome/domain/user/dto"
	"somecome/superobject"
)

func (o *Object) registerHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		request := new(LoginInput)

		err := wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(r.Context(), w, http.StatusBadRequest, err)

			return
		}

		userID, err := o.superObject.AuthDomain().CreateUser(r.Context(), request.Login, request.Password)
		if err != nil {
			wsd.WriteJSON(r.Context(), w, http.StatusBadRequest, err)

			return
		}

		err = o.superObject.UserDomain().CreateMainInfo(r.Context(), userDTO.MainInfo{
			UserID:      userID,
			OriginLogin: &request.Login,
		})
		if err != nil {
			wsd.WriteJSON(r.Context(), w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteNoContent(w)
	})
}
