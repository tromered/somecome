package apiHandlers

import (
	"errors"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/errs"
	"somecome/superobject"
)

func (o *Object) loginHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		request := new(LoginInput)

		err := wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(r.Context(), w, http.StatusBadRequest, err)

			return
		}

		token, err := o.superObject.AuthDomain().CreateSession(r.Context(), request.Login, request.Password)

		if errors.Is(err, authDTO.LoginOrPasswordIncorrectErr) {
			err = errs.AddText(err, "не корректный логин/пароль")
		}

		if err != nil {
			wsd.WriteJSON(r.Context(), w, http.StatusBadRequest, err)

			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     core.SessionCookieName,
			Value:    token,
			HttpOnly: true,
			Path:     "/",
		})

		wsd.WriteNoContent(w)
	})
}
