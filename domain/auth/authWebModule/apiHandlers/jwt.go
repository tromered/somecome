package apiHandlers

import (
	"errors"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/pkg/errs"
	"somecome/superobject"
)

func (o *Object) createJWTHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		request := new(LoginInput)

		err := wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		token, err := o.superObject.AuthDomain().CreateJWT(ctx, request.Login, request.Password)

		if errors.Is(err, authDTO.LoginOrPasswordIncorrectErr) {
			err = errs.AddText(err, "не корректный логин/пароль")
		}

		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, LoginJWTOutput{
			Token: token,
		})
	})
}
