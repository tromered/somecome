package apiHandlers

type LoginInput struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type LoginJWTOutput struct {
	Token string `json:"token"`
}
