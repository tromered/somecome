package pageHandlers

import "time"

// LevelInfoUnit - данные уровня доступа
type LevelInfoUnit struct {
	// Уровень
	Level int16
	// Название
	Name string
	// Выбрано на данный момент
	Selected bool
}

// Domain - домен для группировки доступов
type DomainWithPermissionsUnit struct {
	// Код домена
	Code string
	// Название домена
	Name string
	// Описание домена
	Description *string

	// Права в домене
	Permissions []PermissionStatusUnit
}

// PermissionStatusUnit - право доступа у пользователя/группы
type PermissionStatusUnit struct {
	// ИД доступа
	ID int64
	// Код доступа
	Code string
	// Название доступа
	Name string
	// Описание доступа
	Description *string

	// Уровень доступа
	Level *int16

	// Время создания доступа для пользователя/группы
	Created *time.Time
	// Время обновления доступа для пользователя/группы
	Updated *time.Time

	// Уровни доступа для права
	Levels []LevelInfoUnit
}

// PermissionEditorTD - данные для рендеринга страницы редактора прав
type PermissionEditorTD struct {
	// Права в доменах
	Domains []DomainWithPermissionsUnit
	// Адрес для сохранения
	SaveLink string
	// Название редактора
	Name string
	// ИД ресурса, если есть
	ResourceID int64
}
