package pageHandlers

import (
	"fmt"
	"net/http"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"strconv"
)

func (o *Object) groupPermissionEditorGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, AuthPermissionEditorPageName)

		groupID, err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		group, err := o.superObject.AuthDomain().GetGroup(ctx, groupID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		permissions, err := o.superObject.AuthDomain().ListGroupPermissions(ctx, groupID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		levels, err := o.superObject.AuthDomain().ListLevels(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := PermissionEditorTD{
			Domains:    fromDomains(permissions, levels),
			SaveLink:   PathToAuthGroupPermissionsEditor,
			Name:       fmt.Sprintf("Права группы \"%s\"", group.Name),
			ResourceID: groupID,
		}
		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) groupPermissionEditorPostHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		err := r.ParseForm()
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		actionType := r.FormValue("at")
		groupID, err := strconv.ParseInt(r.FormValue("resource-id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		permissions, err := toPermissionInput(r.Form)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		err = o.superObject.AuthDomain().SetGroupPermissions(ctx, groupID, permissions)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		switch actionType {
		case "save":
			http.Redirect(w, r, GetAuthGroupPermissionEditorLink(groupID), http.StatusSeeOther)
		default:
			http.Redirect(w, r, PathToAuthGroupList, http.StatusSeeOther)
		}
	})
}
