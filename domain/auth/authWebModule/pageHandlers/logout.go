package pageHandlers

import (
	"net/http"
	"somecome/domain/webserver/core"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"time"
)

func (o *Object) logoutPageGetPost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tdata := wsd.TemplateData(r.Context(), link.EmptyPageTemplateName)

		cookie, err := r.Cookie(core.SessionCookieName)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(r.Context(), w, http.StatusBadRequest, tdata)

			return
		}

		err = o.superObject.AuthDomain().DeleteSession(r.Context(), cookie.Value)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(r.Context(), w, http.StatusBadRequest, tdata)

			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     core.SessionCookieName,
			Value:    "",
			HttpOnly: true,
			Path:     "/",
			Expires:  time.Now(),
		})

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}
