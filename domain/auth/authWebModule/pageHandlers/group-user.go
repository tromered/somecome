package pageHandlers

import (
	"fmt"
	"net/http"
	userDTO "somecome/domain/user/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"strconv"
)

// GroupUserUnit - данные пользователя в списке
type GroupUserUnit struct {
	// Данные пользователя
	Info UserUnit
	// Включен в список
	Included bool
}

// GroupUsersEditorTD - данные для рендеринга редактора списка пользователей группы доступа
type GroupUsersEditorTD struct {
	// ИД ресурса, если есть
	ResourceID int64
	// Пользователи
	Users []GroupUserUnit
	// Ссылка на сохранение
	SaveLink string
	// Название редактора
	Name string
}

// UserUnit - данные пользователя для отображения
type UserUnit struct {
	// ИД пользователя
	ID int64
	// Аватарка пользователя
	AvatarURL *string
	// Отображаемое имя
	DisplayName string
}

func userUnitFromModel(raw *userDTO.PrettyInfo) *UserUnit {
	info := &UserUnit{
		ID:          raw.UserID,
		DisplayName: raw.DisplayName,
	}

	if raw.HasAvatar {
		aurl := link.GetAvatarLink(raw.UserID)
		info.AvatarURL = &aurl
	}

	return info
}

func (o *Object) groupUsersEditorGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, AuthGroupUsersEditorPageName)

		groupID, err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		group, err := o.superObject.AuthDomain().GetGroup(ctx, groupID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		usersInGroup, err := o.superObject.AuthDomain().UsersInGroup(ctx, groupID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		users, err := o.superObject.UserDomain().GetAllPrettyInfo(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := GroupUsersEditorTD{
			SaveLink:   PathToAuthGroupUsersEditor,
			Name:       fmt.Sprintf("Пользователи группы \"%s\"", group.Name),
			ResourceID: groupID,
		}

		for _, user := range users {
			_, inGroup := usersInGroup[user.UserID]
			data.Users = append(data.Users, GroupUserUnit{
				Info:     *userUnitFromModel(user),
				Included: inGroup,
			})
		}

		tdata.SetData(data)
		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) groupUsersEditorPostHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		err := r.ParseForm()
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		actionType := r.FormValue("at")
		groupID, err := strconv.ParseInt(r.FormValue("resource-id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		ids, err := toUsersInput(r.Form, "user-id")
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		err = o.superObject.AuthDomain().SetGroupUsers(ctx, groupID, ids)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		switch actionType {
		case "save":
			http.Redirect(w, r, GetAuthGroupUsersEditorLink(groupID), http.StatusSeeOther)
		default:
			http.Redirect(w, r, PathToAuthGroupList, http.StatusSeeOther)
		}
	})
}
