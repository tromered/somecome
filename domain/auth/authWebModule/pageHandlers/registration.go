package pageHandlers

import (
	"errors"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	userDTO "somecome/domain/user/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/errs"
	"somecome/superobject"
)

type registerPageTD struct {
	// Ссылка на регистрацию
	RegistrationLink string
	// Логин
	Login string
	// Имя
	Firstname string
	// Фамилия
	Secondname string
	// Отчество
	Patronimyc string
}

func (o *Object) registrationPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tdata := wsd.TemplateData(r.Context(), RegistrationPageTemplateName)
		tdata.SetData(registerPageTD{
			RegistrationLink: PathToRegistration,
		})

		wsd.WriteTemplate(r.Context(), w, http.StatusOK, tdata)
	})
}

func (o *Object) registrationPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, RegistrationPageTemplateName)

		login := r.FormValue("login")
		password := r.FormValue("password")
		firstname := r.FormValue("firstname")
		secondname := r.FormValue("secondname")
		patronimyc := r.FormValue("patronimyc")

		tdata.SetData(registerPageTD{
			RegistrationLink: PathToRegistration,
			Login:            login,
			Firstname:        firstname,
			Secondname:       secondname,
			Patronimyc:       patronimyc,
		})

		userID, err := o.superObject.AuthDomain().CreateUser(ctx, login, password)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		mainInfo := userDTO.MainInfo{
			UserID:      userID,
			OriginLogin: &login,
		}

		if firstname != "" {
			mainInfo.Firstname = &firstname
		}

		if secondname != "" {
			mainInfo.Secondname = &secondname
		}

		if patronimyc != "" {
			mainInfo.Patronymic = &patronimyc
		}

		err = o.superObject.UserDomain().CreateMainInfo(ctx, mainInfo)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		token, err := o.superObject.AuthDomain().CreateSession(ctx, login, password)

		if errors.Is(err, authDTO.LoginOrPasswordIncorrectErr) {
			err = errs.AddText(err, "не корректный логин/пароль")
		}

		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     core.SessionCookieName,
			Value:    token,
			HttpOnly: true,
			Path:     "/",
		})

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}
