package authWebModule

import (
	"context"
	"net/http"
	"somecome/domain/auth/authShared"
	"somecome/domain/auth/authWebModule/apiHandlers"
	"somecome/domain/auth/authWebModule/pageHandlers"
	authDTO "somecome/domain/auth/dto"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/domain/webserver/link"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля аутентификации
	ModuleError = errs.NewGroup("auth module")
)

// InitModule - инициализирует модуль авторизации
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		apiHandler:  apiHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль авторизации
type Module struct {
	pageHandler *pageHandlers.Object
	apiHandler  *apiHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "auth" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
	m.apiHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, categoryControl)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var (
	categoryControl = webServerDTO.MenuCategoryInternal{
		Name: "Учетная запись",
		Items: []webServerDTO.MenuItemInternal{
			{
				Link:           link.PathToLogin,
				Name:           "Авторизация",
				Login:          webServerDTO.LoginNoAuthorizedOnly,
				ShowInMainMenu: true,
			},
			{
				Link:  pageHandlers.PathToRegistration,
				Name:  "Регистрация",
				Login: webServerDTO.LoginNoAuthorizedOnly,
			},
			{
				Link: pageHandlers.PathToControlUnauthorized,
				Name: "Права не авторизированных",
				Permissions: webServerDTO.PermissionCheck{
					Enable: true,
					Code:   authShared.AuthPermissionControlPC,
					Level:  authDTO.LevelReadOnly,
				},
			},
			{
				Link: pageHandlers.PathToAuthGroupList,
				Name: "Группы доступа",
				Permissions: webServerDTO.PermissionCheck{
					Enable: true,
					Code:   authShared.AuthPermissionControlPC,
					Level:  authDTO.LevelReadOnly,
				},
			},
		},
		Priority: 190,
	}
)
