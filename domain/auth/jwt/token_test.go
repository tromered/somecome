package jwt

import (
	"context"
	"testing"
	"time"
)

func TestToken(t *testing.T) {
	t.Parallel()

	publicKey, privateKey, err := NewKeyPair()
	if err != nil {
		t.Fatal(err)
	}

	originPayload := TokenPayload{
		AccountID: 1,
		Created:   time.Time{},
	}

	encodedToken, err := Encode(context.TODO(), originPayload, privateKey)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(encodedToken)

	decodedToken, err := Decode(context.TODO(), encodedToken, publicKey)
	if err != nil {
		t.Fatal(err)
	}

	if originPayload.AccountID != decodedToken.Payload.AccountID {
		t.Fatalf(`originPayload.AccountID != decodedToken.Payload.AccountID`)
	}

	if originPayload.Created != decodedToken.Payload.Created {
		t.Fatalf(`originPayload.Created != decodedToken.Payload.Created`)
	}
}

func BenchmarkTokenEncode(b *testing.B) {
	_, privateKey, err := NewKeyPair()
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		originPayload := TokenPayload{
			AccountID: 1,
			Created:   time.Time{},
		}

		_, err := Encode(context.TODO(), originPayload, privateKey)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkTokenParse(b *testing.B) {
	publicKey, privateKey, err := NewKeyPair()
	if err != nil {
		b.Fatal(err)
	}

	originPayload := TokenPayload{
		AccountID: 1,
		Created:   time.Time{},
	}

	encodedToken, err := Encode(context.TODO(), originPayload, privateKey)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, err := Decode(context.TODO(), encodedToken, publicKey)
		if err != nil {
			b.Fatal(err)
		}
	}
}
