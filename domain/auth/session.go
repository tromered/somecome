package auth

import (
	"context"
	"database/sql"
	"errors"
	"somecome/domain/auth/database"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/auth/logic"
	"somecome/pkg/errs"
	"strings"
)

// CreateSession - создает новую сессию пользователя
func (domain *Domain) CreateSession(ctx context.Context, login, password string) (string, error) {
	user, err := domain.checkUser(ctx, login, password)
	if err != nil {
		return "", errs.WrapError(ctx, DomainError, err)
	}

	token := logic.RandomSHA256String()

	err = domain.storage.CreateSession(ctx, &database.Session{
		Token:  token,
		UserID: user.ID,
	})
	if err != nil {
		return "", errs.WrapError(ctx, DomainError, err)
	}

	return token, nil
}

// DeleteSession - удаляет сессию пользователя
func (domain *Domain) DeleteSession(ctx context.Context, token string) error {
	err := domain.storage.CloseSessionByToken(ctx, token)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// checkUser - проверяет данные пользователя
func (domain *Domain) checkUser(ctx context.Context, login, password string) (*database.User, error) {
	login = strings.ToLower(login)

	user, err := domain.storage.GetUserByLogin(ctx, login)

	// Такого пользователя не существует
	if errors.Is(err, sql.ErrNoRows) {
		return nil, authDTO.LoginOrPasswordIncorrectErr
	}

	if err != nil {
		return nil, err
	}

	// Проверка пароля
	if logic.SaltPassword(password, user.Salt) != user.Password {
		return nil, authDTO.LoginOrPasswordIncorrectErr
	}

	return user, nil
}
