package auth

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// UsersInGroup - возвращает карту пользователей в группе
func (domain *Domain) UsersInGroup(ctx context.Context, groupID int64) (map[int64]time.Time, error) {
	users, err := domain.storage.SelectGroupUsers(ctx, groupID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make(map[int64]time.Time)
	for _, user := range users {
		result[user.UserID] = user.Created
	}

	return result, nil
}

// SetGroupUsers - устанавливает список пользователей в группе доступа
func (domain *Domain) SetGroupUsers(ctx context.Context, groupID int64, userIDs []int64) error {
	oldUsers, err := domain.UsersInGroup(ctx, groupID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	// Добавляем пользователей
	for _, userID := range userIDs {
		_, exists := oldUsers[userID]

		if exists {
			delete(oldUsers, userID)
			continue
		}

		err = domain.storage.InsertGroupUser(ctx, groupID, userID)
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}
	}

	// Удаляем пользователей, что были исключены из группы
	for userID := range oldUsers {
		err = domain.storage.DeleteGroupUser(ctx, groupID, userID)
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}
	}

	return nil
}
