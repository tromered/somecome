package auth

import (
	"context"
	"database/sql"
	"errors"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/auth/jwt"
	"somecome/pkg/errs"
	"time"
)

// CreateJWT - создает JWT токен
func (domain *Domain) CreateJWT(ctx context.Context, login, password string) (string, error) {
	user, err := domain.checkUser(ctx, login, password)
	if err != nil {
		return "", errs.WrapError(ctx, DomainError, err)
	}

	encodedToken, err := jwt.Encode(ctx, jwt.TokenPayload{
		AccountID: user.ID,
		Created:   time.Now(),
	}, domain.privateKey)
	if err != nil {
		return "", errs.WrapError(ctx, DomainError, err)
	}

	return encodedToken, nil
}

// GetUserByJWT - возвращает данные пользователя по JWT токену
func (domain *Domain) GetUserByJWT(ctx context.Context, token string) (*authDTO.User, error) {
	dToken, err := jwt.Decode(ctx, token, domain.publicKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	user, err := domain.storage.GetUserByID(ctx, dToken.Payload.AccountID)

	if errors.Is(err, sql.ErrNoRows) {
		return nil, errs.WrapError(ctx, DomainError, authDTO.UserNotFoundErr)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return &authDTO.User{
		ID:      user.ID,
		Login:   user.Login,
		Created: user.Created,
	}, nil
}
