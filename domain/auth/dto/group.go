package authDTO

import "time"

// Group - группа доступов пользователей
type Group struct {
	// ИД группы
	ID int64
	// Название группы
	Name string
	// Описание группы
	Description *string
	// Признак того что пользователя после регистрации необходимо автоматически добавить в группу
	AutoAppend bool
	// Время создания группы
	Created time.Time
	// Количество пользователей в группе
	UserCount int64
	// Количество привилегий у группы
	PermissionCount int64
}
