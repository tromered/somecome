package authDTO

const (
	// LevelForbidden - доступ запрещен
	LevelForbidden int16 = 0
	// LevelReadOnly - доступ только на чтение
	LevelReadOnly int16 = 1
	// LevelReadWrite - доступ на чтение и запись
	LevelReadWrite int16 = 2
	// LevelFullAccess - полный доступ (включая удаление)
	LevelFullAccess int16 = 3
)

// LevelInfo - данные уровня доступа
type LevelInfo struct {
	// Уровень
	Level int16
	// Название
	Name string
}
