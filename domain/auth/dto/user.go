package authDTO

import "time"

// User - данные пользователя
type User struct {
	// ИД пользователя
	ID int64
	// Логин пользователя
	Login string
	// Время создания пользователя (регистрация)
	Created time.Time
}
