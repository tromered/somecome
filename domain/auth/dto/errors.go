package authDTO

import (
	"errors"
)

var (
	// Неверный логин/пароль
	LoginOrPasswordIncorrectErr = errors.New("login or password incorrect")
	// Сессия уже закрыта
	SessionIsClosedErr = errors.New("session is closed")
	// Сессия не существует
	SessionNotFoundErr = errors.New("session not found")
	// Пользователя не существует (не найден)
	UserNotFoundErr = errors.New("user not found")
)
