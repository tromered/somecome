package logic

import (
	"crypto/sha256"
	"fmt"
	"time"
)

func Hash(s string) string { return fmt.Sprintf("%x", sha256.Sum256([]byte(s))) }

func RandomSHA256String() string {
	// TODO: не годится для нагрузок
	return Hash(time.Now().String())
}

func SaltPassword(password, salt string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(password+salt)))
}
