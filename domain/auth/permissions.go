package auth

import (
	"context"
	"errors"
	authDTO "somecome/domain/auth/dto"
	"somecome/pkg/logger"
)

// HasAccessFromCtx - проверяет наличие доступа по данным пользователя из контекста,
// если данных нет, то проверяет доступ как для неавторизованного.
func (domain *Domain) HasAccessFromCtx(ctx context.Context, code string, lv int16) bool {
	user, err := domain.GetUserFromContext(ctx)
	if err != nil && !errors.Is(err, authDTO.UserNotFoundErr) { // В теории такой кейс не возможен
		logger.DebugError(ctx, err)

		return false
	}

	if user != nil {
		return domain.UserHasAccess(ctx, user, code, lv)
	}

	return domain.UnauthorizedHasAccess(ctx, code, lv)
}

// UserHasAccess - проверяет наличие доступа пользователю
func (domain *Domain) UserHasAccess(ctx context.Context, user *authDTO.User, code string, lv int16) bool {
	view, err := domain.storage.GetUserPermissionViewByCode(ctx, user.ID, string(code))
	if err != nil {
		logger.DebugError(ctx, err)
		// Обработка отсутствия данных попадает сюда же - нет данных == нет доступа
		return false
	}

	// Максимальный доступ пользователя ниже требуемого
	if view.Maximum < int16(lv) {
		return false
	}

	return true
}

// UnauthorizedHasAccess - проверяет наличие доступа у не авторизированного пользователя
func (domain *Domain) UnauthorizedHasAccess(ctx context.Context, code string, lv int16) bool {
	view, err := domain.storage.GetPermissionOfUnauthorizedByCode(ctx, string(code))
	if err != nil {
		logger.DebugError(ctx, err)
		// Обработка отсутствия данных попадает сюда же - нет данных == нет доступа
		return false
	}

	// Максимальный доступ пользователя ниже требуемого
	if view.Level < int16(lv) {
		return false
	}

	return true
}
