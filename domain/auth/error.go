package auth

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка аутентификационного домена
	DomainError = errs.NewGroup("auth domain")
)
