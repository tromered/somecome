package webserver

import (
	"context"
	"html/template"
	"os"
	"somecome/pkg/ctxench"
	"somecome/pkg/logger"
)

// getTemplates - возвращает текущий шаблон для рендеринга
func (domain *Domain) getTemplates(ctx context.Context) (*template.Template, error) {
	var (
		err          error
		templateData *template.Template
	)

	if ctxench.GetDeveloperMode(ctx) {
		templateData, err = template.ParseFS(
			os.DirFS(domain.debugTemplateDirPath),
			"*.gohtml",
			"*/*.gohtml",
		)

		// Нет ошибки, парсим модульные шаблоны
		if err == nil {
			err = domain.parseModuleTemplates(ctx, templateData)
		}
	} else {
		templateData, err = domain.embedTemplate.data, domain.embedTemplate.err
	}

	if err != nil {
		return nil, err
	}

	return templateData, nil
}

// parseEmbeded - парсит шаблоны для рендеринга со встроенной файловой системы
func (domain *Domain) parseEmbeded(ctx context.Context) error {
	t, err := template.ParseFS(
		templateDir,
		"template/*.gohtml",
		"template/*/*.gohtml",
	)

	if domain.embedTemplate.err == nil {
		domain.embedTemplate.err = err
	}

	if err != nil {
		return err
	}

	err = domain.parseModuleTemplates(ctx, t)

	if domain.embedTemplate.err == nil {
		domain.embedTemplate.err = err
	}

	if err != nil {
		return err
	}

	domain.embedTemplate.data, domain.embedTemplate.err = t, nil

	return nil
}

// parseModuleTemplates - заполняет шаблоны для рендеринга из модулей
func (domain *Domain) parseModuleTemplates(ctx context.Context, t *template.Template) error {
	for _, module := range domain.modules {
		filler, ok := module.(ModuleTemplater)
		if !ok {
			logger.Debug(ctx, "module:", module.Name(), " - no templater")

			continue
		}

		err := filler.FillTemplates(ctx, t)
		if err != nil {
			return err
		}
	}

	return nil
}
