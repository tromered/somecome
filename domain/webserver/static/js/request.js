async function MakeRequest(url, data = null, method = "GET") {
  function appAlert(text) {
    window.dispatchEvent(
      new CustomEvent("app-runtime-error", {
        detail: { text: text },
      })
    );
  }

  return new Promise((resolve, reject) => {
    let requestInit = {
      method: method,
    };

    if (data != null) {
      requestInit.body = JSON.stringify(data);
    }

    fetch(url, requestInit)
      .then((response) => {
        // No content
        if (response.status == 204) {
          resolve(null);
          return;
        }

        if (response.ok) {
          response
            .json()
            .then((responseJSONData) => resolve(responseJSONData))
            .catch((someError) => {
              appAlert(someError.toString());
              reject({ message: someError.toString() });
            });
        } else {
          response
            .json()
            .then((responseJSONData) => {
              appAlert(responseJSONData.message || responseJSONData.info);
              reject(responseJSONData);
            })
            .catch(() => {
              response
                .text()
                .then((responseTextData) => {
                  appAlert(responseTextData);
                  reject({ message: responseTextData });
                })
                .catch((someError) => {
                  appAlert(someError.toString());
                  reject({ message: someError.toString() });
                });
            });
        }
      })
      .catch((someError) => reject({ message: someError.toString() }));
  });
}

async function PostRequest(url, data = null) {
  return MakeRequest(url, data, "POST");
}

async function GetRequest(url) {
  return MakeRequest(url);
}

async function SendFileRequest(url, file = null, token = null) {
  return new Promise((resolve, reject) => {
    let requestInit = {
      method: "POST",
      headers: {
        "Content-type": file.type,
        "X-Name": file.name,
        "X-Size": file.size,
        "X-Modified": new Date(file.lastModified).toJSON(),
        "X-Token": token,
      },
      body: file,
    };

    fetch(url, requestInit)
      .then((response) => {
        // No content
        if (response.status == 204) {
          resolve(null);
        }
        if (response.ok) {
          response
            .json()
            .then((responseJSONData) => resolve(responseJSONData))
            .catch((someError) => reject({ message: someError.toString() }));
        } else {
          response
            .json()
            .then((responseJSONData) => reject(responseJSONData))
            .catch(() => {
              response
                .text()
                .then((responseTextData) =>
                  reject({ message: responseTextData })
                )
                .catch((someError) =>
                  reject({ message: someError.toString() })
                );
            });
        }
      })
      .catch((someError) => reject({ message: someError.toString() }));
  });
}
