window.addEventListener("load", function () {
  function renderReactionUnit(data, refresher) {
    let mainNode = document.createElement("a");
    mainNode.className = "content-reaction";
    mainNode.onclick = () => {
      PostRequest("/api/reaction/switch", {
        content_key: data.content_key,
        reaction_id: data.reaction_id,
      })
        .then(() => {
          refresher();
        })
        .catch((err) => {
          console.log(err);
        });
    };
    mainNode.setAttribute("used", data.used ? "true" : "false");

    let imageNode = document.createElement("img");
    imageNode.className = "content-reaction";
    imageNode.src = data.preview_link;
    mainNode.appendChild(imageNode);

    let countNode = document.createElement("span");
    countNode.innerText = data.count;
    mainNode.appendChild(countNode);

    return mainNode;
  }

  function renderNewReactionUnit(contentKey, refresher) {
    let newReactionNode = document.createElement("span");
    newReactionNode.className = "new-content-reaction";
    newReactionNode.innerText = "+";

    let fullscreenNode = document.createElement("div");
    fullscreenNode.className = "fullscreen content-reaction-creator";
    fullscreenNode.setAttribute("hide", "");

    newReactionNode.onclick = () => {
      fullscreenNode.removeAttribute("hide");
    };

    let reactionListNode = document.createElement("div");
    reactionListNode.className = "std-box std-column";
    fullscreenNode.appendChild(reactionListNode);

    let reactionUnitsNode = document.createElement("div");
    reactionUnitsNode.className = "std-column";
    reactionListNode.appendChild(reactionUnitsNode);

    let commentNode = document.createElement("input");
    commentNode.className = "std";
    reactionListNode.appendChild(commentNode);

    let selects = [];
    let reactionID = null;

    GetRequest("/api/reaction/list")
      .then((reactions) => {
        reactions.forEach((reaction) => {
          let containerNode = document.createElement("div");
          containerNode.className = "content-reaction-lu";
          reactionUnitsNode.appendChild(containerNode);

          selects.push(() => {
            containerNode.removeAttribute("selected");
          });

          containerNode.onclick = () => {
            selects.forEach((cleaner) => {
              cleaner();
            });

            containerNode.setAttribute("selected", "");
            reactionID = reaction.id;
          };

          let imageNode = document.createElement("img");
          imageNode.className = "content-reaction-lu";
          imageNode.src = reaction.preview_link;
          containerNode.appendChild(imageNode);

          let countNode = document.createElement("span");
          countNode.innerText = reaction.name;
          containerNode.appendChild(countNode);
        });
      })
      .catch((err) => {
        console.log(err);
      });

    let sendNode = document.createElement("button");
    sendNode.className = "std";
    sendNode.innerText = "Отправить";
    sendNode.onclick = () => {
      PostRequest("/api/reaction/create", {
        content_key: contentKey,
        reaction_id: reactionID,
        comment: commentNode.value,
      })
        .then(() => {
          refresher();
        })
        .catch((err) => {
          console.log(err);
        });
    };
    reactionListNode.appendChild(sendNode);

    let cancelNode = document.createElement("button");
    cancelNode.className = "std";
    cancelNode.innerText = "Отмена";
    cancelNode.onclick = () => {
      fullscreenNode.setAttribute("hide", "");
    };
    reactionListNode.appendChild(cancelNode);

    return [newReactionNode, fullscreenNode];
  }

  function refreshOneReaction(node) {
    const ckey = node.getAttribute("content-key");
    PostRequest("/api/reaction/compressed", { content_key: ckey })
      .then((reactions) => {
        node.innerHTML = "";
        node.removeAttribute("unprocessed");

        if (reactions) {
          reactions.forEach((reaction) =>
            node.appendChild(
              renderReactionUnit(reaction, () => refreshOneReaction(node))
            )
          );
        }
        let [newReactionNode, fullscreenNode] = renderNewReactionUnit(
          ckey,
          () => refreshOneReaction(node)
        );
        node.appendChild(newReactionNode);
        node.appendChild(fullscreenNode);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  
  function refreshAll() {
    document
      .querySelectorAll("div.content-reactions[unprocessed]")
      .forEach((node) => {
        refreshOneReaction(node);
      });
  }

  refreshAll();

  window.addEventListener("app-module-reaction-refresh", refreshAll);
});
