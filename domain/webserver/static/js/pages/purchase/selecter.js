function renderSelecter(info, value, onchange = null, asInt = false) {
  let selectNode = document.createElement("select");
  selectNode.className = "std";

  selectNode.onchange = () => {
    if (!onchange) return;
    if (asInt) {
      onchange(parseInt(selectNode.value));
    } else {
      onchange(selectNode.value);
    }
  };

  selectNode.value = value;

  let optionNode = document.createElement("option");
  optionNode.value = asInt ? 0 : "";
  optionNode.innerText = "Не выбрано";

  selectNode.appendChild(optionNode);

  info.forEach((element) => {
    let optionNode = document.createElement("option");
    optionNode.value = element.value;
    optionNode.innerText = element.name;
    optionNode.selected = element.value == value;

    selectNode.appendChild(optionNode);
  });

  return selectNode;
}
