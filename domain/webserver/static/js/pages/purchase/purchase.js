function renderItem(itemData, categoriesInfo, onDelete = null) {
  let trNode = document.createElement("tr");

  let tdNode = null;

  tdNode = document.createElement("td");
  tdNode.innerText = itemData.id || 0;
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.appendChild(
    renderSelecter(
      categoriesInfo,
      itemData.category_id || 0,
      (value) => {
        itemData.category_id = value;
        if (value == 0) {
          delete itemData.category_id;
        }
      },
      true
    )
  );
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");

  let inputName = document.createElement("input");
  inputName.className = "std";
  inputName.type = "text";
  inputName.value = itemData.name || "";
  inputName.onchange = () => {
    itemData.name = inputName.value;
  };

  tdNode.appendChild(inputName);
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");

  let inputPrice = document.createElement("input");
  inputPrice.className = "std purchase-short";
  inputPrice.type = "number";
  inputPrice.step = 0.01;
  inputPrice.value = itemData.price || 0;
  inputPrice.onchange = () => {
    itemData.price = inputPrice.valueAsNumber;
  };

  tdNode.appendChild(inputPrice);
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");

  let inputAmount = document.createElement("input");
  inputAmount.className = "std purchase-short";
  inputAmount.type = "number";
  inputAmount.step = 0.001;
  inputAmount.value = itemData.amount || 0;
  inputAmount.onchange = () => {
    itemData.amount = inputAmount.valueAsNumber;
  };

  tdNode.appendChild(inputAmount);
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");

  let inputSale = document.createElement("input");
  inputSale.className = "std purchase-short";
  inputSale.type = "number";
  inputSale.step = 0.01;
  inputSale.value = itemData.sale || 0;
  inputSale.onchange = () => {
    itemData.sale = inputSale.valueAsNumber;
  };

  tdNode.appendChild(inputSale);
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");

  let inputTotal = document.createElement("input");
  inputTotal.className = "std purchase-short";
  inputTotal.type = "number";
  inputTotal.step = 0.01;
  inputTotal.value = itemData.total || 0;
  inputTotal.onchange = () => {
    itemData.total = inputTotal.valueAsNumber;
  };

  tdNode.appendChild(inputTotal);

  let calcButton = document.createElement("button");
  calcButton.innerText = "C";
  calcButton.className = "std";
  calcButton.onclick = () => {
    let total =
      inputPrice.valueAsNumber * inputAmount.valueAsNumber -
      inputSale.valueAsNumber;

    inputTotal.value = total;

    itemData.total = total;
  };
  tdNode.appendChild(calcButton);

  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.innerText = new Date(itemData.created).toLocaleString() || "";
  trNode.appendChild(tdNode);

  if (onDelete) {
    tdNode = document.createElement("td");
    let removeButton = document.createElement("button");
    removeButton.innerText = "Удалить";
    removeButton.className = "std red";
    removeButton.onclick = () => {
      trNode.remove();
      onDelete();
    };
    tdNode.appendChild(removeButton);
    trNode.appendChild(tdNode);
  }

  return trNode;
}

function renderPayment(paymentData, cardsInfo, onDelete = null) {
  let trNode = document.createElement("tr");

  let tdNode = null;

  tdNode = document.createElement("td");
  tdNode.innerText = paymentData.id || 0;
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.appendChild(
    renderSelecter(
      cardsInfo,
      paymentData.card_id || 0,
      (value) => {
        paymentData.card_id = value;
        if (value == 0) {
          delete paymentData.card_id;
        }
      },
      true
    )
  );

  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");

  let inputTotal = document.createElement("input");
  inputTotal.className = "std";
  inputTotal.type = "number";
  inputTotal.step = 0.01;
  inputTotal.value = paymentData.total || 0;
  inputTotal.onchange = () => {
    paymentData.total = inputTotal.valueAsNumber;
  };

  tdNode.appendChild(inputTotal);
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.innerText = new Date(paymentData.created).toLocaleString() || "";
  trNode.appendChild(tdNode);

  if (onDelete) {
    tdNode = document.createElement("td");
    let removeButton = document.createElement("button");
    removeButton.innerText = "Удалить";
    removeButton.className = "std red";
    removeButton.onclick = () => {
      trNode.remove();
      onDelete();
    };
    tdNode.appendChild(removeButton);
    trNode.appendChild(tdNode);
  }

  return trNode;
}

function renderPurchase(purchaseData, orgsInfo) {
  let trNode = null;
  let tdNode = null;
  let result = [];

  trNode = document.createElement("tr");
  result.push(trNode);
  tdNode = document.createElement("td");
  tdNode.innerText = "ID";
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.innerText = purchaseData.id || 0;
  trNode.appendChild(tdNode);

  trNode = document.createElement("tr");
  result.push(trNode);
  tdNode = document.createElement("td");
  tdNode.innerText = "Организация";
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.appendChild(
    renderSelecter(
      orgsInfo,
      purchaseData.organization_id || 0,
      (value) => {
        purchaseData.organization_id = value;
        if (value == 0) {
          delete purchaseData.organization_id;
        }
      },
      true
    )
  );

  trNode.appendChild(tdNode);

  trNode = document.createElement("tr");
  result.push(trNode);
  tdNode = document.createElement("td");
  tdNode.innerText = "Итого";
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");

  let inputTotal = document.createElement("input");
  inputTotal.className = "std";
  inputTotal.type = "number";
  inputTotal.step = 0.01;
  inputTotal.value = purchaseData.total || 0;
  inputTotal.onchange = () => {
    purchaseData.total = inputTotal.valueAsNumber;
  };

  tdNode.appendChild(inputTotal);

  let calcButton = document.createElement("button");
  calcButton.innerText = "C";
  calcButton.className = "std";
  calcButton.onclick = () => {
    let total = (purchaseData.items || []).reduce(
      (sum, element) => sum + element.total,
      0
    );

    inputTotal.value = total;

    purchaseData.total = total;
  };
  tdNode.appendChild(calcButton);

  trNode.appendChild(tdNode);

  trNode = document.createElement("tr");
  result.push(trNode);
  tdNode = document.createElement("td");
  tdNode.innerText = "Дата покупки";
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.appendChild(
    renderDatetime(
      purchaseData.payment_at || new Date(),
      (value) => {
        purchaseData.payment_at = value;
      },
      true
    )
  );
  trNode.appendChild(tdNode);

  trNode = document.createElement("tr");
  result.push(trNode);
  tdNode = document.createElement("td");
  tdNode.innerText = "Дата создания";
  trNode.appendChild(tdNode);

  tdNode = document.createElement("td");
  tdNode.innerText = new Date(purchaseData.created).toLocaleString() || "";
  trNode.appendChild(tdNode);

  return result;
}
