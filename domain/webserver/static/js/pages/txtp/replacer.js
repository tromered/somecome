window.addEventListener("load", function () {
  function appAlert(text) {
    window.dispatchEvent(
      new CustomEvent("app-runtime-error", {
        detail: { text: text },
      })
    );
  }
  function refreshOneNode(node) {
    const rawText = node.getAttribute("value");
    const dialect = node.getAttribute("dialect");
    // TODO: заменить на нормальную ссылку
    fetch("/pages/txtp/test", {
      body: rawText,
      method: "POST",
      headers: {
        "X-Dialect": dialect,
      },
    })
      .then((response) => {
        response.text().then((text) => {
          if (response.ok) {
            node.innerHTML = text;
            node.removeAttribute("unprocessed");
          } else {
            appAlert(text);
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function refreshAll() {
    document
      .querySelectorAll("div.text-processor-root[unprocessed]")
      .forEach((node) => {
        refreshOneNode(node);
      });
  }

  refreshAll();

  window.addEventListener("app-module-txtp-refresh", refreshAll);
});
