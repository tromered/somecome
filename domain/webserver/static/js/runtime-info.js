window.addEventListener("load", function () {
  function addMessage(text, mType) {
    let rootNode = document.getElementById("main-runtime-info");

    let errNode = document.createElement("div");
    errNode.className = "info";
    errNode.setAttribute("type", mType);
    errNode.innerText = text;

    rootNode.appendChild(errNode);

    setTimeout(() => {
      errNode.remove();
    }, 10 * 1000);
  }

  function addError(event) {
    addMessage(event.detail.text, "error");
  }

  function addSuccess(event) {
    addMessage(event.detail.text, "success");
  }

  window.addEventListener("app-runtime-error", addError);
  window.addEventListener("app-runtime-success", addSuccess);
});

// Пример использования
// window.dispatchEvent(
//   new CustomEvent("app-runtime-error", {
//     detail: { text: text },
//   })
// );

// window.dispatchEvent(
//   new CustomEvent("app-runtime-success", { detail: { text: "Успешно" } })
// );
