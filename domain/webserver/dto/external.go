package webServerDTO

// MenuItem - элемент меню
type MenuItem struct {
	// Ссылка на элемент
	Link string
	// Название элемента
	Name string
}

// MenuCategory - категория меню
type MenuCategory struct {
	// Название категории
	Name string
	// Пункты меню в категории
	Items []MenuItem
}
