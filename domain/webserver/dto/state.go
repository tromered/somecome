package webServerDTO

type LoginParams int

func (lp LoginParams) Skip(Authorized bool) bool {
	if lp == LoginNoAuthorizedOnly && Authorized {
		return true
	}

	if lp == LoginAuthorizedOnly && !Authorized {
		return true
	}

	return false
}

const (
	// Авторизация не имеет значения
	_ LoginParams = iota
	// Только для не авторизированных
	LoginNoAuthorizedOnly
	// Только для авторизированных
	LoginAuthorizedOnly
)

type DebugParams int

func (dp DebugParams) Skip(DebugMode bool) bool {
	if dp == DebugDisableOnly && DebugMode {
		return true
	}

	if dp == DebugEnableOnly && !DebugMode {
		return true
	}

	return false
}

const (
	// Отладка не имеет значения
	_ DebugParams = iota
	// Только при отключенной отладке
	DebugDisableOnly
	// Только при включенной отладке
	DebugEnableOnly
)

// State - текущее состояние для генерации меню
type State struct {
	// Пользователь авторизован
	Authorized bool
	// Состояние отладки
	DebugMode bool
}
