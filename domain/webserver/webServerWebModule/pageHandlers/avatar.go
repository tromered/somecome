package pageHandlers

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	userDTO "somecome/domain/user/dto"
	"somecome/pkg/logger"
	"somecome/superobject"
	"strconv"
)

func (o *Object) avatarGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			userData *authDTO.User
			err      error
		)

		rawID := r.URL.Query().Get("id")

		if rawID != "" {
			userID, parseErr := strconv.ParseInt(rawID, 10, 64)
			if parseErr != nil {
				wsd.WritePlain(r.Context(), w, http.StatusBadRequest, parseErr.Error())

				return
			}

			userData, err = o.superObject.AuthDomain().GetUser(r.Context(), userID)
		} else {
			userData, err = o.superObject.AuthDomain().GetUserFromContext(r.Context())
		}

		if errors.Is(err, authDTO.UserNotFoundErr) {
			wsd.WritePlain(r.Context(), w, http.StatusNotFound, err.Error())

			return
		}
		if err != nil {
			wsd.WritePlain(r.Context(), w, http.StatusInternalServerError, err.Error())

			return
		}

		mainInfo, err := o.superObject.UserDomain().GetMainInfo(r.Context(), userData.ID)
		if errors.Is(err, userDTO.UserNotFoundErr) {
			wsd.WritePlain(r.Context(), w, http.StatusNotFound, err.Error())

			return
		}
		if err != nil {
			wsd.WritePlain(r.Context(), w, http.StatusInternalServerError, err.Error())

			return
		}

		if mainInfo.Avatar == nil {
			wsd.WritePlain(r.Context(), w, http.StatusNotFound, "avatar not set")

			return
		}

		includeBody := r.Method != http.MethodOptions

		o.fileToWeb(wsd, r.Context(), w, *mainInfo.Avatar, false, includeBody)
	})
}

func (o *Object) fileToWeb(wsd superobject.WebServerDomain, ctx context.Context, w http.ResponseWriter, token string, download bool, includeBody bool) {
	info, body, err := o.superObject.FileDomain().GetFile(ctx, token, includeBody)
	if err != nil {
		wsd.WritePlain(ctx, w, http.StatusInternalServerError, err.Error())

		return
	}

	if info.Mime != nil {
		w.Header().Set("Content-Type", *info.Mime)
	}

	if download && info.Name != nil {
		w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="%s"`, *info.Name))
	}

	w.WriteHeader(http.StatusOK)

	if !includeBody {
		return
	}

	if body != nil {
		defer logger.IfErrFunc(ctx, body.Close)

		_, err := io.Copy(w, body)
		logger.IfErr(ctx, err)
	}
}
