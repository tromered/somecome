package pageHandlers

import (
	"net/http"
	"somecome/domain/webserver/link"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToTestingErrors, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.testingErrorPageGet(wsd),
		},
	))
	mux.Handle(PathToTestingDesign, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.testingDesignPageGet(wsd),
		},
	))
	mux.Handle(PathToTestingModules, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.testingModulesPageGet(wsd),
		},
	))

	mux.Handle(PathToMenu, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.menuGetHandler(wsd),
		},
	))

	mux.Handle(link.PathToFile, wsd.PublicCacheMiddleware(o.fileGetHandler(wsd)))
	mux.Handle(link.PathToAvatar, wsd.PublicCacheMiddleware(o.avatarGetHandler(wsd)))
	mux.Handle(link.PathToReaction, wsd.PublicCacheMiddleware(o.reactionGetHandler(wsd)))
}
