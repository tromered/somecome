package pageHandlers

import (
	"fmt"
	"net/http"
	"somecome/domain/webserver/link"
	"somecome/superobject"
)

func (o *Object) testingErrorPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tdata := wsd.TemplateData(r.Context(), link.EmptyPageTemplateName)
		tdata.WithError(fmt.Errorf("error 1"))
		tdata.WithError(fmt.Errorf("error 2"))
		tdata.WithError(fmt.Errorf("error 3"))
		wsd.WriteTemplate(r.Context(), w, http.StatusBadRequest, tdata)
	})
}

func (o *Object) testingDesignPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tdata := wsd.TemplateData(r.Context(), TestingPageTemplateName)
		wsd.WriteTemplate(r.Context(), w, http.StatusBadRequest, tdata)
	})
}
