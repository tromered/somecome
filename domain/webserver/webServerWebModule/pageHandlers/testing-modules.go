package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

// WebModuleInfoUnit - данные для рендеринга информации об веб модуле
type WebModuleInfoUnit struct {
	// Название домена
	DomainName string

	// Есть ли веб модуль
	HasWebModule bool

	// Название веб модуля
	Name string
	// Имеет ли веб модуль обработчики
	HasHandlers bool
	// Имеет ли вкб модуль меню
	HasMenu bool
	// Имеет ли веб модуль шаблоны
	HasTemplate bool
}

// TestingModulesTD - данные для рендеринга информации о состоянии веб сервера
type TestingModulesTD struct {
	// Данные о веб модулях
	WebModules []WebModuleInfoUnit
}

func (o *Object) testingModulesPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ModulesPageTemplateName)

		data := TestingModulesTD{}

		unusedDomains := make(map[string]struct{})

		for _, name := range o.superObject.ActiveDomains(ctx) {
			unusedDomains[name] = struct{}{}
		}

		for _, module := range wsd.ActiveModules(ctx) {
			data.WebModules = append(data.WebModules, WebModuleInfoUnit{
				DomainName:   module.DomainName,
				Name:         module.Name,
				HasWebModule: true,
				HasHandlers:  module.HasHandlers,
				HasMenu:      module.HasMenu,
				HasTemplate:  module.HasTemplate,
			})

			delete(unusedDomains, module.DomainName)
		}

		for name := range unusedDomains {
			data.WebModules = append(data.WebModules, WebModuleInfoUnit{
				DomainName: name,
			})
		}

		tdata.SetData(data)
		wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)
	})
}
