package pageHandlers

import (
	"net/http"
	"somecome/superobject"
	"strconv"
)

func (o *Object) reactionGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rawID := r.URL.Query().Get("id")

		reactionID, err := strconv.ParseInt(rawID, 10, 64)
		if err != nil {
			wsd.WritePlain(r.Context(), w, http.StatusBadRequest, err.Error())

			return
		}

		if err != nil {
			wsd.WritePlain(r.Context(), w, http.StatusInternalServerError, err.Error())

			return
		}

		reaction, err := o.superObject.ReactionDomain().GetReaction(r.Context(), reactionID)
		if err != nil {
			wsd.WritePlain(r.Context(), w, http.StatusInternalServerError, err.Error())

			return
		}

		includeBody := r.Method != http.MethodOptions

		o.fileToWeb(wsd, r.Context(), w, reaction.FileToken, false, includeBody)
	})
}
