package pageHandlers

import "somecome/domain/webserver/link"

const moduleTemplateName = "module:webserver:page"

// Коды страниц для рендеринга шаблонов
var (
	TestingPageTemplateName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:webserver:page:design"}
	ModulesPageTemplateName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:webserver:page:modules"}
	MenuPageTemplateName    = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:webserver:page:menu"}
	MarkdownTemplateName    = link.PageTemplateName{TemplateName: "module:markdown", PageName: ""}
	TestMDPageTemplateName  = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:webserver:page:test-md"}
)

// Ендпоинты веб сервера
const (
	PathToTestingDesign  = "/pages/testing/design"
	PathToTestingModules = "/pages/testing/modules"
	PathToMenu           = "/pages/menu"
	PathToTestingErrors  = "/pages/testing/errors"
)
