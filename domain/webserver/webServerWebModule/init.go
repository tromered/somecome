package webServerWebModule

import (
	"context"
	"net/http"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/domain/webserver/webServerWebModule/pageHandlers"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля тестирования данными
	ModuleError = errs.NewGroup("webserver module")
)

// InitModule - инициализирует модуль тестирования
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль тестирования
type Module struct {
	pageHandler *pageHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "webserver" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, category)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var category = webServerDTO.MenuCategoryInternal{
	Name: "Основное",
	Items: []webServerDTO.MenuItemInternal{
		{
			Link:           pageHandlers.PathToMenu,
			Name:           "Меню",
			ShowInMainMenu: true,
		},
		{
			Link:  pageHandlers.PathToTestingErrors,
			Name:  "Отображение ошибок",
			Debug: webServerDTO.DebugEnableOnly,
		},
		{
			Link:  pageHandlers.PathToTestingDesign,
			Name:  "Тестирование дизайна",
			Debug: webServerDTO.DebugEnableOnly,
		},
		{
			Link:  pageHandlers.PathToTestingModules,
			Name:  "Тестирование модулей",
			Debug: webServerDTO.DebugEnableOnly,
		},
	},
	Priority: 200,
}
