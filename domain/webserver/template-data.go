package webserver

import (
	"encoding/json"
	"somecome/pkg/errs"
)

// UserInfo - информация о пользователе
type UserInfo struct {
	// Логин пользователя
	Login string
	// Отображаемое имя пользователя
	DisplayName string
	// Ссылка на аватар пользователя, если установлен
	AvatarLink *string
	// Имя пользователя
	Firstname *string
	// Фамилия пользователя
	Secondname *string
	// Отчество пользователя
	Patronymic *string
	// Количество непрочитанных сообщений
	UnreadNotificationCount int64
	// ссылка на уведомления
	NotificationLink string
}

// ErrorInfo - данные об ошибке
type ErrorInfo struct {
	// Текст для отображения
	Text string
}

// errorInfoFromError - формирует информацию об ошибке
func errorInfoFromError(err error) ErrorInfo {
	if err == nil {
		return ErrorInfo{}
	}

	var text string

	if errText := errs.GetText(err); errText != nil {
		text = *errText
	} else {
		text = err.Error()
	}

	return ErrorInfo{
		Text: text,
	}
}

// LinksUnit - данные для рендеринга ссылок
type LinksUnit struct {
	// Ссылка для выхода
	Logout string
	// Ссылка для входа
	Login string
}

// TemplateData - данные для рендеринга шаблона
type TemplateData struct {
	// Название шаблона для рендеринга
	TemplateName string
	// Название под-шаблона для рендеринга
	TemplatePageName string
	// Оригинальный путь, для которого был произведен рендеринг
	OriginPath string
	// Меню
	Menu []MenuItem
	// Сырые данные для внутреннего рендеринга
	RawData interface{}
	// Данные пользователя
	UserInfo *UserInfo
	// Данные об ошибках
	ErrorsInfo []ErrorInfo
	// Включенный режим отладки
	Debug bool
	// Ссылки в приложении
	Links LinksUnit
	// Импорты на странице
	Imports ImportsUnit
}

// MenuItem - элемент меню
type MenuItem struct {
	// Ссылка на элемент
	Link string
	// Название элемента
	Name string
}

// ImportsUnit - данные для рендеринга импортов в шапке страницы
type ImportsUnit struct {
	// Импорты CSS
	CSSPaths []string
	// Импорты JS
	JSPaths []string
}

// WithError - добавляет в ответ данные ошибки
func (td *TemplateData) WithError(err error) {
	if err == nil {
		return
	}

	td.ErrorsInfo = append(td.ErrorsInfo, errorInfoFromError(err))
}

// SetData - устанавливает в ответ данные для рендеринга
func (td *TemplateData) SetData(data interface{}) {
	td.RawData = data
}

// Template - название шаблона для рендеринга
func (td *TemplateData) Template() string {
	return td.TemplateName
}

func (td TemplateData) ToDebug() string {
	data, _ := json.MarshalIndent(td, "", "  ")

	return string(data)
}
