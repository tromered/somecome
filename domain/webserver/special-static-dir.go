package webserver

import (
	"bytes"
	"context"
	"errors"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path"
	"somecome/domain/webserver/link"
	"somecome/pkg/ctxench"
	"somecome/pkg/logger"
	"strings"
	"time"
)

// fileSystemWithCustomPath - тип с заменой работы открытия файла (для кастомизации пути)
type fileSystemWithCustomPath struct {
	// Оригинальная файловая система
	origin fs.FS
	// Дополнительный путь для открытия
	path string
}

// Open - реализация интерфейса
func (fs fileSystemWithCustomPath) Open(name string) (fs.File, error) {
	file, err := fs.origin.Open(path.Join(fs.path, name))
	if err != nil {
		return nil, err
	}

	return file, nil
}

// PublicCacheMiddleware - оборачивает ответ заголовками публичного кеша
func (domain *Domain) PublicCacheMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		var liveTime time.Duration = time.Hour

		if ctxench.GetDeveloperMode(ctx) {
			liveTime = time.Minute
		}

		CacheControlMiddleware(next, liveTime, false).ServeHTTP(w, r)
	})
}

func (domain *Domain) rootPathHandler(ctx context.Context) http.Handler {
	var fscp *fileSystemWithCustomPath

	if ctxench.GetDeveloperMode(ctx) {
		fscp = &fileSystemWithCustomPath{
			origin: os.DirFS(domain.debugStaticDirPath),
			path:   "",
		}
	} else {
		fscp = &fileSystemWithCustomPath{
			origin: staticDir,
			path:   "static",
		}
	}

	return domain.mainSeparateHandler(
		domain.mainPageHandler(),
		domain.PublicCacheMiddleware(domain.dir404Handler(fscp)),
	)
}

// dir404Handler - специальный обработчик для статики, совмещенный с обработкой 404 ошибки,
// лютейшая дичь, но что поделать)
func (domain *Domain) dir404Handler(fileSystem *fileSystemWithCustomPath) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		path := r.URL.Path
		logger.Debug(ctx, "dir404 path:", path)

		// Срезаем лидирующий слеш
		path = strings.TrimLeft(path, "/")

		file, err := fileSystem.Open(path)
		if errors.Is(err, fs.ErrNotExist) {
			domain.WriteTemplate(ctx, w, http.StatusNotFound, domain.TemplateData(ctx, link.NotFoundPageTemplateName))

			return
		}

		if err != nil {
			domain.WritePlain(ctx, w, http.StatusBadRequest, err.Error())

			return
		}

		defer logger.IfErrFunc(ctx, file.Close)

		fstat, err := file.Stat()
		if err != nil {
			domain.WritePlain(ctx, w, http.StatusBadRequest, err.Error())

			return
		}

		// Не обрабатываем директории
		if fstat.IsDir() {
			domain.WriteTemplate(ctx, w, http.StatusNotFound, domain.TemplateData(ctx, link.NotFoundPageTemplateName))

			return
		}

		// TODO: надо сделать более аккуратную реализацию, иначе можно улететь по памяти
		rawData, err := io.ReadAll(file)
		if err != nil {
			domain.WritePlain(ctx, w, http.StatusInternalServerError, err.Error())

			return
		}

		buff := bytes.NewReader(rawData)

		http.ServeContent(w, r, fstat.Name(), fstat.ModTime(), buff)
	})
}
