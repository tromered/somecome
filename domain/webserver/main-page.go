package webserver

import (
	"net/http"
	"somecome/domain/webserver/link"
)

func (wsd *Domain) mainPageHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		wsd.WriteTemplate(r.Context(), w, http.StatusOK, wsd.TemplateData(r.Context(), link.MainPageTemplateName))
	})
}
