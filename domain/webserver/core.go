package webserver

import (
	"encoding/json"
	"net"
	"net/http"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"strings"
)

// Ошибка модуля веб ядра
var WebCoreError = errs.NewGroup("web core")

// ParseJSON - парсит тело запроса в переданный объект
func (_ *Domain) ParseJSON(r *http.Request, data interface{}) error {
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		logger.Debug(r.Context(), err)
	}

	return errs.WrapError(r.Context(), WebCoreError, err)
}

// GetIP возвращает реальный ip пользователя
func GetIP(r *http.Request) string {
	if ip := r.Header.Get("X-Real-IP"); ip != "" {
		return ip
	}

	if ips := r.Header.Get("X-Forwarded-For"); ips != "" {
		for _, ip := range strings.Split(ips, ",") {
			return ip
		}
	}

	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		logger.Error(r.Context(), err)

		return ""
	}

	return ip
}
