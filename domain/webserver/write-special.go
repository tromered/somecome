package webserver

import (
	"context"
	"io"
	"net/http"
)

func (_ *Domain) WriteNoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

func (_ *Domain) WritePlain(ctx context.Context, w http.ResponseWriter, statusCode int, text string) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(statusCode)
	_, _ = io.WriteString(w, text)
}
