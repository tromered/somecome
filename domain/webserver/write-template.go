package webserver

import (
	"context"
	"errors"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/user/userWebModule/pageHandlers"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/domain/webserver/link"
	"somecome/pkg/ctxench"
	"somecome/pkg/logger"
)

func (domain *Domain) templateUserInfo(ctx context.Context, user *authDTO.User) (info *UserInfo) {
	if user == nil {
		return nil
	}

	mainInfo, err := domain.obj.UserDomain().GetPrettyInfo(ctx, user.ID)
	if err != nil {
		logger.DebugError(ctx, err)

		return
	}

	count, _ := domain.obj.UserDomain().GetUnreadNotificationCount(ctx, user.ID)

	info = &UserInfo{
		Login:                   mainInfo.Login,
		DisplayName:             mainInfo.DisplayName,
		UnreadNotificationCount: count,
		NotificationLink:        pageHandlers.PathToNotification,
	}

	if mainInfo.HasAvatar {
		avatarLink := link.GetAvatarLink(user.ID)
		info.AvatarLink = &avatarLink
	}

	info.Firstname = mainInfo.Firstname
	info.Secondname = mainInfo.Secondname
	info.Patronymic = mainInfo.Patronymic

	return
}

// TemplateData - создает и наполняет основные данные для шаблона
func (domain *Domain) TemplateData(ctx context.Context, name link.PageTemplateName) webServerDTO.TemplateData {
	user, err := domain.obj.AuthDomain().GetUserFromContext(ctx)
	if err != nil && !errors.Is(err, authDTO.UserNotFoundErr) {
		logger.DebugError(ctx, err)
	}

	cssPaths := []string{
		"/css/main.css",
		"/css/std.css",
		"/css/fullscreen.css",
		"/css/header.css",
		"/css/errors.css",
		"/css/pagination.css",
		"/css/runtime-info.css",
	}
	jsPaths := []string{
		"/js/request.js",
		"/js/pretty-time.js",
		"/js/runtime-info.js",
	}

	// TODO: перенести в сигнатуры и регистрацию/обработку веб модулей
	if domain.HasModule(ctx, "reaction") {
		cssPaths = append(cssPaths, "/css/pages/reaction/main.css")
		jsPaths = append(jsPaths, "/js/pages/reaction/replacer.js")
	}

	// TODO: перенести в сигнатуры и регистрацию/обработку веб модулей
	if domain.HasModule(ctx, "txtp") {
		cssPaths = append(cssPaths, "/css/pages/txtp/markdown.css")
		jsPaths = append(jsPaths, "/js/pages/txtp/replacer.js")
	}

	// TODO: перенести в сигнатуры и регистрацию/обработку веб модулей
	if domain.HasModule(ctx, "comment") {
		cssPaths = append(cssPaths, "/css/pages/comment/main.css")
		jsPaths = append(jsPaths, "/js/pages/comment/replacer.js")
	}

	// TODO: перенести в сигнатуры и регистрацию/обработку веб модулей
	if domain.HasModule(ctx, "content") {
		cssPaths = append(cssPaths, "/css/pages/content/main.css")
		jsPaths = append(jsPaths, "/js/pages/content/replacer.js")
	}

	return &TemplateData{
		TemplateName:     name.TemplateName,
		TemplatePageName: name.PageName,
		OriginPath:       ctxench.GetOriginPath(ctx),
		Menu:             domain.stdMenu(ctx, user),
		UserInfo:         domain.templateUserInfo(ctx, user),
		Debug:            ctxench.GetDebugMode(ctx),
		Links: LinksUnit{
			Logout: link.PathToLogout,
			Login:  link.PathToLogin,
		},
		Imports: ImportsUnit{
			CSSPaths: cssPaths,
			JSPaths:  jsPaths,
		},
	}
}

func (domain *Domain) WriteTemplate(
	ctx context.Context, w http.ResponseWriter, statusCode int, data webServerDTO.TemplateData,
) {
	templateData, err := domain.getTemplates(ctx)
	if err != nil {
		logger.Error(ctx, err)

		return
	}

	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(statusCode)

	err = templateData.ExecuteTemplate(w, data.Template(), data)

	logger.IfErr(ctx, err)
}
