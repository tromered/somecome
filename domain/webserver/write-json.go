package webserver

import (
	"context"
	"encoding/json"
	"net/http"
	"somecome/pkg/ctxench"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
)

type WebErrorTraceUnit struct {
	From   string `json:"from"`
	Method string `json:"method,omitempty"`
}

type WebErrorResponse struct {
	Message *string             `json:"message,omitempty"`
	Info    string              `json:"info"`
	Trace   []WebErrorTraceUnit `json:"trace,omitempty"`
}

func fromTrace(err error) []WebErrorTraceUnit {
	result := make([]WebErrorTraceUnit, 0)

	for _, unit := range errs.GetTrace(err) {
		result = append(result, WebErrorTraceUnit{
			From:   unit.From,
			Method: unit.Method,
		})
	}

	return result
}

func fromError(err error) WebErrorResponse {
	response := WebErrorResponse{
		Info:    err.Error(),
		Message: errs.GetText(err),
		Trace:   fromTrace(err),
	}

	return response
}

func (domain *Domain) WriteJSON(ctx context.Context, w http.ResponseWriter, statusCode int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	enc := json.NewEncoder(w)

	if errData, ok := data.(error); ok {
		data = fromError(errData)
	}

	if ctxench.GetDebugMode(ctx) {
		enc.SetIndent("", "  ")
	}

	if err := enc.Encode(data); err != nil {
		logger.Error(ctx, err)
	}

}
