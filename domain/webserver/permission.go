package webserver

import (
	"errors"
	"net/http"
	"somecome/domain/webserver/link"
)

var hasNotAccessErr = errors.New("access denied")

// PermissionTDHandler - обработчик  прав с рендерингом шаблонной страницы
func (domain *Domain) PermissionTDHandler(next http.Handler, code string, lv int16) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		if domain.obj.AuthDomain().HasAccessFromCtx(ctx, code, lv) {
			next.ServeHTTP(w, r)

			return
		}

		tdata := domain.TemplateData(ctx, link.EmptyPageTemplateName)
		tdata.WithError(hasNotAccessErr)
		domain.WriteTemplate(ctx, w, http.StatusForbidden, tdata)
	})
}

// PermissionJSONHandler - обработчик  прав с JSON ответом
func (domain *Domain) PermissionJSONHandler(next http.Handler, code string, lv int16) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		if domain.obj.AuthDomain().HasAccessFromCtx(ctx, code, lv) {
			next.ServeHTTP(w, r)

			return
		}

		domain.WriteJSON(ctx, w, http.StatusForbidden, hasNotAccessErr)
	})
}
