package webserver

import (
	"context"
	authDTO "somecome/domain/auth/dto"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/ctxench"
	"sort"
)

func (domain *Domain) stdMenu(ctx context.Context, user *authDTO.User) []MenuItem {
	menu := make([]MenuItem, 0)

	for _, el := range domain.FilterMainMenu(
		ctx,
		webServerDTO.State{
			Authorized: user != nil,
			DebugMode:  ctxench.GetDebugMode(ctx),
		},
	) {
		menu = append(menu, MenuItem{
			Link: el.Link,
			Name: el.Name,
		})
	}

	return menu
}

// AddCategory - добавляет раздел в меню
func (domain *Domain) AddCategory(ctx context.Context, category webServerDTO.MenuCategoryInternal) error {
	domain.menu = append(domain.menu, category)

	sortCategory(domain.menu)

	return nil
}

// FilterMainMenu - производит фильтрацию элементов главного меню
func (domain *Domain) FilterMainMenu(ctx context.Context, state webServerDTO.State) []webServerDTO.MenuItem {
	result := []webServerDTO.MenuItem{}

	for _, category := range domain.menu {
		if domain.isSkippedCategory(ctx, category, state) {
			continue
		}

		for _, item := range category.Items {
			if domain.isSkippedItem(ctx, item, state, true) {
				continue
			}

			name := item.Name
			if item.NameInMainMenu != "" {
				name = item.NameInMainMenu
			}

			result = append(result, webServerDTO.MenuItem{
				Link: item.Link,
				Name: name,
			})
		}
	}

	return result
}

// FilterFullMenu - производит фильтрацию элементов полного меню
func (domain *Domain) FilterFullMenu(ctx context.Context, state webServerDTO.State) []webServerDTO.MenuCategory {
	result := []webServerDTO.MenuCategory{}

	for _, category := range domain.menu {
		if domain.isSkippedCategory(ctx, category, state) {
			continue
		}

		resultCategory := webServerDTO.MenuCategory{
			Name: category.Name,
		}

		for _, item := range category.Items {
			if domain.isSkippedItem(ctx, item, state, false) {
				continue
			}

			resultCategory.Items = append(resultCategory.Items, webServerDTO.MenuItem{
				Link: item.Link,
				Name: item.Name,
			})
		}

		if len(resultCategory.Items) > 0 {
			result = append(result, resultCategory)
		}
	}

	return result
}

// isSkippedItem - проверяет требуется ли пропуск элемента меню
func (domain *Domain) isSkippedItem(ctx context.Context, item webServerDTO.MenuItemInternal, state webServerDTO.State, isMain bool) bool {
	if isMain && !item.ShowInMainMenu {
		return true
	}

	if item.Debug.Skip(state.DebugMode) {
		return true
	}

	if item.Login.Skip(state.Authorized) {
		return true
	}

	if item.Permissions.Enable {
		hasAccess := domain.obj.AuthDomain().HasAccessFromCtx(ctx, item.Permissions.Code, item.Permissions.Level)
		if !hasAccess {
			return true
		}
	}

	return false
}

// isSkippedCategory - проверяет требуется ли пропуск категории
func (domain *Domain) isSkippedCategory(ctx context.Context, category webServerDTO.MenuCategoryInternal, state webServerDTO.State) bool {
	if category.Debug.Skip(state.DebugMode) {
		return true
	}

	if category.Login.Skip(state.Authorized) {
		return true
	}

	if category.Permissions.Enable {
		hasAccess := domain.obj.AuthDomain().HasAccessFromCtx(ctx, category.Permissions.Code, category.Permissions.Level)
		if !hasAccess {
			return true
		}
	}

	return false
}

func sortCategory(items []webServerDTO.MenuCategoryInternal) {
	sort.Slice(items, func(i, j int) bool {
		return items[i].Priority > items[j].Priority
	})
}
