package core

const (
	// Максимальный размер файла для загрузки
	MaxFileSize = 50 << 30
	// Токен сессии
	SessionCookieName = "sc-token"
	// Заголовок с JWT
	JWTHeaderName = "X-SomeCome-Auth"
)
