package txtp

import (
	"context"
	"somecome/config"
	"somecome/domain/txtp/txtpWebModule"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

// Domain - домен для работы с текстами
type Domain struct {
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	module *txtpWebModule.Module
}

// Name - название домена
func (_ *Domain) Name() string { return "txtp" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = txtpWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	return nil
}
