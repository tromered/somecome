package pageHandlers

import "somecome/domain/webserver/link"

// Коды страниц для рендеринга шаблонов
var (
	MarkdownTemplateName   = link.PageTemplateName{TemplateName: "module:txtp:markdown", PageName: ""}
	TestMDPageTemplateName = link.PageTemplateName{TemplateName: "module:txtp:page", PageName: "module:txtp:page:test"}
)

// Ендпоинты веб сервера
const (
	PathToTesting = "/pages/txtp/test"
)
