package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToTesting, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.testMDGetHandler(wsd),
			http.MethodPost: o.markdownPostHandler(wsd),
		},
	))
}
