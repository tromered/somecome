package txtp

import "somecome/pkg/errs"

var (
	// Ошибка домена текстовой обработки
	DomainError = errs.NewGroup("text-processor domain")
)
