package txtpDTO

// TextUnit - элемент текста
type TextUnit struct {
	// Значение элемента, если есть
	Value string
	// Тип элемента
	Type UnitType
	// Дочерние элементы
	Children []*TextUnit
	// Размер заголовка, только для типа заголовка
	HeaderNumber int8
	// Элемент имеет перенос строки
	HasLineBreak bool
	// Количество пробелов перед элементом, таб эквивалентен 4-м
	SpaceBefore int
}
