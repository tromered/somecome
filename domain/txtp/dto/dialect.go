package txtpDTO

type Dialect string

const (
	MarkdownBaseDialect   Dialect = "markdown:base"
	MarkdownExtendDialect Dialect = "markdown:extend"
	PlainTextDialect      Dialect = "plain"
)
