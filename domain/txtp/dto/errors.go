package txtpDTO

import "errors"

var (
	// Неизвестный диалект
	UnknownDialectErr = errors.New("unknown dialect error")
)
