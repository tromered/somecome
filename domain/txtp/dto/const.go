package txtpDTO

type UnitType string

const (
	// Корневой элемент
	RootUT UnitType = "root"
	// Простой текст
	PlainTextUT UnitType = "plain"
	// Заголовок
	HeaderUT UnitType = "header"
	// Жирный текст
	BoldTextUT UnitType = "bold"
	// Курсивный текст
	CursiveTextUT UnitType = "cursive"
	// Жирно-курсивный текст
	BoldCursiveTextUT UnitType = "bold-cursive"
	// Вставка кода
	CodeUT UnitType = "code"
	// Многострочный код
	BlockCodeUT UnitType = "block-code"
	// Корневой элемент нумерованного списка
	NumericListRootUT UnitType = "numeric-list-root"
	// Элемент нумерованного списка
	NumericListElementUT UnitType = "numeric-list-element"
	// Корневой элемент маркированного списка
	MarkListRootUT UnitType = "mark-list-root"
	// Элемент маркированного списка
	MarkListElementUT UnitType = "mark-list-element"
	// Цитата
	QuoteUT UnitType = "quote"
	// Блочная цитата
	BlockQuoteUT UnitType = "block-quote"
	// Подчеркнутый текст
	UnderlineTextUT = "underline"
	// Перечеркнутый текст
	StrikeTextUT = "strike"
)
