package txtp

import (
	"context"
	"io"
	txtpDTO "somecome/domain/txtp/dto"
	"somecome/domain/txtp/markdown"
	"somecome/domain/txtp/plain"
	"somecome/pkg/errs"
)

// Parse - разбирает поток на текстовые блоки, согласно указанному диалекту
func (domain *Domain) Parse(ctx context.Context, r io.Reader, dialect txtpDTO.Dialect) ([]*txtpDTO.TextUnit, error) {
	var units []*txtpDTO.TextUnit

	switch dialect {

	case txtpDTO.MarkdownBaseDialect:
		units = markdown.Parse(r, false)

	case txtpDTO.MarkdownExtendDialect:
		units = markdown.Parse(r, true)

	case txtpDTO.PlainTextDialect:
		units = plain.Parse(r)

	default:
		return nil, errs.WrapErrorf(ctx, DomainError, "%w \"%s\"", txtpDTO.UnknownDialectErr, dialect)
	}

	return units, nil
}
