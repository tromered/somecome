package markdown

import (
	txtpDTO "somecome/domain/txtp/dto"
)

func codeBlockParse(row []rune, extendMode bool) (*txtpDTO.TextUnit, bool) {
	spaceCount := spaceDetect(row)
	if spaceCount == len(row) || len(row) < spaceCount+3 {
		return nil, false
	}

	if row[spaceCount] != '`' ||
		row[spaceCount+1] != '`' ||
		row[spaceCount+2] != '`' {
		return nil, false
	}

	value, subs := rowLineParse(row[spaceCount+3:], extendMode)

	unit := &txtpDTO.TextUnit{
		Value:       value,
		SpaceBefore: spaceCount,
		Type:        txtpDTO.BlockCodeUT,
		Children:    subs,
	}

	return unit, true
}
