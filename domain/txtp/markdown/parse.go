package markdown

import (
	"bufio"
	"io"
	txtpDTO "somecome/domain/txtp/dto"
)

func Parse(r io.Reader, extendMode bool) []*txtpDTO.TextUnit {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	tmp := []string{}
	for scanner.Scan() {
		tmp = append(tmp, scanner.Text())
	}

	sc := &CustomScanner{
		data:     tmp,
		position: 0,
		len:      len(tmp),
	}

	root := &txtpDTO.TextUnit{Type: txtpDTO.RootUT}
	parseBlocks(sc, root, extendMode)

	return root.Children
}

func parseBlocks(scanner Scanner, parent *txtpDTO.TextUnit, extendMode bool) {
	for scanner.Scan() {
		rawRow := scanner.Text()
		runnedRow := []rune(rawRow)
		var unit *txtpDTO.TextUnit

		if code, ok := codeBlockParse(runnedRow, extendMode); ok {
			unit = code
			// Особое поведение для блочного кода, повторение его приводит к закрытию
			if parent.Type == txtpDTO.BlockCodeUT {
				break
			}
		} else if quote, ok := quoteBlockParse(runnedRow, extendMode); ok {
			unit = quote
		} else if header, ok := headerBlockParse(runnedRow, extendMode); ok {
			unit = header
		} else if list, ok := listBlockParse(runnedRow, extendMode); ok {
			unit = list
		} else {
			value := rawRow

			var subs []*txtpDTO.TextUnit

			if parent.Type != txtpDTO.BlockCodeUT {
				value, subs = rowLineParse(runnedRow, extendMode)
			}

			unit = &txtpDTO.TextUnit{
				Value:        value,
				Type:         txtpDTO.PlainTextUT,
				HasLineBreak: hasTrueBR(runnedRow),
				SpaceBefore:  spaceDetect(runnedRow),
				Children:     subs,
			}
		}

		newParent := newParent(parent, unit)
		if newParent != nil {
			if needAppendChild(newParent, unit) {
				newParent.Children = append(newParent.Children, unit)
			}

			parent.Children = append(parent.Children, newParent)

			parseBlocks(scanner, newParent, extendMode)

			continue
		}

		if !canChild(parent, unit) {
			scanner.Rollback()

			break
		}

		if needAppendChild(parent, unit) {
			parent.Children = append(parent.Children, unit)
		}
	}
}

type Scanner interface {
	Scan() bool
	Text() string
	Rollback()
}

type CustomScanner struct {
	data     []string
	position int
	len      int
}

func (cs *CustomScanner) Scan() bool {
	return cs.position < cs.len
}

func (cs *CustomScanner) Text() string {

	if cs.position < cs.len {
		v := cs.data[cs.position]
		cs.position++

		return v
	}

	return ""
}

func (cs *CustomScanner) Rollback() {
	if cs.position > 0 {
		cs.position--
	}
}

func newParent(oldParent *txtpDTO.TextUnit, child *txtpDTO.TextUnit) *txtpDTO.TextUnit {
	if child.SpaceBefore < oldParent.SpaceBefore &&
		oldParent.Type != txtpDTO.BlockCodeUT &&
		oldParent.Type != txtpDTO.BlockQuoteUT {
		return nil
	}

	switch child.Type {
	case txtpDTO.MarkListElementUT:
		if oldParent.Type == txtpDTO.MarkListRootUT &&
			oldParent.SpaceBefore < child.SpaceBefore {
			return &txtpDTO.TextUnit{
				Type:        txtpDTO.MarkListRootUT,
				SpaceBefore: child.SpaceBefore,
			}
		}

		if oldParent.Type != txtpDTO.MarkListRootUT {
			return &txtpDTO.TextUnit{
				Type:        txtpDTO.MarkListRootUT,
				SpaceBefore: child.SpaceBefore,
			}
		}

	case txtpDTO.NumericListElementUT:
		if oldParent.Type == txtpDTO.NumericListRootUT &&
			oldParent.SpaceBefore < child.SpaceBefore {
			return &txtpDTO.TextUnit{
				Type:        txtpDTO.NumericListRootUT,
				SpaceBefore: child.SpaceBefore,
			}
		}

		if oldParent.Type != txtpDTO.NumericListRootUT {
			return &txtpDTO.TextUnit{
				Type:        txtpDTO.NumericListRootUT,
				SpaceBefore: child.SpaceBefore,
			}
		}
	case txtpDTO.BlockCodeUT:
		return &txtpDTO.TextUnit{
			Type:  txtpDTO.BlockCodeUT,
			Value: child.Value,
		}
	case txtpDTO.QuoteUT:
		if oldParent.Type == txtpDTO.BlockQuoteUT {
			return nil
		}

		return &txtpDTO.TextUnit{
			Type: txtpDTO.BlockQuoteUT,
		}
	}

	return nil
}

func canChild(parent *txtpDTO.TextUnit, child *txtpDTO.TextUnit) bool {
	switch parent.Type {
	case txtpDTO.RootUT:
		// Корневой элемент может быть родителем для любого элемента
		return true
	case txtpDTO.MarkListRootUT:
		if child.Type != txtpDTO.MarkListElementUT {
			return false
		}
	case txtpDTO.NumericListRootUT:
		if child.Type != txtpDTO.NumericListElementUT {
			return false
		}
	case txtpDTO.BlockQuoteUT:
		return child.Type == txtpDTO.QuoteUT
	}

	if child.SpaceBefore < parent.SpaceBefore {
		return false
	}

	switch child.Type {
	case txtpDTO.MarkListElementUT:
		if parent.Type == txtpDTO.MarkListRootUT {
			return true
		}
	case txtpDTO.NumericListElementUT:
		if parent.Type == txtpDTO.NumericListRootUT {
			return true
		}
	case txtpDTO.HeaderUT:
		// У Заголовка только корневой элемент может быть родителем
		return false
	case txtpDTO.PlainTextUT:
		// Сырой текст может быть дочерним для любого элемента
		return true
	}

	return false
}

func needAppendChild(parent *txtpDTO.TextUnit, child *txtpDTO.TextUnit) bool {
	switch child.Type {
	case txtpDTO.BlockCodeUT:
		return false
	}

	return true
}
