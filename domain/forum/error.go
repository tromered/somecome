package forum

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка домена форума
	DomainError = errs.NewGroup("forum domain")
)
