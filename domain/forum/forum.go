package forum

import (
	"context"
	"fmt"
	"somecome/domain/forum/database"
	forumDTO "somecome/domain/forum/dto"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"somecome/pkg/sqlconvert"
	"strconv"
	"time"
)

// NewForum - создает новый форум и добавляет на него сообщение если оно есть
func (domain *Domain) NewForum(
	ctx context.Context, userID int64, forumName string,
) (int64, error) {
	forumID, err := domain.storage.CreateForum(ctx, forumName, userID)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return forumID, nil
}

// Forums - возвращает список форумов
func (domain *Domain) Forums(ctx context.Context) ([]*forumDTO.Forum, error) {
	rawForums, err := domain.storage.SelectForums(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	list := make([]*forumDTO.Forum, 0)

	for _, rawInfo := range rawForums {
		forum := forumFromModel(rawInfo)

		count, pages, err := domain.superObject.CommentDomain().MessageCount(ctx, forum.ContentKey)
		if err != nil {
			return nil, errs.WrapError(ctx, DomainError, err)
		}

		forum.MessageCount = count
		forum.PageCount = pages

		list = append(list, forum)
	}

	return list, nil
}

// Forum - возвращает данные форума
func (domain *Domain) Forum(ctx context.Context, id int64) (*forumDTO.Forum, error) {
	rawForum, err := domain.storage.SelectForum(ctx, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	forum := forumFromModel(rawForum)

	count, pages, err := domain.superObject.CommentDomain().MessageCount(ctx, forum.ContentKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	forum.MessageCount = count
	forum.PageCount = pages

	return forum, nil
}

func forumFromModel(raw *database.Forum) *forumDTO.Forum {
	return &forumDTO.Forum{
		ID:         raw.ID,
		CreatorID:  raw.CreatorID,
		Name:       raw.Name,
		ContentKey: fmt.Sprintf("%s%d", ckPrefix, raw.ID),
		Created:    raw.Created,
		Updated:    sqlconvert.FromNullTime(raw.Updated),
	}
}

// forumNameToContent - возвращает названеи для контента
func (domain *Domain) forumNameToContent(ctx context.Context, value string) string {
	id, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		logger.DebugError(ctx, err)

		return ""
	}

	forumInfo, err := domain.storage.SelectForum(ctx, id)
	if err != nil {
		logger.DebugError(ctx, err)

		return ""
	}

	return fmt.Sprintf("Форум \"%s\"", forumInfo.Name)
}

// forumDescriptionToContent - возвращает названеи для контента
func (domain *Domain) forumDescriptionToContent(ctx context.Context, value string) string {
	id, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		logger.DebugError(ctx, err)

		return ""
	}

	forumInfo, err := domain.Forum(ctx, id)
	if err != nil {
		logger.DebugError(ctx, err)

		return ""
	}

	days := time.Since(forumInfo.Created) / time.Hour / 24

	return fmt.Sprintf(
		"Существует %d дней, сообщений %d, страниц %d",
		days,
		forumInfo.MessageCount,
		forumInfo.PageCount,
	)
}
