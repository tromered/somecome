package database

import (
	"database/sql"
	"time"
)

// Forum - данные о форуме
type Forum struct {
	// ИД форума
	ID int64 `db:"id"`
	// Ид пользователя создавшего форум
	CreatorID int64 `db:"creator_id"`
	// Название форума
	Name string `db:"name"`
	// Время создания форума
	Created time.Time `db:"created"`
	// Время последнего обновления форума
	Updated sql.NullTime `db:"updated"`
}