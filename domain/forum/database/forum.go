package database

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// CreateForum - создает запись о новом форуме в БД
func (d *Database) CreateForum(ctx context.Context, name string, userID int64) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO forum.forums(creator_id, name, created) VALUES ($1, $2, $3) RETURNING id;`, userID, name, time.Now())
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// SelectForums - получает список форумов
func (d *Database) SelectForums(ctx context.Context) ([]*Forum, error) {
	list := make([]*Forum, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM forum.forums;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectForum - получает форум по его ИД
func (d *Database) SelectForum(ctx context.Context, id int64) (*Forum, error) {
	data := new(Forum)

	err := d.db.GetContext(ctx, data, `SELECT * FROM forum.forums WHERE id = $1 LIMIT 1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return data, nil
}
