package pageHandlers

import (
	"net/http"
	"somecome/pkg/logger"
	"somecome/superobject"
	"strconv"
)

func (o *Object) forumListPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ForumListPageName)

		forums, err := o.superObject.ForumDomain().Forums(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := ForumListTD{
			NewLink: PathToCreateForum,
		}

		for _, raw := range forums {
			data.Forums = append(data.Forums, forumTDFromModel(raw))
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) forumPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ForumPageName)
		pageRaw := r.URL.Query().Get("page")

		var (
			currentPage int64 = 1
		)

		// Если указана страница то обрабатываем ее
		if pageRaw != "" && pageRaw != "last" {
			parsedPage, err := strconv.ParseInt(pageRaw, 10, 64)
			if err != nil {
				logger.DebugError(ctx, err)
			} else {
				currentPage = parsedPage
			}

		}

		forumID, err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		forumInfo, err := o.superObject.ForumDomain().Forum(ctx, forumID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		// Если указана последняя страница, то переходим на нее
		if pageRaw == "last" {
			currentPage = forumInfo.PageCount
		}

		data := ForumPageTD{
			Info:        forumTDFromModel(forumInfo),
			CurrentPage: currentPage,
			ContentKey:  forumInfo.ContentKey,
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
