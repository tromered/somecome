package pageHandlers

import (
	"net/http"
	txtpDTO "somecome/domain/txtp/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/logger"
	"somecome/superobject"
)

// ForumCreateTD - данные для рендеринга страницы создания форума
type ForumCreateTD struct {
	// Ссылка на сохранение форума
	SaveLink string
	// Диалекты сообщения
	Dialects []string
}

func (o *Object) forumCreatePageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ForumCreatePageName)

		tdata.SetData(ForumCreateTD{
			SaveLink: PathToCreateForum,
			Dialects: []string{
				string(txtpDTO.PlainTextDialect),
				string(txtpDTO.MarkdownBaseDialect),
				string(txtpDTO.MarkdownExtendDialect),
			},
		})

		wsd.WriteTemplate(r.Context(), w, http.StatusOK, tdata)
	})
}

func (o *Object) forumCreatePagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ForumCreatePageName)
		tdata.SetData(ForumCreateTD{
			SaveLink: PathToCreateForum,
		})

		err := r.ParseMultipartForm(core.MaxFileSize)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		forumName := r.FormValue("forum-name")

		forumID, err := o.superObject.ForumDomain().NewForum(ctx, userData.ID, forumName)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		logger.Debug(ctx, "forum created:", forumID)

		http.Redirect(w, r, GetForumPageLink(forumID, nil, false), http.StatusSeeOther)
	})
}
