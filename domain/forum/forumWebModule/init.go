package forumWebModule

import (
	"context"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/forum/forumShared"
	"somecome/domain/forum/forumWebModule/pageHandlers"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля форума
	ModuleError = errs.NewGroup("forum module")
)

// InitModule - инициализирует модуль форума
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль форума
type Module struct {
	pageHandler *pageHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "forum" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, category)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var category = webServerDTO.MenuCategoryInternal{
	Name: "Форум",
	Items: []webServerDTO.MenuItemInternal{
		{
			Link:  pageHandlers.PathToCreateForum,
			Name:  "Создать",
			Login: webServerDTO.LoginAuthorizedOnly,
			Permissions: webServerDTO.PermissionCheck{
				Enable: true,
				Code:   forumShared.ForumAccessPC,
				Level:  authDTO.LevelReadWrite,
			},
		},
		{
			Link:           pageHandlers.PathToForumList,
			Name:           "Список",
			NameInMainMenu: "Форумы",
			ShowInMainMenu: true,
		},
	},
	Permissions: webServerDTO.PermissionCheck{
		Enable: true,
		Code:   forumShared.ForumAccessPC,
		Level:  authDTO.LevelReadOnly,
	},
}
