package purchase

import (
	"context"
	purchaseDTO "somecome/domain/purchase/dto"
	"somecome/pkg/errs"
)

// ListOrganizations - список всех организаций
func (domain *Domain) ListOrganizations(ctx context.Context, creatorID int64) ([]*purchaseDTO.Organization, error) {
	data, err := domain.storage.ListOrganizations(ctx, creatorID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*purchaseDTO.Organization, len(data))

	for index, category := range data {
		result[index] = organizationFromRepo(category)
	}

	return result, nil
}

// NewOrganization - новая организация
func (domain *Domain) NewOrganization(ctx context.Context, creatorID int64, name string) (int64, error) {
	id, err := domain.storage.CreateOrganization(ctx, creatorID, name)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}
