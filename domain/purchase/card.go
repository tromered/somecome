package purchase

import (
	"context"
	purchaseDTO "somecome/domain/purchase/dto"
	"somecome/pkg/errs"
)

// ListCard - список всех карт
func (domain *Domain) ListCard(ctx context.Context, creatorID int64) ([]*purchaseDTO.Card, error) {
	data, err := domain.storage.ListCards(ctx, creatorID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*purchaseDTO.Card, len(data))

	for index, category := range data {
		result[index] = cardFromRepo(category)
	}

	return result, nil
}

// NewCard - новая карта
func (domain *Domain) NewCard(ctx context.Context, creatorID int64, name string) (int64, error) {
	id, err := domain.storage.CreateCard(ctx, creatorID, name)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}
