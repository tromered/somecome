package pageHandlers

import (
	"net/url"
	"somecome/domain/webserver/link"
	"strconv"
)

// Ендпоинты веб сервера
const (
	PathToPurchaseList               = "/pages/purchase/list"
	PathToPurchaseEditor             = "/pages/purchase/editor"
	PathToPurchaseCardEditor         = "/pages/purchase/card/editor"
	PathToPurchaseCategoryEditor     = "/pages/purchase/category/editor"
	PathToPurchaseOrganizationEditor = "/pages/purchase/organization/editor"
)

const moduleTemplateName = "module:purchase:page"

// Коды страниц для рендеринга шаблонов
var (
	PurchaseListPageName               = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:purchase:page:purchase-list"}
	PurchaseEditorPageName             = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:purchase:page:purchase-editor"}
	PurchaseCardEditorPageName         = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:purchase:page:card-editor"}
	PurchaseCategoryEditorPageName     = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:purchase:page:category-editor"}
	PurchaseOrganizationEditorPageName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:purchase:page:organization-editor"}
)

// GetPurchasePageLink - генерирует ссылку на страницу списка покупок
func GetPurchasePageLink(pageNumber *int64) string {
	link := url.URL{
		Path: PathToPurchaseList,
	}

	args := url.Values{}

	switch {
	case pageNumber != nil:
		args.Set("page", strconv.FormatInt(*pageNumber, 10))
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetPurchasePageLinkWrapper - враппер для генерации ссылок на список покупок
func GetPurchasePageLinkWrapper() link.PageLinkGetter {
	return func(pageNumber int64) string {
		return GetPurchasePageLink(&pageNumber)
	}
}

// GetPurchaseEditorLink - генерирует ссылку на редактор покупки
func GetPurchaseEditorLink(purchaseID *int64) string {
	link := url.URL{
		Path: PathToPurchaseEditor,
	}

	args := url.Values{}

	if purchaseID != nil {
		args.Set("id", strconv.FormatInt(*purchaseID, 10))
	} else {
		args.Set("id", "new")
	}

	link.RawQuery = args.Encode()

	return link.String()
}
