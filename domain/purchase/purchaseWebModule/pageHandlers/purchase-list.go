package pageHandlers

import (
	"net/http"
	"somecome/pkg/logger"
	"somecome/superobject"
	"strconv"
)

func (o *Object) purchaseListPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, PurchaseListPageName)

		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		pageRaw := r.URL.Query().Get("page")

		var (
			currentPage int64 = 1
		)

		// Если указана страница то обрабатываем ее
		if pageRaw != "" {
			parsedPage, err := strconv.ParseInt(pageRaw, 10, 64)
			if err != nil {
				logger.DebugError(ctx, err)
			} else {
				currentPage = parsedPage
			}

		}

		_, pageCount, err := o.superObject.PurchaseDomain().PurchaseCount(ctx, user.ID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		purchases, err := o.superObject.PurchaseDomain().ListPurchases(ctx, user.ID, currentPage, false)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := PurchaseListTD{
			NewLink: GetPurchaseEditorLink(nil),
			Pages: wsd.GeneratePagination(
				currentPage, pageCount,
				GetPurchasePageLinkWrapper(),
			),
		}

		for _, raw := range purchases {
			data.List = append(data.List, PurchaseListUnit{
				ID:           raw.ID,
				OrgName:      raw.OrganizationName,
				Total:        raw.Total,
				PurchaseDate: raw.PaymentAt,
				Created:      raw.Created,
				EditLink:     GetPurchaseEditorLink(&raw.ID),
			})
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
