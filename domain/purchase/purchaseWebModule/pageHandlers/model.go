package pageHandlers

import (
	webServerDTO "somecome/domain/webserver/dto"
	"time"
)

// PurchaseListUnit - данные покупке в списке покупок
type PurchaseListUnit struct {
	// ИД покупки
	ID int64
	// Название организации
	OrgName string
	// Общая сумма
	Total *float64
	// Дата покупки
	PurchaseDate *time.Time
	// Дата создания
	Created time.Time
	// Ссылка на редактирование
	EditLink string
}

// PurchaseListTD - данные для рендеринга списка покупок
type PurchaseListTD struct {
	// Покупки
	List []PurchaseListUnit
	// Страницы покупок для пагинации
	Pages []webServerDTO.Page
	// Ссылка на создание новой покупки
	NewLink string
}
