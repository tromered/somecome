package purchaseWebModule

import (
	"context"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/purchase/purchaseShared"
	"somecome/domain/purchase/purchaseWebModule/apiHandlers"
	"somecome/domain/purchase/purchaseWebModule/pageHandlers"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля покупок
	ModuleError = errs.NewGroup("purchase module")
)

// InitModule - инициализирует модуль покупок
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		apiHandler:  apiHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль покупок
type Module struct {
	pageHandler *pageHandlers.Object
	apiHandler  *apiHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "purchase" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
	m.apiHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, category)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var category = webServerDTO.MenuCategoryInternal{
	Name: "Покупки",
	Items: []webServerDTO.MenuItemInternal{
		{
			Link:           pageHandlers.PathToPurchaseList,
			Name:           "Список",
			ShowInMainMenu: true,
			NameInMainMenu: "Покупки",
		},
		{
			Link: pageHandlers.PathToPurchaseEditor,
			Name: "Новая",
		},
		{
			Link: pageHandlers.PathToPurchaseCardEditor,
			Name: "Карты",
		},
		{
			Link: pageHandlers.PathToPurchaseCategoryEditor,
			Name: "Категории",
		},
		{
			Link: pageHandlers.PathToPurchaseOrganizationEditor,
			Name: "Организации",
		},
	},
	Login: webServerDTO.LoginAuthorizedOnly,
	Permissions: webServerDTO.PermissionCheck{
		Enable: true,
		Code:   purchaseShared.PurchaseAccessPC,
		Level:  authDTO.LevelReadOnly,
	},
}
