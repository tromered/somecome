package purchase

import (
	"somecome/domain/purchase/database"
	purchaseDTO "somecome/domain/purchase/dto"
	"somecome/pkg/sqlconvert"
)

func cardFromRepo(src *database.Card) *purchaseDTO.Card {
	return &purchaseDTO.Card{
		ID:      src.ID,
		Name:    src.Name,
		Created: src.Created,
		Updated: sqlconvert.FromNullTime(src.Updated),
	}
}

func categoryFromRepo(src *database.Category) *purchaseDTO.Category {
	return &purchaseDTO.Category{
		ID:      src.ID,
		Name:    src.Name,
		Created: src.Created,
		Updated: sqlconvert.FromNullTime(src.Updated),
	}
}

func organizationFromRepo(src *database.Organization) *purchaseDTO.Organization {
	return &purchaseDTO.Organization{
		ID:      src.ID,
		Name:    src.Name,
		Created: src.Created,
		Updated: sqlconvert.FromNullTime(src.Updated),
	}
}

func purchaseFromRepo(src *database.Purchase) *purchaseDTO.Purchase {
	return &purchaseDTO.Purchase{
		ID:             src.ID,
		OrganizationID: sqlconvert.FromNullInt64(src.OrganizationID),
		PaymentAt:      sqlconvert.FromNullTime(src.PaymentAt),
		Total:          sqlconvert.FromNullFloat64(src.Total),
		Created:        src.Created,
		Updated:        sqlconvert.FromNullTime(src.Updated),
	}
}

func purchaseToRepo(src *purchaseDTO.Purchase, creatorID int64) *database.Purchase {
	return &database.Purchase{
		ID:             src.ID,
		CreatorID:      creatorID,
		OrganizationID: sqlconvert.ToNullInt64(src.OrganizationID),
		PaymentAt:      sqlconvert.ToNullTime(src.PaymentAt),
		Total:          sqlconvert.ToNullFloat64(src.Total),
		Created:        src.Created,
		Updated:        sqlconvert.ToNullTime(src.Updated),
	}
}

func itemFromRepo(src *database.PurchaseItem) *purchaseDTO.Item {
	return &purchaseDTO.Item{
		ID:         src.ID,
		PurchaseID: src.PurchaseID,
		CategoryID: sqlconvert.FromNullInt64(src.CategoryID),
		Name:       src.Name,
		Price:      src.Price,
		Amount:     src.Amount,
		Sale:       src.Sale,
		Total:      src.Total,
		Created:    src.Created,
		Updated:    sqlconvert.FromNullTime(src.Updated),
	}
}

func itemToRepo(src *purchaseDTO.Item, creatorID int64) *database.PurchaseItem {
	return &database.PurchaseItem{
		ID:         src.ID,
		CreatorID:  creatorID,
		PurchaseID: src.PurchaseID,
		CategoryID: sqlconvert.ToNullInt64(src.CategoryID),
		Name:       src.Name,
		Price:      src.Price,
		Amount:     src.Amount,
		Sale:       src.Sale,
		Total:      src.Total,
		Created:    src.Created,
		Updated:    sqlconvert.ToNullTime(src.Updated),
	}
}

func paymentFromRepo(src *database.PurchasePayment) *purchaseDTO.Payment {
	return &purchaseDTO.Payment{
		ID:         src.ID,
		PurchaseID: src.PurchaseID,
		CardID:     sqlconvert.FromNullInt64(src.CardID),
		Total:      src.Total,
		Created:    src.Created,
		Updated:    sqlconvert.FromNullTime(src.Updated),
	}
}

func paymentToRepo(src *purchaseDTO.Payment, creatorID int64) *database.PurchasePayment {
	return &database.PurchasePayment{
		ID:         src.ID,
		CreatorID:  creatorID,
		PurchaseID: src.PurchaseID,
		CardID:     sqlconvert.ToNullInt64(src.CardID),
		Total:      src.Total,
		Created:    src.Created,
		Updated:    sqlconvert.ToNullTime(src.Updated),
	}
}
