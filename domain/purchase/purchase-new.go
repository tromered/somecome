package purchase

import (
	"context"
	purchaseDTO "somecome/domain/purchase/dto"
	"somecome/pkg/errs"
)

// NewPurchase - создает данные о покупке
func (domain *Domain) NewPurchase(ctx context.Context, purchase *purchaseDTO.Purchase, creatorID int64) (int64, error) {
	id, err := domain.storage.CreatePurchase(ctx, purchaseToRepo(purchase, creatorID))
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	purchase.ID = id

	err = domain.syncItems(ctx, purchase, creatorID)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	err = domain.syncPayments(ctx, purchase, creatorID)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}

// UpdatePurchase - обновляет данные о покупке
func (domain *Domain) UpdatePurchase(ctx context.Context, purchase *purchaseDTO.Purchase, creatorID int64) error {
	err := domain.storage.UpdatePurchase(ctx, purchaseToRepo(purchase, creatorID))
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	err = domain.syncItems(ctx, purchase, creatorID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	err = domain.syncPayments(ctx, purchase, creatorID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}
