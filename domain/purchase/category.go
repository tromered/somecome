package purchase

import (
	"context"
	purchaseDTO "somecome/domain/purchase/dto"
	"somecome/pkg/errs"
)

// ListCategories - список всех категорий
func (domain *Domain) ListCategories(ctx context.Context, creatorID int64) ([]*purchaseDTO.Category, error) {
	data, err := domain.storage.ListCategories(ctx, creatorID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*purchaseDTO.Category, len(data))

	for index, category := range data {
		result[index] = categoryFromRepo(category)
	}

	return result, nil
}

// NewCategory - новая категория
func (domain *Domain) NewCategory(ctx context.Context, creatorID int64, name string) (int64, error) {
	id, err := domain.storage.CreateCategory(ctx, creatorID, name)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}
