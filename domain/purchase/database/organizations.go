package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Organization - организация.
type Organization struct {
	// ИД в базе
	ID int64 `db:"id"`
	// ИД пользователя создавшего запись
	CreatorID int64 `db:"creator_id"`
	// Название организации
	Name string `db:"name"`
	// Время создания
	Created time.Time `db:"created"`
	// Время последнего обновления
	Updated sql.NullTime `db:"updated"`
}

// CreateOrganization - создает в базе новую запись об организации
func (d *Database) CreateOrganization(ctx context.Context, creatorID int64, name string) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO purchase.organizations (creator_id, name, created) VALUES ($1, $2, $3) RETURNING id;`, creatorID, name, time.Now())
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// ListOrganizations - возвращает список всех организаций в базе
func (d *Database) ListOrganizations(ctx context.Context, creatorID int64) ([]*Organization, error) {
	result := make([]*Organization, 0)

	err := d.db.SelectContext(ctx, &result, `SELECT * FROM purchase.organizations WHERE creator_id = $1 ORDER BY name;`, creatorID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return result, nil
}

// GetOrganizationByID - возвращает организацию по ее ИД
func (d *Database) GetOrganizationByID(ctx context.Context, id int64) (*Organization, error) {
	organization := new(Organization)

	err := d.db.GetContext(ctx, organization, `SELECT * FROM purchase.organizations WHERE id = $1 LIMIT 1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return organization, nil
}
