package content

import (
	"context"
	"somecome/domain/content/database"
	contentDTO "somecome/domain/content/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

// AddTagToContent - добавляет тег контенту
func (domain *Domain) AddTagToContent(ctx context.Context, tagID int64, contentKey string, userID int64) error {
	err := domain.storage.InsertContentTag(ctx, tagID, contentKey, userID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// ContentTags - теги привязанные к контенту
func (domain *Domain) ContentTags(ctx context.Context, contentKey string) ([]*contentDTO.Tag, error) {
	rawTags, err := domain.storage.SelectContentTags(ctx, contentKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	tags := make([]*contentDTO.Tag, 0, len(rawTags))

	for _, tag := range rawTags {
		tags = append(tags, fromModel(tag))
	}

	return tags, nil
}

func fromModel(raw *database.Tag) *contentDTO.Tag {
	return &contentDTO.Tag{
		ID:        raw.ID,
		Tag:       raw.Tag,
		Color:     raw.Color,
		TextColor: raw.TextColor,
		CreatorID: raw.CreatorID,
		Created:   raw.Created,
		Updated:   sqlconvert.FromNullTime(raw.Updated),
	}
}

// ContentByTag - выполняет поиск контента по тегу
func (domain *Domain) ContentByTag(ctx context.Context, tagID int64) ([]*contentDTO.TaggedContent, error) {
	tagInfo, err := domain.storage.SelectTagByID(ctx, tagID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	contentList, err := domain.storage.SelectTaggedContentByTag(ctx, tagInfo.ID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*contentDTO.TaggedContent, 0, len(contentList))

	for _, content := range contentList {
		item := &contentDTO.TaggedContent{
			ContentKey:  content.ContentKey,
			Link:        domain.GetKeyLink(ctx, content.ContentKey),
			Name:        domain.GetKeyName(ctx, content.ContentKey),
			Description: domain.GetKeyDescription(ctx, content.ContentKey),
			TagID:       content.TagID,
		}

		result = append(result, item)
	}

	return result, nil
}
