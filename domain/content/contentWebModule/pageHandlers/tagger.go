package pageHandlers

import (
	"net/http"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"strconv"
)

// TaggerTD - данные для теггера контента
type TaggerTD struct {
	// Теги
	Tags []SimpleTagUnit
	// Ключ контента
	ContentKey string
	// Ссылка для редиректа
	RedirectLink string
	// Ссылка на привязку тега
	SaveLink string
}

func (o *Object) taggerPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, TaggerPageName)

		contentKey := r.URL.Query().Get("content-key")
		redirect := r.URL.Query().Get("redirect")

		data := TaggerTD{
			ContentKey:   contentKey,
			RedirectLink: redirect,
			SaveLink:     PathToTagger,
		}

		tags, err := o.superObject.ContentDomain().GetTags(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		for _, tag := range tags {
			data.Tags = append(data.Tags, fromDTO(tag))
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) taggerPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		contentKey := r.FormValue("content-key")
		redirectTo := r.FormValue("redirect")

		tagID, err := strconv.ParseInt(r.FormValue("tag-id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		err = o.superObject.ContentDomain().AddTagToContent(ctx, tagID, contentKey, userData.ID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		if redirectTo == "" {
			redirectTo = "/"
		}

		http.Redirect(w, r, redirectTo, http.StatusSeeOther)
	})
}
