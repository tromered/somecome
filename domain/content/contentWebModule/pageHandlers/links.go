package pageHandlers

import (
	"net/url"
	"somecome/domain/webserver/link"
	"strconv"
)

const moduleTemplateName = "module:content:page"

// Коды страниц для рендеринга шаблонов
var (
	TagEditorPageName     = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:content:page:tag-editor"}
	TagListPageName       = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:content:page:tag-list"}
	TaggerPageName        = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:content:page:tagger"}
	ContentTagsPageName   = link.PageTemplateName{TemplateName: "module:content:content-tags", PageName: ""}
	ContentSearchPageName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:content:page:content-search"}
)

// Ендпоинты веб сервера
const (
	PathToTagEditor     = "/pages/content/tag/editor"
	PathToTagList       = "/pages/content/tag/list"
	PathToTagger        = "/pages/content/tagger"
	PathToContentTags   = "/pages/content/tags"
	PathToContentSearch = "/pages/content/search"
)

// GetTagEditorLink - генерирует ссылку на редактор тега
func GetTagEditorLink(tagID int64, isNew bool) string {
	link := url.URL{
		Path: PathToTagEditor,
	}

	args := url.Values{
		"id": []string{strconv.FormatInt(tagID, 10)},
	}

	if isNew {
		args.Set("id", "new")
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetTaggerLink - генерирует ссылку на теггер контента
func GetTaggerLink(contentKey string, redirectTo string) string {
	link := url.URL{
		Path: PathToTagger,
	}

	args := url.Values{
		"content-key": []string{contentKey},
		"redirect":    []string{redirectTo},
	}

	link.RawQuery = args.Encode()

	return link.String()
}
