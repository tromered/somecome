package pageHandlers

import (
	"net/http"
	"somecome/superobject"
	"strconv"
)

// ContentUnit - данные контента
type ContentUnit struct {
	// Название контента
	Name string
	// Описание контента
	Description string
	// Ссылка на контент
	Link string
}

// ContentSearchTD - данные для поиска контента
type ContentSearchTD struct {
	// Теги для выбора
	Tags []SimpleTagUnit
	// ИД выбранного тега
	SelectedTagID int64
	// Найденный контент
	Content []ContentUnit
	// Ссылка для поиска
	SearchLink string
}

func (o *Object) contentSearchPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ContentSearchPageName)

		var (
			tagID int64
			err   error
		)

		rawTagID := r.URL.Query().Get("tag-id")

		if rawTagID != "" {
			tagID, err = strconv.ParseInt(rawTagID, 10, 64)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}
		}

		tags, err := o.superObject.ContentDomain().GetTags(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		data := ContentSearchTD{
			SelectedTagID: tagID,
			SearchLink:    PathToContentSearch,
		}

		for _, tag := range tags {
			data.Tags = append(data.Tags, fromDTO(tag))
		}

		if tagID > 0 {
			contentList, err := o.superObject.ContentDomain().ContentByTag(ctx, tagID)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

				return
			}

			for _, content := range contentList {
				item := ContentUnit{
					Name: content.ContentKey,
				}

				if content.Name != nil {
					item.Name = *content.Name
				}

				if content.Link != nil {
					item.Link = *content.Link
				}

				if content.Description != nil {
					item.Description = *content.Description
				}

				data.Content = append(data.Content, item)
			}

		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
