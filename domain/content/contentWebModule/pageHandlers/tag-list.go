package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

// TagUnit - данные тега
type TagUnit struct {
	SimpleTagUnit
	// Ссылка на редактирование тега
	EditorLink string
}

// TagListTD - данные для ренлеринга страницы со списком тегов
type TagListTD struct {
	// Теги
	Tags []TagUnit
	// Ссылка на создание нового тега
	NewLink string
}

func (o *Object) tagListPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, TagListPageName)

		data := TagListTD{
			NewLink: GetTagEditorLink(0, true),
		}

		tags, err := o.superObject.ContentDomain().GetTags(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		for _, tag := range tags {
			data.Tags = append(data.Tags, TagUnit{
				SimpleTagUnit: fromDTO(tag),
				EditorLink:    GetTagEditorLink(tag.ID, false),
			})
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
