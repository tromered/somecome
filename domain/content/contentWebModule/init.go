package contentWebModule

import (
	"context"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/content/contentWebModule/apiHandlers"
	"somecome/domain/content/contentWebModule/pageHandlers"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля контента
	ModuleError = errs.NewGroup("content module")
)

// InitModule - инициализирует модуль контента
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		apiHandler:  apiHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль контента
type Module struct {
	pageHandler *pageHandlers.Object
	apiHandler  *apiHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "content" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
	m.apiHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, category)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var category = webServerDTO.MenuCategoryInternal{
	Name: "Контент",
	Items: []webServerDTO.MenuItemInternal{
		{
			Link: pageHandlers.PathToTagList,
			Name: "Список тегов",
			Permissions: webServerDTO.PermissionCheck{
				Enable: true,
				Code:   authDTO.TestingAlphaAccess,
				Level:  authDTO.LevelReadOnly,
			},
		},
		{
			Link: pageHandlers.PathToContentSearch,
			Name: "Поиск",
			Permissions: webServerDTO.PermissionCheck{
				Enable: true,
				Code:   authDTO.TestingAlphaAccess,
				Level:  authDTO.LevelReadOnly,
			},
		},
	},
}
