package database

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// SelectTaggedContent - получает список привязанных тегов к контенту
func (d *Database) SelectTaggedContent(ctx context.Context, contentKey string) ([]*TaggedContent, error) {
	list := make([]*TaggedContent, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM content.tagged_content WHERE content_key = $1 ORDER BY created;`, contentKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// InsertContentTag - создает запись о привязке тега к контенту в БД
func (d *Database) InsertContentTag(ctx context.Context, tagID int64, contentKey string, userID int64) error {
	_, err := d.db.ExecContext(ctx, `INSERT INTO content.tagged_content(tag_id, content_key, creator_id, created)
	 VALUES ($1, $2, $3, $4);`, tagID, contentKey, userID, time.Now())
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// DeleteContentTag - удаляет запись о привязке тега к контенту в БД
func (d *Database) DeleteContentTag(ctx context.Context, tagID int64, contentKey string) error {
	_, err := d.db.ExecContext(ctx, `DELETE * FROM content.tagged_content WHERE tag_id = $1 content_key = $2;`, tagID, contentKey)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// SelectContentTags - получает список тегов для контента
func (d *Database) SelectContentTags(ctx context.Context, contentKey string) ([]*Tag, error) {
	list := make([]*Tag, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT tags.* FROM content.tagged_content INNER JOIN content.tags ON tags.id = tagged_content.tag_id 
	WHERE content_key = $1 ORDER BY tagged_content.created;`, contentKey)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectTaggedContentByTag - получает список контента по тегу
func (d *Database) SelectTaggedContentByTag(ctx context.Context, tagID int64) ([]*TaggedContent, error) {
	list := make([]*TaggedContent, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM content.tagged_content WHERE tag_id = $1 ORDER BY created;`, tagID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}
