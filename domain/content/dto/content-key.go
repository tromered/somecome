package contentDTO

import "context"

// ContentKey - ключ контента
type ContentKey struct {
	// Префикс ключа
	Prefix string
	// Генерация ссылки по значению ключа
	GetLink func(ctx context.Context, v string) string
	// Генерация названия по значению ключа
	GetName func(ctx context.Context, v string) string
	// Генерация описания по значению ключа
	GetDescription func(ctx context.Context, v string) string
}

// TaggedContent - тегированный контент
type TaggedContent struct {
	// Ключ контента
	ContentKey string
	// Ссылка на контент
	Link *string
	// Название контента
	Name *string
	// Описание контента
	Description *string
	// ИД тега
	TagID int64
}
