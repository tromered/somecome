package file

import (
	"context"
	"errors"
	"os"
	"somecome/config"
	"somecome/domain/file/database"
	"somecome/domain/file/fileWebModule"
	"somecome/pkg/errs"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

// Domain - домен для работы с файлами
type Domain struct {
	// БД
	storage *database.Database
	// Путь для расположения файлов
	path string
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	module *fileWebModule.Module
}

// checkAndInitPath - проверяет и при необходимости создает каталог для сохранения файлов
func checkAndInitPath(ctx context.Context, filesPath string) error {
	info, err := os.Stat(filesPath)
	switch {
	// Нет такой директории
	case os.IsNotExist(err):
		createErr := os.MkdirAll(filesPath, os.ModeDir|os.ModePerm)
		if createErr != nil {
			return createErr
		} else {
			return nil
		}

	// Не получилось собрать информацию
	case err != nil:
		return err

	// По указанному пути не директория
	case !info.IsDir():
		return errors.New("it`s not dir")
	}

	return nil
}

// Name - название домена
func (_ *Domain) Name() string { return "file" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = fileWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	if err := checkAndInitPath(ctx, cfg.System.FileDir); err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	domain.path = cfg.System.FileDir
	domain.storage = database.New(db)

	return nil
}
