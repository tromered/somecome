package file

import (
	"somecome/domain/file/database"
	fileDTO "somecome/domain/file/dto"
	"somecome/pkg/sqlconvert"
)

// FileInfoFromDB - конвертирует данные из модели БД
func FileInfoFromDB(rawInfo *database.File) *fileDTO.FileInfo {
	return &fileDTO.FileInfo{
		Token:   rawInfo.Token,
		Loaded:  rawInfo.Loaded,
		UserID:  rawInfo.UserID,
		Name:    sqlconvert.FromNullString(rawInfo.Name),
		Mime:    sqlconvert.FromNullString(rawInfo.Mime),
		Size:    sqlconvert.FromNullInt64(rawInfo.Size),
		Created: rawInfo.Created,
		Used:    sqlconvert.FromNullTime(rawInfo.Used),
		Updated: sqlconvert.FromNullTime(rawInfo.Updated),
	}
}

// FileInfoToDB - конвертирует данные в модель БД
func FileInfoToDB(rawInfo fileDTO.FileInfo) database.File {
	return database.File{
		Token:   rawInfo.Token,
		Loaded:  rawInfo.Loaded,
		UserID:  rawInfo.UserID,
		Name:    sqlconvert.ToNullString(rawInfo.Name),
		Mime:    sqlconvert.ToNullString(rawInfo.Mime),
		Size:    sqlconvert.ToNullInt64(rawInfo.Size),
		Created: rawInfo.Created,
		Used:    sqlconvert.ToNullTime(rawInfo.Used),
		Updated: sqlconvert.ToNullTime(rawInfo.Updated),
	}
}
