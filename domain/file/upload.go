package file

import (
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"somecome/domain/file/database"
	fileDTO "somecome/domain/file/dto"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"somecome/pkg/sqlconvert"
	"time"
)

// CreateFileToken - подготавливает файл для загрузки
func (domain *Domain) CreateFileToken(ctx context.Context, userID int64) (string, error) {
	// TODO: не годится для нагрузок
	token := fmt.Sprintf("%x", sha256.Sum256([]byte(time.Now().String())))

	err := domain.storage.CreateFileToken(ctx, token, userID)
	if err != nil {
		return "", errs.WrapError(ctx, DomainError, err)
	}

	return token, nil
}

// UploadFile - подготавливает файл для загрузки
func (domain *Domain) UploadFile(ctx context.Context, info fileDTO.FileInfo, r io.Reader) error {
	// Проверяем возможность загрузки файла
	err := domain.checkFileToUpload(ctx, info.Token)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	// Загружаем тело файла
	size, err := domain.uploadFileBody(ctx, info.Token, r)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	// Обновляем информацию в БД
	err = domain.storage.UpdateFileAfterLoad(ctx, database.File{
		Token: info.Token,
		Name:  sqlconvert.ToNullString(info.Name),
		Mime:  sqlconvert.ToNullString(info.Mime),
		Size:  sqlconvert.ToNullInt64(&size),
	})
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// uploadFileBody - загружает тело файла на диск, в специальную директорию
func (domain *Domain) uploadFileBody(ctx context.Context, token string, r io.Reader) (int64, error) {
	file, err := os.Create(domain.filepath(token))
	if err != nil {
		return 0, err
	}

	defer logger.IfErrFunc(ctx, file.Close)

	size, err := io.Copy(file, r)
	if err != nil {
		return 0, err
	}

	return size, nil
}
