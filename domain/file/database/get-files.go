package database

import (
	"context"
	"somecome/pkg/errs"
)

// GetFilesByUserAndMimeMask - возвращает файлы по пользователю и маске типа.
func (d *Database) GetFilesByUserAndMimeMask(ctx context.Context, userID int64, mimeMask string) ([]*File, error) {
	files := make([]*File, 0)

	err := d.db.SelectContext(ctx, &files, `SELECT * FROM file.files WHERE user_id = $1 AND mime ILIKE $2 ORDER BY created;`, userID, mimeMask)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return files, nil
}

// GetFilesByUser - возвращает файлы по пользователю.
func (d *Database) GetFilesByUser(ctx context.Context, userID int64) ([]*File, error) {
	files := make([]*File, 0)

	err := d.db.SelectContext(ctx, &files, `SELECT * FROM file.files WHERE user_id = $1 ORDER BY created;`, userID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return files, nil
}

// GetFilesByUser - возвращает файлы по пользователю с ограничением и сдвигом.
func (d *Database) GetFilesByUserWithLimit(ctx context.Context, userID int64, limit int64, offset int64) ([]*File, error) {
	files := make([]*File, 0)

	err := d.db.SelectContext(ctx, &files, `SELECT * FROM file.files WHERE user_id = $1 ORDER BY created LIMIT $2 OFFSET $3;`, userID, limit, offset)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return files, nil
}
