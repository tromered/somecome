package file

import (
	"context"
	"io"
	"os"
	"path"
	"somecome/domain/file/database"
	fileDTO "somecome/domain/file/dto"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"somecome/pkg/paginator"
)

// GetFile - возвращает данные о файле в хранилище
func (domain *Domain) GetFile(ctx context.Context, token string, includeBody bool) (*fileDTO.FileInfo, io.ReadCloser, error) {
	rawInfo, err := domain.storage.GetFile(ctx, token)
	if err != nil {
		return nil, nil, errs.WrapError(ctx, DomainError, err)
	}

	info := FileInfoFromDB(rawInfo)

	// Нам не требуется содержание файла
	if !includeBody {
		return info, nil, nil
	}

	// Файл не был загружен, но нам нужно его содержание
	if !info.Loaded {
		return nil, nil, errs.WrapError(ctx, DomainError, fileDTO.FileNotLoaded)
	}

	// Обновляем время использования
	logger.IfErr(ctx, domain.storage.UpdateFileUsedTime(ctx, token))

	file, err := os.Open(domain.filepath(token))
	if err != nil {
		return nil, nil, errs.WrapError(ctx, DomainError, err)
	}

	return info, file, nil
}

// filepath - возвращает путь до файла по его токену
func (domain *Domain) filepath(token string) string {
	return path.Join(domain.path, path.Clean(token))
}

// GetUserImages - возвращает список изображений для пользователя из хранилища
func (domain *Domain) GetUserImages(ctx context.Context, userID int64) ([]*fileDTO.FileInfo, error) {
	rawList, err := domain.storage.GetFilesByUserAndMimeMask(ctx, userID, "image/%")
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	list := make([]*fileDTO.FileInfo, 0, len(rawList))

	for _, info := range rawList {
		// Файл не был загружен, пропускаем
		if !info.Loaded {
			continue
		}

		list = append(list, FileInfoFromDB(info))
	}

	return list, nil
}

// GetUserFiles - возвращает список файлов загруженным пользователем в хранилище,
// или данных токенов для загрузки, если файл еще не был загружен на диск (тело файла).
func (domain *Domain) GetUserFiles(ctx context.Context, userID int64, page *int64) ([]*fileDTO.FileInfo, error) {
	var (
		rawList []*database.File
		err     error
	)

	if page != nil {
		limit, offset := paginator.PageToLimit(*page, paginator.DefaultOnPageCount)
		rawList, err = domain.storage.GetFilesByUserWithLimit(ctx, userID, limit, offset)
	} else {
		rawList, err = domain.storage.GetFilesByUser(ctx, userID)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	list := make([]*fileDTO.FileInfo, 0, len(rawList))

	for _, info := range rawList {
		list = append(list, FileInfoFromDB(info))
	}

	return list, nil
}

// GetUserFilesTotal - возвращает сумму занимаемого места и количество файлов пользователя.
func (domain *Domain) GetUserFilesTotal(ctx context.Context, userID int64) (*fileDTO.TotalInfo, error) {
	fileCount, totalSize, err := domain.storage.GetFilesByUserTotal(ctx, userID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	pageCount := paginator.TotalToPages(fileCount, paginator.DefaultOnPageCount)

	info := fileDTO.TotalInfo{
		Count: fileCount,
		Size:  totalSize,
		Pages: pageCount,
	}

	return &info, nil
}
