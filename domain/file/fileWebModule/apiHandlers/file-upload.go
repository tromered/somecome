package apiHandlers

import (
	"bytes"
	"net/http"
	fileDTO "somecome/domain/file/dto"
	"somecome/pkg/logger"
	"somecome/superobject"
)

func (o *Object) fileUploadHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		contentType := r.Header.Get("Content-type")
		xName := r.Header.Get("X-Name")
		xSize := r.Header.Get("X-Size")
		xModified := r.Header.Get("X-Modified")
		// Токен для заливки файла, по факту является ID файла
		xToken := r.Header.Get("X-Token")

		logger.Debug(ctx, "contentType = ", contentType)
		logger.Debug(ctx, "xName = ", xName)
		logger.Debug(ctx, "xSize = ", xSize)
		logger.Debug(ctx, "xModified = ", xModified)
		logger.Debug(ctx, "xToken = ", xToken)

		buff := new(bytes.Buffer)
		trueSize, err := buff.ReadFrom(r.Body)
		r.Body.Close()

		logger.IfErr(ctx, err)

		logger.Debug(ctx, "trueSize = ", trueSize)
		// logger.Debug(ctx, buff.String())

		err = o.superObject.FileDomain().UploadFile(
			ctx,
			fileDTO.FileInfo{
				Token: xToken,
				Name:  &xName,
				Mime:  &contentType,
			},
			buff,
		)
		logger.IfErr(ctx, err)

		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		logger.IfErr(ctx, err)
		logger.DebugData(ctx, user)

		wsd.WriteNoContent(w)
	})
}

func (o *Object) createFileTokenHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		token, err := o.superObject.FileDomain().CreateFileToken(ctx, user.ID)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, token)
	})
}
