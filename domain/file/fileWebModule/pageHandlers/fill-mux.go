package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToFileUpload, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.fileUploadPageGet(wsd),
			http.MethodPost: o.fileUploadPagePost(wsd),
		},
	))
	mux.Handle(PathToFileList, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.fileListPageGet(wsd),
			http.MethodPost: o.fileListPagePost(wsd),
		},
	))
}
