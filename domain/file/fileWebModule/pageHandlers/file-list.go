package pageHandlers

import (
	"fmt"
	"net/http"
	fileDTO "somecome/domain/file/dto"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/domain/webserver/link"
	"somecome/pkg/logger"
	"somecome/superobject"
	"strconv"
	"strings"
	"time"
)

const (
	fileSizeKB int64 = 1 << 10
	fileSizeMB int64 = 1 << 20
	fileSizeGB int64 = 1 << 30
)

// FileListTD - данные для страницы со списком файлов
type FileListTD struct {
	// Список файлов
	List []FileInfoUnit
	// Общее занимаемое место
	Total TotalInfoUnit
	// Страницы списка файлов
	Pages []webServerDTO.Page
	// Адрес для сохранения
	SaveLink string
}

// TotalInfoUnit - данные о занимаемом файлами пользователя месте
type TotalInfoUnit struct {
	// Размер файлов
	Size int64
	// Форматированный для вывода пользователю размер файлов
	PrettySize string
	// Количество файлов
	Count int64
}

// FileInfoUnit - данные файла
type FileInfoUnit struct {
	// Адрес хранения файла
	URL string
	// Это изображение
	IsImage bool
	// Форматированный для вывода пользователю размер файла
	PrettySize string

	// Токен файла
	Token string
	// Файл загружен
	Loaded bool
	// Ид пользователя в базе
	UserID int64
	// Название файла
	Name *string
	// MIME тип файла
	Mime *string
	// Размер файла
	Size *int64
	// Время создания файла
	Created time.Time
	// Время последнего использования (обращения) файла
	Used *time.Time
	// Время последнего обновления файла
	Updated *time.Time
}

func fileInfoUnitFromModel(raw *fileDTO.FileInfo) FileInfoUnit {
	info := FileInfoUnit{
		Token:   raw.Token,
		URL:     link.GetFileLink(raw.Token, false),
		Created: raw.Created,
		Loaded:  raw.Loaded,
		UserID:  raw.UserID,
		Name:    raw.Name,
		Mime:    raw.Mime,
		Size:    raw.Size,
		Used:    raw.Used,
		Updated: raw.Updated,
	}

	if raw.Mime != nil {
		// TODO: перенести в домен
		info.IsImage = strings.HasPrefix(*raw.Mime, "image/")
	}

	if raw.Size != nil {
		info.PrettySize = prettyFileSize(*raw.Size)
	}

	return info
}

func (o *Object) fileListPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, FileListPageTemplateName)

		pageRaw := r.URL.Query().Get("page")

		var (
			currentPage int64 = 1
		)

		// Если указана страница то обрабатываем ее
		if pageRaw != "" {
			parsedPage, err := strconv.ParseInt(pageRaw, 10, 64)
			if err != nil {
				logger.DebugError(ctx, err)
			} else {
				currentPage = parsedPage
			}

		}

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		tdRaw := FileListTD{
			SaveLink: PathToFileList,
		}

		files, err := o.superObject.FileDomain().GetUserFiles(ctx, userData.ID, &currentPage)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		totalInfo, err := o.superObject.FileDomain().GetUserFilesTotal(ctx, userData.ID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		tdRaw.Total.Count = totalInfo.Count
		tdRaw.Total.Size = totalInfo.Size
		tdRaw.Total.PrettySize = prettyFileSize(totalInfo.Size)

		tdRaw.List = make([]FileInfoUnit, 0, len(files))
		for _, fileInfo := range files {
			tdRaw.List = append(tdRaw.List, fileInfoUnitFromModel(fileInfo))
		}

		tdRaw.Pages = wsd.GeneratePagination(
			currentPage, totalInfo.Pages,
			GetFileListLinkWrapper(),
		)

		tdata.SetData(tdRaw)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func prettyFileSize(size int64) string {
	var (
		d    int64
		unit string
	)

	switch {
	case size > fileSizeGB:
		d = fileSizeGB
		unit = "GB"
	case size > fileSizeMB:
		d = fileSizeMB
		unit = "MB"
	case size > fileSizeKB:
		d = fileSizeKB
		unit = "KB"
	default:
		return fmt.Sprintf("%d B", size)
	}

	v := float64(size) / float64(d)

	return fmt.Sprintf("%.2f %s", v, unit)
}
