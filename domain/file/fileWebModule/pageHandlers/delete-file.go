package pageHandlers

import (
	"net/http"
	"somecome/domain/webserver/link"
	"somecome/superobject"
)

func (o *Object) fileListPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		token := r.FormValue("token")

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		err = o.superObject.FileDomain().DeleteUserFile(ctx, token, userData.ID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		http.Redirect(w, r, PathToFileList, http.StatusSeeOther)
	})
}
