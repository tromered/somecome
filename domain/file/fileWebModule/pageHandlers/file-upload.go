package pageHandlers

import (
	"context"
	"errors"
	"fmt"
	"mime/multipart"
	"net/http"
	fileDTO "somecome/domain/file/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/logger"
	"somecome/superobject"
)

// FileUploadTD - данные для рендеринга формы загрузки файла
type FileUploadTD struct {
	// Ссылка для сохранение данных
	SaveLink string
}

func (o *Object) fileUploadPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tdata := wsd.TemplateData(r.Context(), FilePageTemplateName)
		tdata.SetData(FileUploadTD{
			SaveLink: PathToFileUpload,
		})

		wsd.WriteTemplate(r.Context(), w, http.StatusOK, tdata)
	})
}

func (o *Object) fileUploadPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, FilePageTemplateName)
		tdata.SetData(FileUploadTD{
			SaveLink: PathToFileUpload,
		})

		err := r.ParseMultipartForm(core.MaxFileSize)
		if errors.Is(err, multipart.ErrMessageTooLarge) {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusRequestEntityTooLarge, tdata)

			return
		}

		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		token := r.FormValue("token")
		files := r.MultipartForm.File["file"]

		if len(files) < 1 {
			tdata.WithError(core.NoFileToUploadErr)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		if token != "" && len(files) > 1 {
			tdata.WithError(core.MultipleFilesWithOneTokenErr)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		hasErrors := false

		for _, fileHead := range files {
			_, err := uploadFile(o.superObject, ctx, token, fileHead)

			if err != nil {
				hasErrors = true
				filename := fileHead.Filename
				tdata.WithError(fmt.Errorf("file %s not upload: %w", filename, err))
			}
		}

		if hasErrors {
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

// uploadFile - загружает файл в хранилище
func uploadFile(wsd superobject.SuperObject, ctx context.Context, token string, formFile *multipart.FileHeader) (string, error) {
	user, err := wsd.AuthDomain().GetUserFromContext(ctx)
	if err != nil {
		return "", err
	}

	if formFile.Size > core.MaxFileSize {
		return "", core.FileToLargeErr
	}

	name := formFile.Filename
	mime := formFile.Header.Get("Content-Type")
	fileBody, err := formFile.Open()
	if err != nil {
		return "", err
	}

	defer logger.IfErrFunc(ctx, fileBody.Close)

	token, err = wsd.FileDomain().UploadFileFull(
		ctx,
		user.ID,
		fileDTO.FileInput{
			Token: token,
			Name:  name,
			Mime:  mime,
			Body:  fileBody,
		},
	)
	if err != nil {
		return "", err
	}

	return token, nil
}
