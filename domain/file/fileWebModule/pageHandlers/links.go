package pageHandlers

import (
	"net/url"
	"somecome/domain/webserver/link"
	"strconv"
)

const moduleTemplateName = "module:file:page"

// Коды страниц для рендеринга шаблонов
var (
	FilePageTemplateName     = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:file:page:upload"}
	FileListPageTemplateName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:file:page:file-list"}
)

// Ендпоинты веб сервера
const (
	PathToFileUpload = "/pages/file/upload"
	PathToFileList   = "/pages/file/list"
)

// GetFileListLink - генерирует ссылку на страницу списка файлов
func GetFileListLink(pageNumber *int64) string {
	link := url.URL{
		Path: PathToFileList,
	}

	args := url.Values{}

	if pageNumber != nil {
		args.Set("page", strconv.FormatInt(*pageNumber, 10))
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetFileListLinkWrapper - враппер для генерации ссылок на страницы списка файлов
func GetFileListLinkWrapper() link.PageLinkGetter {
	return func(pageNumber int64) string {
		return GetFileListLink(&pageNumber)
	}
}
