package fileDTO

import (
	"io"
	"time"
)

// FileInfo - хранит сведенья о файле
type FileInfo struct {
	// Токен файла
	Token string
	// Файл загружен
	Loaded bool
	// Ид пользователя в базе
	UserID int64
	// Название файла
	Name *string
	// MIME тип файла
	Mime *string
	// Размер файла
	Size *int64
	// Время создания файла
	Created time.Time
	// Время последнего использования (обращения) файла
	Used *time.Time
	// Время последнего обновления файла
	Updated *time.Time
}

// FileInput - данные для загрузки файла
type FileInput struct {
	// Токен файла
	Token string
	// Название файла
	Name string
	// MIME тип файла
	Mime string
	// Тело файла
	Body io.Reader
}

// TotalInfo - общая информация о файлах
type TotalInfo struct {
	// Количество файлов
	Count int64
	// Общий размер файлов
	Size int64
	// Количество страниц
	Pages int64
}
