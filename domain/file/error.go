package file

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка файлового домена
	DomainError = errs.NewGroup("file domain")
)
