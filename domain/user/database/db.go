package database

import (
	"somecome/pkg/errs"

	"github.com/jmoiron/sqlx"
)

var DatabaseErr = errs.NewGroup("user-info database")

type Database struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Database {
	return &Database{
		db: db,
	}
}
