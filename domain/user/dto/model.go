package userDTO

import (
	"io"
	"time"
)

// MainInfo - основные данные пользователя
type MainInfo struct {
	// ИД пользователя
	UserID int64
	// Оригинальный логин пользователя
	OriginLogin *string
	// Имя пользователя
	Firstname *string
	// Фамилия пользователя
	Secondname *string
	// Отчество пользователя
	Patronymic *string
	// Токен аватара пользователя
	Avatar *string
	// Время создания записи
	Created time.Time
	// Время последнего обновления записи
	Updated *time.Time
}

// PrettyInfo - основные данные пользователя
type PrettyInfo struct {
	// ИД пользователя
	UserID int64
	// Логин пользователя
	Login string
	// Отображаемое имя пользователя
	DisplayName string
	// Имя пользователя
	Firstname *string
	// Фамилия пользователя
	Secondname *string
	// Отчество пользователя
	Patronymic *string
	// Установлен ли аватар
	HasAvatar bool
}

// AvatarInput - данные для загрузки аватара
type AvatarInput struct {
	// Токен файла
	Token string
	// Название файла
	Name string
	// MIME тип файла
	Mime string
	// Тело файла
	Body io.Reader
}

// UpdateMainInfoInput - данные пользователя для обновления
type UpdateMainInfoInput struct {
	// Оригинальный логин пользователя
	OriginLogin string
	// Имя пользователя
	Firstname string
	// Фамилия пользователя
	Secondname string
	// Отчество пользователя
	Patronymic string
}
