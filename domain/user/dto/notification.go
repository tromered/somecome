package userDTO

import "time"

// Notification - данные для уведомления
type Notification struct {
	// ИД уведомления
	ID int64
	// ИД пользователя получившего уведомление
	UserID int64
	// Заголовок уведомления
	Title string
	// Текст уведомления
	Message string
	// Ключ контента
	ContentKey *string
	// Ссылка в уведомлении
	Link *string
	// Текст ссылки
	LinkText *string
	// Уведомление прочитано
	IsRead bool
	// Время создания уведомления
	Created time.Time
}

// Notifications - данные уведомлений
type Notifications struct {
	// Уведомления
	Items []*Notification
	// Общее количество уведомлений
	Count int64
	// КОличество не прочитанных уведомлений
	UnreadCount int64
	// Количество страниц
	PageCount int64
}

// NotificationInput - данные для создания уведомления
type NotificationInput struct {
	// ИД пользователя получившего уведомление
	UserID int64
	// Заголовок уведомления
	Title string
	// Текст уведомления
	Message string
	// Ключ контента
	ContentKey string
	// Ссылка в уведомлении
	Link string
	// Текст ссылки
	LinkText string
}
