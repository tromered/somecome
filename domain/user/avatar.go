package user

import (
	"context"
	"database/sql"
	"errors"
	fileDTO "somecome/domain/file/dto"
	"somecome/domain/user/database"
	userDTO "somecome/domain/user/dto"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"somecome/pkg/sqlconvert"
)

// UpdateUserAvatar - обновляет аватар пользователя
func (domain *Domain) UpdateUserAvatar(ctx context.Context, userID int64, fileToken string) error {
	user, err := domain.storage.GetUserInfoByID(ctx, userID)

	// Данных нет в базе, создаем
	if errors.Is(err, sql.ErrNoRows) {
		createErr := domain.storage.CreateUserInfo(ctx, &database.UserInfo{
			UserID: userID,
			Avatar: sqlconvert.ToNullString(&fileToken),
		})
		if createErr != nil {
			return errs.WrapError(ctx, DomainError, createErr)
		}

		return nil
	}

	// Прочие ошибки
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	user.Avatar = sqlconvert.ToNullString(&fileToken)

	// Данные есть, обновляем
	err = domain.storage.UpdateUserInfo(ctx, user)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// UploadUserAvatar - загружает новый аватар пользователя
func (domain *Domain) UploadUserAvatar(ctx context.Context, userID int64, fileInfo userDTO.AvatarInput) error {
	var (
		fileTokenToDelete string
		hasOldAvatar      bool
	)

	user, err := domain.storage.GetUserInfoByID(ctx, userID)

	// Проблемы с данными, дальнейшие действия невозможны
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return errs.WrapError(ctx, DomainError, err)
	}

	// У пользователя был установлен другой аватар
	if user != nil && user.Avatar.Valid {
		hasOldAvatar = true
		fileTokenToDelete = user.Avatar.String
	}

	// Загружаем новый файл
	fileToken, err := domain.superObject.FileDomain().UploadFileFull(ctx, userID, fileDTO.FileInput{
		Token: fileInfo.Token,
		Name:  fileInfo.Name,
		Mime:  fileInfo.Mime,
		Body:  fileInfo.Body,
	})
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	// Обновляем аватар
	err = domain.UpdateUserAvatar(ctx, userID, fileToken)
	if err != nil {
		// Удаляем новый файл из хранилища, поскольку его не получилось установить пользователю
		logger.IfErr(ctx, domain.superObject.FileDomain().DeleteFile(ctx, fileToken))

		return errs.WrapError(ctx, DomainError, err)
	}

	// Нет старого аватара, дальнейшие действия не требуются
	if !hasOldAvatar {
		return nil
	}

	// Удаляем старый аватар из хранилища
	err = domain.superObject.FileDomain().DeleteFile(ctx, fileTokenToDelete)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}
