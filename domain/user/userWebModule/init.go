package userWebModule

import (
	"context"
	"net/http"
	"somecome/domain/user/userWebModule/apiHandlers"
	"somecome/domain/user/userWebModule/pageHandlers"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля с пользовательскими данными
	ModuleError = errs.NewGroup("user module")
)

// InitModule - инициализирует модуль пользовательских данных
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		apiHandler:  apiHandlers.Init(obj),
		domainName:  domainName,
	}
}

// Module - модуль пользовательских данных
type Module struct {
	pageHandler *pageHandlers.Object
	apiHandler  *apiHandlers.Object
	domainName  string
}

// Name - название модуля
func (_ *Module) Name() string { return "user" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
	m.apiHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, category)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var category = webServerDTO.MenuCategoryInternal{
	Name: "Профиль",
	Items: []webServerDTO.MenuItemInternal{
		{
			Link:  pageHandlers.PathToUserInfo,
			Name:  "Информация",
			Login: webServerDTO.LoginAuthorizedOnly,
		},
		{
			Link:  pageHandlers.PathToUserEdit,
			Name:  "Редактирование",
			Login: webServerDTO.LoginAuthorizedOnly,
		},
		{
			Link:  pageHandlers.PathToChangeAvatar,
			Name:  "Загрузка аватара",
			Login: webServerDTO.LoginAuthorizedOnly,
		},
		{
			Link:  pageHandlers.PathToNotification,
			Name:  "Список уведомлений",
			Login: webServerDTO.LoginAuthorizedOnly,
		},
	},
	Priority: 180,
}
