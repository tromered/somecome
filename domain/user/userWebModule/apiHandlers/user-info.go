package apiHandlers

import (
	"net/http"
	"somecome/superobject"
	"time"
)

type UserInfoOutput struct {
	// ИД пользователя
	ID int64 `json:"id"`
	// Логин пользователя
	Login string `json:"login"`
	// Время создания пользователя (регистрация)
	Created time.Time `json:"created"`
}

func (o *Object) userInfoHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, err := o.superObject.AuthDomain().GetUserFromContext(r.Context())
		if err != nil {
			wsd.WriteJSON(r.Context(), w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteJSON(r.Context(), w, http.StatusOK, UserInfoOutput{
			ID:      user.ID,
			Login:   user.Login,
			Created: user.Created,
		})
	})
}
