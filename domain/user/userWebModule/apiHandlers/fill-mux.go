package apiHandlers

import (
	"net/http"
	"somecome/domain/webserver/core"
	"somecome/superobject"
)

func (o *Object)FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle("/api/user/info", core.GetWithNoOptionWrapper(wsd, o.userInfoHandler(wsd)))
}
