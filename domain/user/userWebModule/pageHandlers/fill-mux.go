package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToChangeAvatar, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.changeAvatarPageGet(wsd),
			http.MethodPost: o.changeAvatarPagePost(wsd),
		},
	))
	mux.Handle(PathToUserInfo, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.userInfoPageGet(wsd),
		},
	))
	mux.Handle(PathToUserEdit, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.userEditPageGet(wsd),
			http.MethodPost: o.userEditPagePost(wsd),
		},
	))
	mux.Handle(PathToNotification, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.notificationPageGet(wsd),
			http.MethodPost: o.notificationPagePost(wsd),
		},
	))
}
