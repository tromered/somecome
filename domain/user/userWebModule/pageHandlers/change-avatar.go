package pageHandlers

import (
	"context"
	"mime/multipart"
	"net/http"
	userDTO "somecome/domain/user/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/logger"
	"somecome/superobject"
)

// ChangeAvatarTD - данные для рендеринга смены аватара
type ChangeAvatarTD struct {
	// Ссылка для сохранения
	SaveLink string
}

func (o *Object) changeAvatarPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ChangeAvatarPageTemplateName)
		tdata.SetData(ChangeAvatarTD{
			SaveLink: PathToChangeAvatar,
		})

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) changeAvatarPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ChangeAvatarPageTemplateName)
		tdata.SetData(ChangeAvatarTD{
			SaveLink: PathToChangeAvatar,
		})

		err := r.ParseMultipartForm(core.MaxFileSize)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		_, fileHeader, err := r.FormFile("file")
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		err = uploadAvatar(o.superObject, ctx, "", fileHeader)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		http.Redirect(w, r, PathToUserInfo, http.StatusSeeOther)
	})
}

// uploadAvatar - загружает новый аватар
func uploadAvatar(wsd superobject.SuperObject, ctx context.Context, token string, formFile *multipart.FileHeader) error {
	user, err := wsd.AuthDomain().GetUserFromContext(ctx)
	if err != nil {
		return err
	}

	if formFile.Size > core.MaxFileSize {
		return core.FileToLargeErr
	}

	name := formFile.Filename
	mime := formFile.Header.Get("Content-Type")
	fileBody, err := formFile.Open()
	if err != nil {
		return err
	}

	defer logger.IfErrFunc(ctx, fileBody.Close)

	err = wsd.UserDomain().UploadUserAvatar(
		ctx,
		user.ID,
		userDTO.AvatarInput{
			Token: token,
			Name:  name,
			Mime:  mime,
			Body:  fileBody,
		},
	)
	if err != nil {
		return err
	}

	return nil
}
