package pageHandlers

import (
	"net/url"
	"somecome/domain/webserver/link"
	"strconv"
)

const moduleTemplateName = "module:user:page"

// Коды страниц для рендеринга шаблонов
var (
	ChangeAvatarPageTemplateName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:user:page:change-avatar"}
	UserInfoPageTemplateName     = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:user:page:user-info"}
	UserEditPageTemplateName     = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:user:page:user-edit"}
	NotificationPageTemplateName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:user:page:notification"}
)

// Ендпоинты веб сервера
const (
	PathToUserAvatarSelect = "/pages/user/avatar/select"
	PathToChangeAvatar     = "/pages/user/avatar/change"
	PathToUserInfo         = "/pages/user/info"
	PathToUserEdit         = "/pages/user/edit"
	PathToNotification     = "/pages/user/notification"
)

// GetNotificationListLink - генерирует ссылку на страницу списка уведомлений
func GetNotificationListLink(pageNumber *int64) string {
	link := url.URL{
		Path: PathToNotification,
	}

	args := url.Values{}

	if pageNumber != nil {
		args.Set("page", strconv.FormatInt(*pageNumber, 10))
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetNotificationListLinkWrapper - враппер для генерации ссылок на страницы списка уведомлений
func GetNotificationListLinkWrapper() link.PageLinkGetter {
	return func(pageNumber int64) string {
		return GetNotificationListLink(&pageNumber)
	}
}
