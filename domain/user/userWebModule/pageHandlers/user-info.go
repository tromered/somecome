package pageHandlers

import (
	"errors"
	"net/http"
	userDTO "somecome/domain/user/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"time"
)

// UserInfoTD - данные пользователя для рендеринга
type UserInfoTD struct {
	// ИД пользователя
	ID int64
	// Логин пользователя
	Login string
	// Оригинальный логин пользователя
	OriginLogin *string

	// Имя пользователя
	Firstname *string
	// Фамилия пользователя
	Secondname *string
	// Отчество пользователя
	Patronymic *string

	// Есть ли аватар
	HasAvatar bool
	// Ссылка на аватара пользователя
	AvatarLink string

	// Время создания учетной записи
	Created time.Time
	// Время последнего обновления записи
	Updated *time.Time

	// Ссылка для смены аватара
	AvatarEditorLink string
	// Ссылка для редактирования данных
	DataEditorLink string
}

func (o *Object) userInfoPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, UserInfoPageTemplateName)

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := UserInfoTD{
			ID:    userData.ID,
			Login: userData.Login,
			// Важно: это дата создания учетной записи, не дата создания доп информации
			Created: userData.Created,

			AvatarEditorLink: PathToChangeAvatar,
			DataEditorLink:   PathToUserEdit,
		}

		userInfo, err := o.superObject.UserDomain().GetMainInfo(ctx, userData.ID)
		if err != nil && !errors.Is(err, userDTO.UserNotFoundErr) {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		if userInfo != nil {
			data.OriginLogin = userInfo.OriginLogin
			data.Firstname = userInfo.Firstname
			data.Secondname = userInfo.Secondname
			data.Patronymic = userInfo.Patronymic
			data.Updated = userInfo.Updated

			if userInfo.Avatar != nil {
				data.HasAvatar = true
				// Важно: ссылка идет на конкретный файл, чтобы избежать проблем кеширования
				data.AvatarLink = link.GetFileLink(*userInfo.Avatar, false)
			}
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
