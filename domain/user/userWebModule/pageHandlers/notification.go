package pageHandlers

import (
	"context"
	"net/http"
	userDTO "somecome/domain/user/dto"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/domain/webserver/link"
	"somecome/pkg/logger"
	"somecome/superobject"
	"strconv"
	"time"
)

// NotificationsTD - данные для страницы уведомлений
type NotificationsTD struct {
	// Ссылка для действий
	ActionLink string
	// Уведомления
	List []NotificationUnit
	// Страница
	CurrentPage int64
	// Страницы списка уведомлений
	Pages []webServerDTO.Page
	// Количество уведомлений
	Count int64
	// Количество непрочитанных уведомлений
	UnreadCount int64
}

// NotificationUnit - данные для уведомления
type NotificationUnit struct {
	// ИД уведомления
	ID int64
	// ИД пользователя получившего уведомление
	UserID int64
	// Заголовок уведомления
	Title string
	// Текст уведомления
	Message string
	// Название контента
	ContentName string
	// Ссылка на контент
	ContentLink *string
	// Ссылка в уведомлении
	Link *string
	// Текст ссылки
	LinkText string
	// Уведомление прочитано
	IsRead bool
	// Время создания уведомления
	Created time.Time
}

func (o *Object) notificationPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, NotificationPageTemplateName)
		pageRaw := r.URL.Query().Get("page")
		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		var (
			currentPage int64 = 1
		)

		// Если указана страница то обрабатываем ее
		if pageRaw != "" && pageRaw != "last" {
			parsedPage, err := strconv.ParseInt(pageRaw, 10, 64)
			if err != nil {
				logger.DebugError(ctx, err)
			} else {
				currentPage = parsedPage
			}
		}

		notificationList, err := o.superObject.UserDomain().GetNotifications(ctx, userData.ID, &currentPage)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := NotificationsTD{
			ActionLink:  PathToNotification,
			CurrentPage: currentPage,
			Count:       notificationList.Count,
			UnreadCount: notificationList.UnreadCount,
		}

		for _, notification := range notificationList.Items {
			data.List = append(data.List, o.notificationFromModel(ctx, notification))
		}

		data.Pages = wsd.GeneratePagination(
			currentPage, notificationList.PageCount,
			GetNotificationListLinkWrapper(),
		)

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) notificationPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)
		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		page, err := strconv.ParseInt(r.FormValue("page"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		readAction := r.FormValue("read-action")

		if readAction == "read-all" {
			err = o.superObject.UserDomain().MarkNotificationAsReadByUser(ctx, userData.ID)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}
		} else {
			notificationID, err := strconv.ParseInt(readAction, 10, 64)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}

			err = o.superObject.UserDomain().MarkNotificationAsRead(ctx, notificationID, userData.ID)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}
		}

		http.Redirect(w, r, GetNotificationListLink(&page), http.StatusSeeOther)
	})
}

func (o *Object) notificationFromModel(ctx context.Context, raw *userDTO.Notification) NotificationUnit {
	linkText := "Источник"
	contentName := "Контент"

	if raw.LinkText != nil {
		linkText = *raw.LinkText
	}

	var contentLink *string

	if raw.ContentKey != nil {
		cn := o.superObject.ContentDomain().GetKeyName(ctx, *raw.ContentKey)
		if cn != nil {
			contentName = *cn
		}

		contentLink = o.superObject.ContentDomain().GetKeyLink(ctx, *raw.ContentKey)
	}

	return NotificationUnit{
		ID:          raw.ID,
		UserID:      raw.UserID,
		Title:       raw.Title,
		Message:     raw.Message,
		ContentName: contentName,
		ContentLink: contentLink,
		Link:        raw.Link,
		LinkText:    linkText,
		IsRead:      raw.IsRead,
		Created:     raw.Created,
	}
}
