package user

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/user/database"
	userDTO "somecome/domain/user/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

func withUser(info *userDTO.PrettyInfo, userInfo *database.UserInfo) {
	if userInfo.OriginLogin.Valid {
		info.Login = userInfo.OriginLogin.String
		info.DisplayName = userInfo.OriginLogin.String
	}

	info.HasAvatar = userInfo.Avatar.Valid

	info.Firstname = sqlconvert.FromNullString(userInfo.Firstname)
	info.Secondname = sqlconvert.FromNullString(userInfo.Secondname)
	info.Patronymic = sqlconvert.FromNullString(userInfo.Patronymic)

	info.DisplayName = buildName(info.Firstname, info.Secondname, info.Patronymic, info.DisplayName)
}

// GetPrettyInfo - возвращает информацию о пользователе для рендеров по ИД
func (domain *Domain) GetPrettyInfo(ctx context.Context, id int64) (*userDTO.PrettyInfo, error) {
	rawUser, err := domain.superObject.AuthDomain().GetUser(ctx, id)

	// Переоборачиваем ошибку
	if errors.Is(err, authDTO.UserNotFoundErr) {
		return nil, errs.WrapError(ctx, DomainError, userDTO.UserNotFoundErr)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	info := &userDTO.PrettyInfo{
		UserID:      id,
		Login:       rawUser.Login,
		DisplayName: rawUser.Login,
	}

	userInfo, err := domain.storage.GetUserInfoByID(ctx, id)

	// Нет данных, не заполняем до конца
	if errors.Is(err, sql.ErrNoRows) {
		return info, nil
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	withUser(info, userInfo)

	return info, nil
}

// GetAllPrettyInfo - возвращает информацию о всех пользователях для рендеров
func (domain *Domain) GetAllPrettyInfo(ctx context.Context) ([]*userDTO.PrettyInfo, error) {
	rawUsers, err := domain.superObject.AuthDomain().AllUsers(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	allUserInfo, err := domain.storage.GetAllUserInfo(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	infoMap := make(map[int64]*database.UserInfo)
	for _, user := range allUserInfo {
		infoMap[user.UserID] = user
	}

	result := make([]*userDTO.PrettyInfo, 0, len(rawUsers))

	for _, rawUser := range rawUsers {

		info := &userDTO.PrettyInfo{
			UserID:      rawUser.ID,
			Login:       rawUser.Login,
			DisplayName: rawUser.Login,
		}

		userInfo, ok := infoMap[rawUser.ID]
		if ok {
			withUser(info, userInfo)
		}

		result = append(result, info)
	}

	return result, nil
}

func buildName(firstname, secondname, patronymic *string, alter string) string {
	switch {
	case firstname != nil && secondname != nil && patronymic != nil:
		return fmt.Sprintf("%s %s. %s.", *secondname, firstLetter(*firstname), firstLetter(*patronymic))
	case firstname != nil && secondname != nil:
		return fmt.Sprintf("%s %s.", *secondname, firstLetter(*firstname))
	case secondname != nil:
		return *secondname
	case firstname != nil:
		return *firstname
	}

	return alter
}

func firstLetter(v string) string {
	letters := []rune(v)

	if len(letters) == 0 {
		return ""
	}

	return string(letters[0])
}
