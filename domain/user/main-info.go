package user

import (
	"context"
	"database/sql"
	"errors"
	"somecome/domain/user/database"
	userDTO "somecome/domain/user/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
	"strings"
)

func mainInfoFromDB(raw *database.UserInfo) *userDTO.MainInfo {
	return &userDTO.MainInfo{
		UserID:      raw.UserID,
		OriginLogin: sqlconvert.FromNullString(raw.OriginLogin),
		Firstname:   sqlconvert.FromNullString(raw.Firstname),
		Secondname:  sqlconvert.FromNullString(raw.Secondname),
		Patronymic:  sqlconvert.FromNullString(raw.Patronymic),
		Avatar:      sqlconvert.FromNullString(raw.Avatar),
		Created:     raw.Created,
		Updated:     sqlconvert.FromNullTime(raw.Updated),
	}
}

func mainInfoToDB(raw userDTO.MainInfo) *database.UserInfo {
	return &database.UserInfo{
		UserID:      raw.UserID,
		OriginLogin: sqlconvert.ToNullString(raw.OriginLogin),
		Firstname:   sqlconvert.ToNullString(raw.Firstname),
		Secondname:  sqlconvert.ToNullString(raw.Secondname),
		Patronymic:  sqlconvert.ToNullString(raw.Patronymic),
		Avatar:      sqlconvert.ToNullString(raw.Avatar),
		Created:     raw.Created,
		Updated:     sqlconvert.ToNullTime(raw.Updated),
	}
}

// CreateMainInfo - создает основную информацию о пользователе
func (domain *Domain) CreateMainInfo(ctx context.Context, info userDTO.MainInfo) error {
	err := domain.storage.CreateUserInfo(ctx, mainInfoToDB(info))
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// GetMainInfo - возвращает информацию о пользователе по ИД
func (domain *Domain) GetMainInfo(ctx context.Context, id int64) (*userDTO.MainInfo, error) {
	user, err := domain.storage.GetUserInfoByID(ctx, id)

	if errors.Is(err, sql.ErrNoRows) {
		return nil, errs.WrapError(ctx, DomainError, userDTO.UserNotFoundErr)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return mainInfoFromDB(user), nil
}

// UpdateMainInfo - обновляет информацию о пользователе по ИД
func (domain *Domain) UpdateMainInfo(ctx context.Context, userID int64, input userDTO.UpdateMainInfoInput) error {
	authInfo, err := domain.superObject.AuthDomain().GetUser(ctx, userID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	if authInfo.Login != strings.ToLower(input.OriginLogin) {
		return errs.WrapError(ctx, DomainError, userDTO.LoginChangeIsNotPossibleErr)
	}

	user, err := domain.storage.GetUserInfoByID(ctx, userID)

	// Данных нет в базе, создаем
	if errors.Is(err, sql.ErrNoRows) {
		createErr := domain.storage.CreateUserInfo(ctx, &database.UserInfo{
			UserID:      userID,
			OriginLogin: sqlconvert.ToNullStringE(input.OriginLogin),
			Firstname:   sqlconvert.ToNullStringE(input.Firstname),
			Secondname:  sqlconvert.ToNullStringE(input.Secondname),
			Patronymic:  sqlconvert.ToNullStringE(input.Patronymic),
		})
		if createErr != nil {
			return errs.WrapError(ctx, DomainError, createErr)
		}

		return nil
	}

	// Прочие ошибки
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	user.OriginLogin = sqlconvert.ToNullStringE(input.OriginLogin)
	user.Firstname = sqlconvert.ToNullStringE(input.Firstname)
	user.Secondname = sqlconvert.ToNullStringE(input.Secondname)
	user.Patronymic = sqlconvert.ToNullStringE(input.Patronymic)

	// Данные есть, обновляем
	err = domain.storage.UpdateUserInfo(ctx, user)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}
