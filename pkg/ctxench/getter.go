package ctxench

import "context"

func getString(ctx context.Context, key *CtxKey) string {
	v, _ := ctx.Value(key).(string)

	return v
}

func GetRequestID(ctx context.Context) string {
	return getString(ctx, requestIDKey)
}

func GetConnectionID(ctx context.Context) string {
	return getString(ctx, connectionIDKey)
}

func GetServerID(ctx context.Context) string {
	return getString(ctx, serverIDKey)
}

func GetDebugMode(ctx context.Context) bool {
	v, _ := ctx.Value(debugModeKey).(bool)

	return v
}

func GetOriginPath(ctx context.Context) string {
	return getString(ctx, originPathKey)
}

func GetDeveloperMode(ctx context.Context) bool {
	v, _ := ctx.Value(developerModeKey).(bool)

	return v
}
