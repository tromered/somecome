package paginator

// Стандартное количество сообщений на странице
const DefaultOnPageCount int64 = 20

// PageToLimit - конвертирует старицу в сдвиг для БД
func PageToLimit(page int64, onPageCount int64) (limit, offset int64) {
	limit = onPageCount
	offset = (page - 1) * onPageCount

	return
}

// TotalToPages - конвертирует количество данных в количество страниц
func TotalToPages(total int64, onPageCount int64) (pageCount int64) {
	pageCount = total / onPageCount

	if total%onPageCount > 0 {
		pageCount++
	}

	return
}
