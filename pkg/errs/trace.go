package errs

import (
	"errors"
	"fmt"
	"runtime"
)

// ErrorTraceUnit - данные трассировки
type ErrorTraceUnit struct {
	From   string
	Method string
}

// StackTrace - получение трассировки
func StackTrace(skip int) []ErrorTraceUnit {
	pc := make([]uintptr, 500)
	n := runtime.Callers(skip, pc)
	pc = pc[:n]

	frames := runtime.CallersFrames(pc)
	units := make([]ErrorTraceUnit, 0, n)

	for {
		frame, more := frames.Next()

		units = append(units, ErrorTraceUnit{
			From:   fmt.Sprintf("%s:%d", frame.File, frame.Line),
			Method: frame.Function,
		})

		if !more {
			break
		}
	}

	return units
}

type errorWithTrace struct {
	// Обернутая ошибка
	error
	// Специальный текст ошибки ошибки
	trace []ErrorTraceUnit
}

// AddTrace - добавляет текст в ошибку
func AddTrace(err error, skip int) error {
	// Ошибки нет
	if err == nil {
		return nil
	}

	return &errorWithTrace{
		error: err,
		trace: StackTrace(skip),
	}
}

// GetTrace - получает текст из ошибки
func GetTrace(err error) []ErrorTraceUnit {
	// Ошибки нет
	if err == nil {
		return nil
	}

	errWT := new(errorWithTrace)

	if !errors.As(err, &errWT) {
		return nil
	}

	return errWT.trace
}
