package errs

import (
	"context"
	"errors"
	"fmt"
)

type errorGroupWrapper struct {
	// Обернутая ошибка
	error
	// Группа ошибки
	g Group
}

// Is - реализация интерфейса сравнения ошибок
func (egw *errorGroupWrapper) Is(targetErr error) bool {
	// Проверяем целевую ошибку на вхождение в группу (при сравнении с типом группы)
	if errors.Is(egw.g, targetErr) {
		return true
	}

	// Если целевая ошибка не совпала с группой, то проверяем исходную ошибку
	return errors.Is(egw.error, targetErr)
}

// WrapError - оборачивает ошибку в группу
func WrapError(_ context.Context, g Group, err error) error {
	// Ошибки нет
	if err == nil {
		return nil
	}

	// Группы нет, оборачивать не надо
	if g == nil {
		return err
	}

	return &errorGroupWrapper{
		error: fmt.Errorf("%s: %w", g.Error(), err),
		g:     g,
	}
}

// WrapErrorf - оборачивает форматированную ошибку в группу
func WrapErrorf(ctx context.Context, g Group, format string, arg ...interface{}) error {
	return WrapError(ctx, g, fmt.Errorf(format, arg...))
}
