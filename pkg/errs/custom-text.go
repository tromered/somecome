package errs

import (
	"errors"
)

type errorWithText struct {
	// Обернутая ошибка
	error
	// Специальный текст ошибки ошибки
	text string
}

// AddText - добавляет текст в ошибку
func AddText(err error, text string) error {
	// Ошибки нет
	if err == nil {
		return nil
	}

	return &errorWithText{
		error: err,
		text:  text,
	}
}

// GetText - получает текст из ошибки
func GetText(err error) *string {
	// Ошибки нет
	if err == nil {
		return nil
	}

	errWT := new(errorWithText)

	if !errors.As(err, &errWT) {
		return nil
	}

	// Поскольку будет возвращен указатель, копируем данные в новую переменную
	text := errWT.text

	return &text
}
