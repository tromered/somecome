package logger

import (
	"context"
	"fmt"
	"somecome/pkg/ctxench"
)

// Debug - вывести данные для отладки
func Debug(ctx context.Context, args ...interface{}) {
	if !ctxench.GetDebugMode(ctx) {
		return
	}

	print(ctx, logLevelDebug, 0, args...)
}

// DebugData - вывести подробно данные объекта
func DebugData(ctx context.Context, data interface{}) {
	if !ctxench.GetDebugMode(ctx) {
		return
	}

	print(ctx, logLevelDebug, 0, fmt.Sprintf("%#+v", data))
}

// DebugError - вывести в лог ошибку, только в режиме отладки
func DebugError(ctx context.Context, err error) {
	if err == nil {
		return
	}

	if !ctxench.GetDebugMode(ctx) {
		return
	}

	print(ctx, logLevelError, 0, err.Error())
}
